package com.blk_.calculate.db;

import android.provider.BaseColumns;

final class HistoryBD {

    private HistoryBD() {
    }

    public static final class History implements BaseColumns {
        public final static String TABLE_NAME = "lists";
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_RESULT = "result";
        public final static String COLUMN_TNS = "tns";
        public final static String COLUMN_DATE = "date";
    }

    public static final class HistoryItem implements BaseColumns {
        public final static String TABLE_NAME = "items";
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_FK = "foreignKey";
        public final static String COLUMN_VALUE = "element";

    }
}
