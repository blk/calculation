package com.blk_.calculate.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.format.DateFormat;
import android.util.Log;

import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.historylist.CalcList;
import com.blk_.calculate.parsing.Parse;

import java.util.ArrayList;
import java.util.Date;


interface IDatabaseHandler {
    void addCalcList(CalcList list);

    CalcList getCalcList(int id);

    ArrayList<CalcList> getAllList();

    int getContactsCount();

    int updateContact(CalcList list);

    void deleteItem(int id);

    void deleteAll();
}

public class DatabaseHandler extends SQLiteOpenHelper implements IDatabaseHandler {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "listHistory";
    private final Context context;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    private ArrayList<CalcList> mainList;

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_HISTORY_TABLE = "CREATE TABLE " + HistoryBD.History.TABLE_NAME + "("
                + HistoryBD.History._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + HistoryBD.History.COLUMN_RESULT + " TEXT NOT NULL,"
                + HistoryBD.History.COLUMN_TNS + " TEXT NOT NULL DEFAULT 'dec',"
                + HistoryBD.History.COLUMN_DATE + " TEXT" + ");";

        String CREATE_HISTORY_ITEM_TABLE = "CREATE TABLE " + HistoryBD.HistoryItem.TABLE_NAME + "("
                + HistoryBD.HistoryItem._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + HistoryBD.HistoryItem.COLUMN_VALUE + " TEXT NOT NULL,"
                + HistoryBD.HistoryItem.COLUMN_FK + " INTEGER, FOREIGN KEY (" + HistoryBD.HistoryItem.COLUMN_FK + ") REFERENCES " + HistoryBD.History.TABLE_NAME + "(" + HistoryBD.History._ID + ") ON UPDATE CASCADE ON DELETE CASCADE);";
        db.execSQL(CREATE_HISTORY_TABLE);
        db.execSQL(CREATE_HISTORY_ITEM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);

        // Удаляем старую таблицу и создаём новую
        db.execSQL("DROP TABLE IF EXISTS " + HistoryBD.History.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + HistoryBD.HistoryItem.TABLE_NAME);

        // Создаём новую таблицу
        onCreate(db);
    }

    public void replaceDBFromList(ArrayList<CalcList> list) {
        deleteAll();
        setNewList(list);
    }

    @Override
    public void addCalcList(CalcList list) {
        String result = list.getResult();
        String tns = getStringByType(list.getViewableSystem());
        mainList.add(list);

        String currentDateTimeString = (String) DateFormat.format("dd-MM-yyyy kk:mm", new Date());

        DatabaseHandler mDbHelper = new DatabaseHandler(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues valuesMain = new ContentValues();
        valuesMain.put(HistoryBD.History.COLUMN_RESULT, result);
        valuesMain.put(HistoryBD.History.COLUMN_TNS, tns);
        valuesMain.put(HistoryBD.History.COLUMN_DATE, currentDateTimeString);
        long newRowId = db.insert(HistoryBD.History.TABLE_NAME, null, valuesMain);
        if (newRowId >= 0)
            lastIDInHistory++;


        int forKey = lastIDInHistory;

        for (int i = 0; i < list.getArrayList().size(); i++) {
            String element = list.getArrayList().get(i);
            ContentValues valuesItem = new ContentValues();
            valuesItem.put(HistoryBD.HistoryItem.COLUMN_VALUE, element);
            valuesItem.put(HistoryBD.HistoryItem.COLUMN_FK, forKey);
            long newRowId2 = db.insert(HistoryBD.HistoryItem.TABLE_NAME, null, valuesItem);
            if (newRowId2 == -1)
                Log.w("SQLite", "Error adding new calclist to base " + newRowId2);
            else
                Log.w("SQLite", "Add new Element successfull");
        }
        this.close();

    }

    @Override
    public CalcList getCalcList(int id) {
        return null;
    }

    @Override
    public ArrayList<CalcList> getAllList() {
        return new ArrayList<>(mainList);
    }

    @Override
    public int getContactsCount() {
        return 0;
    }

    @Override
    public int updateContact(CalcList list) {
        return 0;
    }

    @Override
    public void deleteItem(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int rc = db.delete(HistoryBD.History.TABLE_NAME, HistoryBD.History._ID + "=" + id, null);

        int rci = db.delete(HistoryBD.HistoryItem.TABLE_NAME, HistoryBD.HistoryItem.COLUMN_FK + "=" + id, null);
        Log.d("DELDB", "row del count = " + String.valueOf(rc));
        Log.d("DELDBITEM", "row item del count = " + String.valueOf(rci));
        this.close();
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + HistoryBD.History.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + HistoryBD.HistoryItem.TABLE_NAME);
        lastIDInHistory = 0;
        onCreate(db);
    }

    private int lastIDInHistory;

    public void initDB(Parse p) {
        SQLiteDatabase db = this.getReadableDatabase();
        mainList = new ArrayList<>();
        String[] projection = {
                HistoryBD.History._ID,
                HistoryBD.History.COLUMN_RESULT,
                HistoryBD.History.COLUMN_TNS,
                HistoryBD.History.COLUMN_DATE
        };
        String[] projectionItems = {
                HistoryBD.HistoryItem._ID,
                HistoryBD.HistoryItem.COLUMN_FK,
                HistoryBD.HistoryItem.COLUMN_VALUE,
        };

        Cursor cursorList = db.query(
                HistoryBD.History.TABLE_NAME,   // таблица
                projection,            // столбцы
                null,                  // столбцы для условия WHERE
                null,                  // значения для условия WHERE
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);
        // порядок сортировки
        lastIDInHistory = 0;
        try {
            // Узнаем индекс каждого столбца
            int idColumnIndex = cursorList.getColumnIndex(HistoryBD.History._ID);
            int resultColumnIndex = cursorList.getColumnIndex(HistoryBD.History.COLUMN_RESULT);
            int tnsColumnIndex = cursorList.getColumnIndex(HistoryBD.History.COLUMN_TNS);
            int dateColumnIndex = cursorList.getColumnIndex(HistoryBD.History.COLUMN_DATE);
            cursorList.moveToFirst();
            while (!cursorList.isAfterLast()) {
                lastIDInHistory = cursorList.getInt(idColumnIndex);
                cursorList.moveToNext();
            }
            cursorList.moveToFirst();


            while (!cursorList.isAfterLast()) {
                ArrayList<String> list;
                list = new ArrayList<>();
                typeOfNumberSystem _bufTons = getTypeByString(cursorList.getString(tnsColumnIndex));
                String result = cursorList.getString(resultColumnIndex);
                String date = cursorList.getString(dateColumnIndex);
                int currentID = cursorList.getInt(idColumnIndex);

                String selection = HistoryBD.HistoryItem.COLUMN_FK + "=?";
                String[] selectionArgs = {String.valueOf(currentID)};
                Cursor cursorItems = db.query(
                        HistoryBD.HistoryItem.TABLE_NAME,   // таблица
                        projectionItems,            // столбцы
                        selection,                  // столбцы для условия WHERE
                        selectionArgs,                  // значения для условия WHERE
                        null,                  // Don't group the rows
                        null,                  // Don't filter by row groups
                        null);
                try {
                    // Узнаем индекс каждого столбца
                    int idColumnIndexItem = cursorItems.getColumnIndex(HistoryBD.HistoryItem._ID);
                    int resultColumnIndexItem = cursorItems.getColumnIndex(HistoryBD.HistoryItem.COLUMN_VALUE);
                    cursorItems.moveToFirst();
                    while (!cursorItems.isAfterLast()) {
                        String valueItem = cursorItems.getString(resultColumnIndexItem);
                        int currentIDitem = cursorItems.getInt(idColumnIndexItem);
                        list.add(valueItem);
                        cursorItems.moveToNext();
                    }

                } finally {
                    cursorItems.close();
                }
                CalcList lst = new CalcList(list, _bufTons, Parse.parseView_staticParseListToSpanString(list, _bufTons, null), result, date);
                mainList.add(lst);
                cursorList.moveToNext();
            }
        } finally {
            // Всегда закрываем курсор после чтения
            cursorList.close();
        }


    }

    private int getLastIDFromHistory() {

        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {
                HistoryBD.History._ID,
                HistoryBD.History.COLUMN_RESULT,
                HistoryBD.History.COLUMN_TNS,
        };
        // Делаем запрос
        int currentID = 0;


        Cursor cursorList = db.query(
                HistoryBD.History.TABLE_NAME,   // таблица
                projection,            // столбцы
                null,                  // столбцы для условия WHERE
                null,                  // значения для условия WHERE
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);
        try {
            int idColumnIndex = cursorList.getColumnIndex(HistoryBD.History._ID);
            while (cursorList.moveToNext()) {
                currentID = cursorList.getInt(idColumnIndex);
            }
        } finally {
            cursorList.close();
        }
        return currentID;


    }

    private typeOfNumberSystem getTypeByString(String val) {
        val = val.toUpperCase();
        if (val.equals("BIN")) return typeOfNumberSystem.NUMBER_SYSTEM_BINARY;
        if (val.equals("OCT")) return typeOfNumberSystem.NUMBER_SYSTEM_OCTAL;
        if (val.equals("DEC")) return typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL;
        if (val.equals("HEX")) return typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL;
        return typeOfNumberSystem.NUMBER_SYSTEM_UNKNOWN;
    }

    private String getStringByType(typeOfNumberSystem tos) {
        switch (tos) {
            case NUMBER_SYSTEM_BINARY:
                return "BIN";
            case NUMBER_SYSTEM_OCTAL:
                return "OCT";
            case NUMBER_SYSTEM_DECIMAL:
                return "DEC";
            case NUMBER_SYSTEM_HEXADECIMAL:
                return "HEX";
        }
        return "";
    }

    private void setNewList(ArrayList<CalcList> lst) {
        for (int i = 0; i < lst.size(); i++) {
            addCalcList(lst.get(i));
        }
    }


}
