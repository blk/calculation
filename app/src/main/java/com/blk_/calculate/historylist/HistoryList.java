package com.blk_.calculate.historylist;

import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;

import com.blk_.calculate.Information;
import com.blk_.calculate.enums.typeOfNumberSystem;

import java.util.ArrayList;
import java.util.Date;

public class HistoryList {

    // хз зачем то что сверху но пока пусть будет
    public ArrayList<CalcList> getList() {
        return list;
    }

    public ArrayList<CalcList> getDeletedList() {
        return deletedList;
    }

    public void setDeletedList(ArrayList<CalcList> _deletedList) {
        deletedList = new ArrayList<>(_deletedList);
    }

    public void setList(ArrayList<CalcList> list) {

        this.list = new ArrayList<>(list);
        setDeletedList(new ArrayList<>(list));
    }

    private boolean equals(CalcList list1, CalcList list2) {
        if (list1.getText().toString().equals(list2.getText().toString()))
            if (list1.getResult().equals(list2.getResult()))
                if (list1.getViewableSystem() == list2.getViewableSystem())
                    return list1.getDate().equals(list2.getDate());
        return false;
    }

    private ArrayList<CalcList> list;
    private ArrayList<CalcList> deletedList;

    public HistoryList() {
        list = new ArrayList<>();
        deletedList = new ArrayList<>();

    }

    public boolean addNewHistory(ArrayList<String> addedList, typeOfNumberSystem tos, SpannableStringBuilder text, String result) {
        CalcList _bufList = new CalcList(addedList, tos, text, result, (String) DateFormat.format("dd-MM-yyyy kk:mm", new Date()));
        if (!addedList.isEmpty()) {
            if (!list.isEmpty()) {
                if (!(equals(list.get(list.size() - 1), _bufList))) {
                    list.add(_bufList);
                    deletedList.add(_bufList);
                    checkDBSize();
                    return true;
                } else
                    return false;
            } else {
                list.add(_bufList);
                deletedList.add(_bufList);
                checkDBSize();
                return true;
            }
        }
        return false;
    }

    public void clearHistory() {
        list.clear();
        deletedList.clear();
    }

    private void checkDBSize() {
        while (list.size() > Information.getCapacityDatabase()) {
            list.remove(0);
        }
    }

    public void removeItem(int itemID) {
        deletedList.remove(itemID);
    }

    public void confirmDeletedList() {
        list.clear();
        //list = ;
        setList(new ArrayList<>(deletedList));
    }

//    public ArrayList<CalcList> inverseHistoryList()
//    {
//        ArrayList<CalcList> invList = new ArrayList<CalcList>();
//        ArrayList<CalcList> invListW = new ArrayList<CalcList>();
//        invListW = (ArrayList<CalcList>) getList().clone();
//
//        //invList = (ArrayList<CalcList>) getList().clone();
//        int i ;
//        for (i = invListW.size()-1;i>=0;i--) {
//            invList.add(invListW.get(i));
//        }
//
//        return invList;
//    }
}
