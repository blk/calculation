package com.blk_.calculate.historylist;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.blk_.calculate.Information;
import com.blk_.calculate.R;
import com.blk_.calculate.enums.typeOfHistoryItemButton;
import com.blk_.calculate.parsing.Parse;
import com.blk_.calculate.styleTheme.DrawableStyleManager;

import java.util.ArrayList;


public class HistoryTest extends DialogFragment {
    public interface OnItemPress {
        void onItemPress(typeOfHistoryItemButton thdb, int index, ArrayList<CalcList> list);
    }


    private OnItemPress oip;

    public void onAttach(Context context) {
        //Log.d(LOG_TAG, "Fragment1 onAttach");

        Activity a = (Activity) context;
        super.onAttach(context);

        try {
            oip = (OnItemPress) a;
            //fragmentButtonServiceListener = (OnSelectedButtonServiceListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString() + " must implement onSomeEventListener");
        }
    }

    private void deleteItemFromHistory(int id) {

        LinearLayout la = ll.findViewById(id);
        //ll.removeView(la);
        //la.setBackgroundColor(Color.RED);
        Animation anim = AnimationUtils.loadAnimation(this.getActivity(), R.anim.itemhistorydelete);
        la.startAnimation(anim);
        lst.set(id, null);
        countPress++;

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //la.setBackgroundColor(Color.RED);
                la.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        if (countPress == lst.size())
            this.dismiss();
        //la.setAnimation(anim);
    }

    private int countPress;
    private LinearLayout ll;
    private ArrayList<CalcList> lst;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        countPress = 0;
        View v = inflater.inflate(R.layout.historydialog, null);
        v.setBackgroundResource(DrawableStyleManager.getParams().dialogHistoryBackground);
        ScrollView sw = v.findViewById(R.id.sw);
        ll = v.findViewById(R.id.layHist);
        //LinearLayout lay=(LinearLayout) inflater.inflate(R.layout.historydialogitem,null);
        lst = Information.getHistoryList();

        Button clear = v.findViewById(R.id.btnclnall);
        Button close = v.findViewById(R.id.btnclose);
        clear.setBackgroundResource(DrawableStyleManager.getParams().drawableOperationID);
        clear.setTextColor(DrawableStyleManager.getParams().textOperationColor);
        close.setBackgroundResource(DrawableStyleManager.getParams().drawableOperationID);
        close.setTextColor(DrawableStyleManager.getParams().textOperationColor);

        clear.setOnClickListener(view -> oip.onItemPress(typeOfHistoryItemButton.BUTTON_CLEAR_ALL, 0, null));
        close.setOnClickListener(view -> oip.onItemPress(typeOfHistoryItemButton.BUTTON_CLOSE, 0, null));
        int idx = 0;
        for (CalcList cl : lst) {
            LinearLayout lay = (LinearLayout) inflater.inflate(R.layout.historydialogitem, null);
            lay.setBackgroundResource(DrawableStyleManager.getParams().dialogHistoryBackground);
            TextView textItem = lay.findViewById(R.id.textViewItemnew);
            TextView textResult = lay.findViewById(R.id.textItemViewResultnew);
            TextView textVDegree = lay.findViewById(R.id.textItemDegrnew);
            TextView textViewDate = lay.findViewById(R.id.textviewdatenew);
            textResult.setTextColor(v.getResources().getColor(DrawableStyleManager.getParams().dialogHistoryText));
            Button btnDel = lay.findViewById(R.id.buttonDeleteHist);
            lay.setId(idx);
            btnDel.setId(idx);
            btnDel.setOnClickListener(view -> {
                Log.d("BUTTON CLOSE CLICK", String.valueOf(view.getId()));
                deleteItemFromHistory(view.getId());
            });
            lay.setOnClickListener((view) -> {
                Log.d("LAYOUT TOUCH", String.valueOf(view.getId()));
                oip.onItemPress(typeOfHistoryItemButton.BUTTON_DIALOG_ITEM_UPLOAD, view.getId(), null);
            });

            textItem.setText(cl.getText());
            textResult.setText(Parse.parseView_addSeparatorsToString(cl.getResult(), cl.getViewableSystem()));
            textViewDate.setText(cl.getDate());

            String textDegree = "";
            switch (cl.getViewableSystem()) {
                case NUMBER_SYSTEM_BINARY:
                    textDegree = "BIN";
                    break;
                case NUMBER_SYSTEM_OCTAL:
                    textDegree = "OCT";
                    break;
                case NUMBER_SYSTEM_DECIMAL:
                    textDegree = "DEC";
                    break;
                case NUMBER_SYSTEM_HEXADECIMAL:
                    textDegree = "HEX";
                    break;
            }
            textVDegree.setText(textDegree);
            ll.addView(lay);
            if (lst.size() > 1 && (idx < lst.size() - 1)) {
                View vvv = lay.findViewById(R.id.viewSpacer);
                vvv.setBackgroundColor(DrawableStyleManager.getParams().textOperationColor);
                vvv.setVisibility(View.VISIBLE);
            }
            idx++;

        }

        sw.post(() -> sw.fullScroll(ScrollView.FOCUS_DOWN));

        return v;
    }

    @Override
    public void onStop() {
        if (countPress != 0)
            oip.onItemPress(typeOfHistoryItemButton.BUTTON_DIALOG_ITEM_DELETE, 0, lst);
        super.onStop();
        Log.d("ONSTOP", "PPO");
    }

}
