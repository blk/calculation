package com.blk_.calculate.historylist;

import android.text.SpannableStringBuilder;

import com.blk_.calculate.enums.typeOfNumberSystem;

import java.util.ArrayList;

public class CalcList {
    public SpannableStringBuilder getText() {
        return text;
    }

    public void setText(SpannableStringBuilder text) {
        this.text = text;
    }

    private SpannableStringBuilder text;

    public String getResult() {
        return result;
    }

    private void setResult(String result) {
        this.result = result;
    }

    private String result;

    public typeOfNumberSystem getViewableSystem() {
        return viewableSystem;
    }

    private void setViewableSystem(typeOfNumberSystem viewableSystem) {
        this.viewableSystem = viewableSystem;
    }

    public CalcList(ArrayList<String> lst, typeOfNumberSystem tos, SpannableStringBuilder textV, String result, String date) {
        setArrayList(lst);
        setViewableSystem(tos);
        setResult(result);
        setText(textV);
        setDate(date);

    }

    private typeOfNumberSystem viewableSystem;

    public String getDate() {
        return date;
    }

    private void setDate(String date) {
        this.date = date;
    }

    private String date;

    public ArrayList<String> getArrayList() {
        return arrayList;
    }

    private void setArrayList(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
    }

    private ArrayList<String> arrayList;

}
