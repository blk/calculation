package com.blk_.calculate.historylist;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blk_.calculate.R;
import com.blk_.calculate.enums.typeOfDialogButton;


public class HistoryDialog extends AlertDialog {
    public interface OnButtonDialogPress {
        void onButtonDialogPress(typeOfDialogButton thdb);
    }

    private AlertDialog dialog;

    public AlertDialog getDialog() {
        return dialog;
    }

    public void setDialog(AlertDialog _d) {
        dialog = _d;
    }

    public OnButtonDialogPress getOnButtonListener() {
        return onButtonListener;
    }

    private OnButtonDialogPress onButtonListener;

    public HistoryDialog(@NonNull Context context) {
        super(context);
        Activity a = (Activity) context;
        try {
            onButtonListener = (OnButtonDialogPress) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString() + " must implement onSomeEventListener");
        }


        builder = new AlertDialog.Builder(context);
        builder.setTitle("История вычислений"); // заголовок для диалога
        LinearLayout lay = (LinearLayout) getLayoutInflater().inflate(R.layout.listview, null);
        lay.setMinimumHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        builder.setView(lay);
        builder.setNeutralButton("Clear", (dialog, which) -> {
            onButtonListener.onButtonDialogPress(typeOfDialogButton.DIALOG_BUTTON_CLEAR);
            // parser.getDbHelper().deleteAll();
            // parser.historyList.clearHistory();
            dialog.cancel();
        });
        builder.setPositiveButton("Ok", (dialog, id) -> {
            onButtonListener.onButtonDialogPress(typeOfDialogButton.DIALOG_BUTTON_OK);

            dialog.cancel();
        });
        builder.setNegativeButton("Cancel", (dialog, id) -> {
            onButtonListener.onButtonDialogPress(typeOfDialogButton.DIALOG_BUTTON_CANCEL);
            dialog.cancel();
        });
        builder.setCancelable(false);


    }

    private AlertDialog.Builder builder;

    public Builder getBuilder() {
        return builder;
    }

}
