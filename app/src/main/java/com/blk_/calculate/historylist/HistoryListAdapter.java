package com.blk_.calculate.historylist;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blk_.calculate.R;
import com.blk_.calculate.enums.typeOfHistoryItemButton;
import com.blk_.calculate.parsing.Parse;
import com.blk_.calculate.styleTheme.DrawableStyleManager;

import java.util.ArrayList;

public class HistoryListAdapter extends BaseAdapter {
    private final ArrayList<CalcList> list;
    private final LayoutInflater inflater;

    public HistoryListAdapter(Context context, ArrayList<CalcList> lst, Parse p) {

        this.list = lst;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Activity a = (Activity) context;
        try {
            onButtonListener = (OnButtonClick) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString() + " must implement onSomeEventListener");
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    //    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
//        @Override
//        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//            if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
//                return false; // справа налево
//            }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
//                return false; // слева направо
//            }
//
//            if(e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
//                return false; // снизу вверх
//            }  else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
//                return false; // сверху вниз
//            }
//            return false;
//        }
//    }
    final float defaultsixe = 22;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.listitemnew, parent, false);
        }

        //  final GestureDetector gestureDetector = new GestureDetector(new GestureListener());
        Button buttonDel = view.findViewById(R.id.buttonDelete);

//        LinearLayout textLay = (LinearLayout) view.findViewById(R.id.textLay);
//        LinearLayout buttLay = (LinearLayout) view.findViewById(R.id.buttLay);
//        textLay.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,0));
//        buttLay.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,0));


        LinearLayout ll = view.findViewById(R.id.listitemnew);
        ll.setBackgroundResource(DrawableStyleManager.getParams().dialogHistoryBackground);
        TextView textView = view.findViewById(R.id.textViewItemnew);

        textView.setText(getCalcList(position).getText());
        TextView textView1 = view.findViewById(R.id.textItemDegrnew);
        TextView textView2 = view.findViewById(R.id.textItemViewResultnew);
        textView2.setTextColor(view.getResources().getColor(DrawableStyleManager.getParams().dialogHistoryText));
        String textDegree = "";
        final TextView textView3 = view.findViewById(R.id.textviewdatenew);


        switch (getCalcList(position).getViewableSystem()) {
            case NUMBER_SYSTEM_BINARY:
                textDegree = "BIN";
                break;
            case NUMBER_SYSTEM_OCTAL:
                textDegree = "OCT";
                break;
            case NUMBER_SYSTEM_DECIMAL:
                textDegree = "DEC";
                break;
            case NUMBER_SYSTEM_HEXADECIMAL:
                textDegree = "HEX";
                break;
        }
        textView1.setText(textDegree);
        textView2.setText(Parse.parseView_addSeparatorsToString(getCalcList(position).getResult(), getCalcList(position).getViewableSystem()));
        textView3.setText(getCalcList(position).getDate());

        buttonDel.setOnClickListener(v -> {
            Log.d("butDel", "click del button on item № " + String.valueOf(position));
            onButtonListener.onButtonClick(typeOfHistoryItemButton.BUTTON_DIALOG_ITEM_DELETE, position);
            //parser.replaceFromHistory(position);
        });
//        buttonUpl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("butUpload","click del button on item № " + String.valueOf(position));
//                onButtonListener.onButtonClick(typeOfHistoryItemButton.BUTTON_DIALOG_ITEM_UPLOAD,position);
//                //parser.replaceFromHistory(position);
//            }
//        });
        view.setOnClickListener(v -> onButtonListener.onButtonClick(typeOfHistoryItemButton.BUTTON_DIALOG_ITEM_UPLOAD, position));

        return view;
    }

    private OnButtonClick onButtonListener;

    private CalcList getCalcList(int position) {
        return (CalcList) getItem(position);
    }

    public interface OnButtonClick {
        void onButtonClick(typeOfHistoryItemButton typeButton, int itemid);
    }
}
