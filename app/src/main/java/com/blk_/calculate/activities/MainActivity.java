package com.blk_.calculate.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.blk_.calculate.App;
import com.blk_.calculate.Information;
import com.blk_.calculate.R;
import com.blk_.calculate.enums.StateRadDegr;
import com.blk_.calculate.enums.textAction;
import com.blk_.calculate.enums.textSize;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.enums.typeOfDialogButton;
import com.blk_.calculate.enums.typeOfDialogVoiceButton;
import com.blk_.calculate.enums.typeOfHistoryItemButton;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.enums.typeOfStyleTheme;
import com.blk_.calculate.fragments.DialogVoiceFragment;
import com.blk_.calculate.fragments.FragmentBasicSetButtons;
import com.blk_.calculate.fragments.FragmentDopButtons;
import com.blk_.calculate.fragments.FragmentDopPortraitButtons;
import com.blk_.calculate.historylist.CalcList;
import com.blk_.calculate.historylist.HistoryTest;
import com.blk_.calculate.logger.SingletonLogger;
import com.blk_.calculate.math.Converter;
import com.blk_.calculate.math.MemoryModule;
import com.blk_.calculate.net.MailSender;
import com.blk_.calculate.parsing.Parse;
import com.blk_.calculate.parsing.voiceRecStruct;
import com.blk_.calculate.styleTheme.DrawableStyleManager;
import com.blk_.calculate.textViews.FragmentTextViews;
import com.blk_.calculate.textViews.TextFragmentStruct;
import com.splunk.mint.Mint;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

//import com.blk_.calculate.Logger.Logger;


/**
 * Класс основного активити
 * Содержит и управляет фрагментами:
 * основная раскладка клавиатуры, дополнительная, и текстовые поля вывода, с к доп кнопками управления
 * связывает всё со всем, базу данных истории с диалогом отображения истории, боковое меню настроек с фраментами
 */
public class MainActivity extends AppCompatActivity
        implements

        //HistoryDialog.OnButtonDialogPress,          // слушатель диалога с историей, тут приходят нажатия на основные кнопки очистки, отмены и подтверждения
        //HistoryListAdapter.OnButtonClick,           // слушатель каждой отдельной записи диалога с историей, приходят нажатия на кнопки удаления одной записи и выгрузки записи пользователю
        FragmentTextViews.OnSelectedButtonListener,         // слушатель фрагмента с текстовыми полями и кнопками. сюда приходят кнопки радиан-градусов
        FragmentTextViews.OnSelectedButtonServiceListener,  // слушатель фрагмента с текстовыми полями и кнопками. сюда приходят события сервисных кнопок (на данный момент только кнопка отображения истории вычислений)
        Parse.OnTextViewChange,                     // слушатель парсера, сюда приходит событие смены текста. нужно для того чтобы фрагмент с текстовыми полями перерисовал текст в себе для изменения размера текста
        Parse.OnTypeOfNumberSystemChanged,          // слушатель парсера, который, также, отвечает за выгрузку из истории. Слушает изменилась ли система счисления при выгрузке истории, и сообщает об этом фрагменту с доп кнопками чтобы тот щелкнул нужную радиокнопку смены системы счисления
        FragmentBasicSetButtons.OnSelectedButtonListener,       // слушатель основных кнопок
        FragmentDopButtons.OnSelectedButtonListener,           // слушатель дополнительных кнопок
        FragmentDopPortraitButtons.OnSelectedButtonListenerDop, // слушатель дополнительных кнопок портретного режима
        DialogVoiceFragment.OnButtonVoiceDialogPress,
        HistoryTest.OnItemPress {          // слушатель активити настоек (ЗАМОРОЖЕНО)
    private final String TAG = "LOG_MAIN_ACTIVITY";
    public static final String KEY_VALUE_RESULT = "VALUE_RESULT";
    public static final String KEY_VALUE_VIEW = "VALUE_VIEW";
    private static final String KEY_VALUE_MEMORY = "VALUE_MEMORY";
    private final int IDD_LIST_HISTORY = 0;

    private TextView textViewSizeText;
    private voiceRecStruct vcr;


    private FragmentBasicSetButtons fragmentButtonMain;
    private FragmentDopButtons fragmentButtonLandButton;
    private FragmentDopPortraitButtons fragmentButtonDopPortrait;
    private FragmentTextViews fragmentTextViews;
    private Parse parser;
    private MemoryModule memMod;
    private DrawerLayout mDrawerLayout;
    private Button historyB;
    private ImageButton darkThemeB;
    private ImageButton lightThemeB;
    private Switch switchBigKey;
    private Switch switchVoiceKey;
    private SeekBar seekBar;
    private RadioButton rbrepl;
    private RadioButton rbadd1;
    private TextView twlog;
    private NavigationView navigationView;
    private boolean stateOfDopKeyboard;

    // variable for checking Voice Recognition support on user device
    private static final int VR_REQUEST = 999;
    // variable for checking TTS engine data on user device
    private static final int MY_DATA_CHECK_CODE = 0;

    private textSize textsize;

    public static void onException(Throwable exception) {
        exception.printStackTrace();
        Toast.makeText(App.getContext(), "Oops - Some Exception! = " + exception.getMessage(),
                Toast.LENGTH_SHORT).show();

        ArrayList<String> list = new ArrayList<>();
        list.add(DateFormat.format("[dd-MM-kk:mm]", new Date()) + "BEGIN EXCEPTION");
        list.add(exception.getMessage());
        for (int i = 0; i < exception.getStackTrace().length; i++) {
            list.add(exception.getStackTrace()[i].toString());
        }
        list.add(DateFormat.format("[dd-MM-kk:mm]", new Date()) + "END EXCEPTION");
//        try {
//            Logger.writeException(list);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //generateMessage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            Mint.initAndStartSession(this.getApplication(), "a3439927");
            SingletonLogger.getInstance().initLogger(this.getFilesDir().toString(), this);
            initialization(savedInstanceState);
            Information.setOrientation(getResources().getConfiguration().orientation);
            //double d = 9/0;
        } catch (Throwable exception) {
            SingletonLogger.getInstance().onException(exception);
//            onException(exception);
        }

    }


    private void initialization(Bundle savedInstanceState) {

        Log.d(TAG, "MAIN ON CREATE");
        // dsm = new DrawableStyleManager();
        //Test tst = new Test(this.getFilesDir().toString());
        //Logger.initPathforLog(this.getFilesDir().toString());

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Information.setCurrentTypeOfSystem(typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL);
        Information.setStateRadDegr(StateRadDegr.TYPE_INPUT_RADIAN);
        setContentView(R.layout.mainactivity);
        initElements();
        initTheme();
        initNavigationBar();
        initDataSaveInstance(savedInstanceState);
        initVoiceRecognition();
        setTheme(DrawableStyleManager.getCurrentStyle());
    }

    private void initTheme() {
        try {
            DrawableStyleManager.getState(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (DrawableStyleManager.getCurrentStyle()) {
            case DARK_THEME:
                DrawableStyleManager.setDarkTheme(getApplicationContext());
                break;
            case LIGHT_THEME: {
                DrawableStyleManager.setLightTheme(getApplicationContext());
                break;
            }
        }
    }

    /**
     * Инициализация голосового ввода
     */
    private void initVoiceRecognition() {
        PackageManager packManager = getPackageManager();
        ImageButton speechBtn = findViewById(R.id.btn_voice);
        List<ResolveInfo> intActivities = packManager.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (intActivities.size() != 0) {
            // speech recognition is supported - detect user button clicks
            speechBtn.setOnClickListener(v -> {
//                DialogVoiceFragment dlg1 = new DialogVoiceFragment();
//                dlg1.show(getFragmentManager(), "DialogVoice!");
//                ArrayList<String> tst = new ArrayList<>();
//                tst.add("11 + cos 5 - корень 3");
//                dlg1.setList(tst);

                listenToSpeech();

                //ArrayList<String> suggestedWords = new ArrayList<>();
                //suggestedWords.add("3 + 2 - 5 + корень из 5");

            });
            // подготовим движок TTS, чтобы проговорить выбранное слово
            Intent checkTTSIntent = new Intent();
            // проверяем данные для TTS
            checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            // запускаем намерение - результат получим в методе onActivityResult
            startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
        } else {
            // speech recognition not supported, disable button and output
            // message
            speechBtn.setEnabled(false);
//            Toast.makeText(this, "Oops - Speech recognition not supported!",
//                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Сюда приходит итог работы голосовой активити
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VR_REQUEST && resultCode == RESULT_OK) {
            ArrayList<String> suggestedWords = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            DialogVoiceFragment dlg1 = new DialogVoiceFragment();
            dlg1.show(getSupportFragmentManager(), "DialogVoice!");
            dlg1.setList(suggestedWords);
//            Toast.makeText(this, suggestedWords.get(0),
//                    Toast.LENGTH_LONG).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Instruct the app to listen for user speech input
     */
    private void listenToSpeech() {
        try {
            // запускаем намерение для распознавания речи
            Intent listenIntent = new Intent(
                    RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            // indicate package
            listenIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                    getClass().getPackage().getName());
            // Текст-приглашение
            listenIntent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                    R.string.saysomething);
            // set speech model
            listenIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            // задаем число получаемых результатов
            listenIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);
            // начинаем слушать
            startActivityForResult(listenIntent, VR_REQUEST);
        } catch (Throwable ex) {
            SingletonLogger.getInstance().onException(ex);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!parser.getArrayListSymbs().getCurrentList().isEmpty()) {
            parser.parseView_parseAndViewArrayList();
            parser.parseView_calculateAndViewPreSummary();
        }
    }

    /**
     * Инициализация предыдущего состояния (после переворота дисплея) для элементов которым требуется сохранение данных
     *
     * @param sis Bundle параметр с сохраненными значениями
     */
    private void initDataSaveInstance(Bundle sis) {
        Log.d(TAG, "initDataSaveInstance");
        if (sis != null) {
            parser.onSaveInstance(sis);
            if (!isPortrait()) {
                fragmentTextViews.setButtonDegreeRadian();
                setBlockButtonsbyNumericSystem(typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL);
            } else {
                setBlockButtonsbyNumericSystem(typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL);
            }
            parser.parseView_parseAndViewArrayList();
            if (!(Objects.requireNonNull(sis.getString(KEY_VALUE_MEMORY)).isEmpty())) {
                memMod.plusToMemory(Converter.convertAdapter(String.valueOf(sis.getString(KEY_VALUE_MEMORY)), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem()));
                fragmentTextViews.setMemory(memMod.getFormatMemoryValue());
            }

        }

        fragmentButtonMain.setMemoryLayoutVisibility(getLocalStateOfDopKeyboard());
        if (!isPortrait())
            fragmentButtonLandButton.setMemoryLayoutVisibility(getLocalStateOfDopKeyboard());
        setSeekBarProgressState(textSizeToInt(getLocalStateTextSize()));
    }

    /**
     * Основная инициализация элементов. Фрагментов и пр.
     */
    private void initElements() {
        memMod = new MemoryModule();
        //setFr = new SettingsFragment();
        Log.d(TAG, "init element");
        if (!isPortrait()) {
            fragmentButtonMain = (FragmentBasicSetButtons) getSupportFragmentManager().findFragmentById(R.id.fragmentLandBut);
            fragmentTextViews = (FragmentTextViews) getSupportFragmentManager().findFragmentById(R.id.fragmentLandText);
            fragmentButtonLandButton = (FragmentDopButtons) getSupportFragmentManager().findFragmentById(R.id.fragmentLandButDop);

            fragmentButtonLandButton.initialElements();
        } else {
            fragmentButtonMain = (FragmentBasicSetButtons) getSupportFragmentManager().findFragmentById(R.id.fragmentPortBut);
            fragmentButtonDopPortrait = (FragmentDopPortraitButtons) fragmentButtonMain.getChildFragmentManager().findFragmentById(R.id.fragmentDopPortrait);
            fragmentButtonDopPortrait.initialElements();
            fragmentTextViews = (FragmentTextViews) getSupportFragmentManager().findFragmentById(R.id.fragmentPortText);
        }
        fragmentButtonMain.initialElements();
        if (fragmentButtonDopPortrait != null)
            fragmentButtonDopPortrait.initNavigationBar(fragmentButtonMain.getDrawerLayout());
        fragmentTextViews.initElements();

        parser = new Parse(this);
        parser.addTextListener(this);
        parser.addNumberSystemListener(this);
        if (!isPortrait()) {
            //parser.setButtonD(fragmentButtonLandButton.getButtonOperationDegreeNum());
            parser.setCurrentTypeOfNumericSystem();
        } else {
            //parser.setButtonD(fragmentButtonDopPortrait.getButtonOperationDegreeNum());
            parser.setCurrentTypeOfNumericSystem();
        }


        setBlockButtonsbyNumericSystem(Information.getCurrentTypeOfSystem());
    }

    /**
     * Реализация слушателя нажатия кнопки радиан-градусов
     *
     * @param buttonType - тип нажатой кнопки
     */
    @Override
    public void onButtonSelected(StateRadDegr buttonType) {
        Log.d(TAG, "MAIN ACTIVITY CHANGE RAD_DEG TO" + String.valueOf(buttonType));
        parser.parseView_parseAndViewArrayList();
        parser.parseView_calculateAndViewPreSummary();
    }

    /**
     * НА ДАННЫЙ МОМЕНТ ЗАМОРОЖЕНО
     * Реализация слушателя нажатия на кнопку настроек или истории (от фрагмента с текстовыми полями, эти кнопки к нему относятся)
     *
     * @param action - действие
     */
    @Override
    public void onButtonServiceSelected(textAction action) {
        switch (action) {
            case TEXTMEM: {
                onButtonSelected(typeOfAction.BUTTON_MEMORY_UPLOAD);
                break;
            }
            case TEXTHISTORY: {
                //showMyDialog(IDD_LIST_HISTORY);
                if (Information.getHistoryList().size() == 0) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            R.string.emptyHistory,
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else {
                    // if (Build.VERSION.SDK_INT < LOLLIPOP_MR1) {
                    //(IDD_LIST_HISTORY);
                    //return;
                    //} //else {
                    hd = new HistoryTest();
                    // Button positiveButton = (hd).getButton(DialogInterface.BUTTON_POSITIVE));
                    // positiveButton.setTextColor(this.getResources().getColor(DrawableStyleManager.getParams().dialogHistoryButtonColor));
                    // positiveButton.invalidate();
                    hd.show(getSupportFragmentManager(), "DialogVoice!");
                    //  }
                }
                break;
            }
            case TEXTPARENTHESIS: {
                parser.addSmartParethesis();
                break;
            }
            case TEXTLONGPARENTHESIS: {
                parser.addAllSmartParenthesis();
                break;
            }
        }
    }

    private HistoryTest hd;

    /**
     * Реализация слушателя парсера , парсер сообщает когда нужно обновить текстовое поле (нужно для перерисовки текстового поля в нужном размере)
     */
    @Override
    public void onTextViewChange(TextFragmentStruct tfs, Animation animation) {
        Log.d(TAG, "dsfs");
        fragmentTextViews.listenerChangingText(tfs, animation);
    }

    /**
     * метод определения ориентации экрана
     *
     * @return - true - портретная ориентация,false  - альбомная
     */
    private boolean isPortrait() {
        Log.d(TAG, "isPortrait()");
        return (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }

    /**
     * Реализация слушателя на кнопки от фрагментов с кнопками
     *
     * @param buttonType - тип нажатого действия
     */
    @Override
    public void onButtonSelected(typeOfAction buttonType) {
        // слушаем копки
        try {
            Log.d(TAG, "MAIN ACTIVITY BUTTON PRESS" + String.valueOf(buttonType));
            switch (buttonType) {
                case BUTTON_MEMORY_CLEAR: {
                    memMod.clearMemory();
                    fragmentTextViews.setMemory(memMod.getFormatMemoryValue());
                    break;
                }
                case BUTTON_MEMORY_UPLOAD: {
                    if (!memMod.getCleanStatus()) {
                        Parse.textCursorStruct tcs = parser.memory_uploadFromMemory(memMod.getMemoryValue(), parser.getIndexofListByTextCursorPosition(fragmentTextViews.getTextView().getSelectionStart(), String.valueOf(fragmentTextViews.getTextView().getText())));
                        fragmentTextViews.getTextView().setSelection(parser.tcsToCursorPosition(tcs, String.valueOf(fragmentTextViews.getTextView().getText())));
                        Information.setCurTextPos(fragmentTextViews.getTextView().getSelectionStart());
                    }

                    break;
                }
                case BUTTON_MEMORY_PLUS: {
                    //memMod.plusToMemory(parser.getLastValue());
                    memMod.plusToMemory(
                            Converter.convertAdapter(
                                    parser.parse_calculatePreSum().getStringValue(),
                                    typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem()));
                    fragmentTextViews.setMemory(memMod.getFormatMemoryValue());
                    break;
                }
                case BUTTON_MEMORY_MINUS: {
                    memMod.minusFromMemory(
                            Converter.convertAdapter(
                                    parser.parse_calculatePreSum().getStringValue(),
                                    typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem()));
                    fragmentTextViews.setMemory(memMod.getFormatMemoryValue());
                    break;
                }
                case ACTION_RADIO_BIN:
                case ACTION_RADIO_OCT:
                case ACTION_RADIO_DEC:
                case ACTION_RADIO_HEX: {
                    setBlockButtonsbyNumericSystem(Information.getCurrentTypeOfSystem());
                    parser.setCurrentTypeOfNumericSystem();
                    parser.parseView_parseAndViewArrayList();
                    parser.parseView_calculateAndViewPreSummary();
                    memMod.setCurrentTNS(Information.getCurrentTypeOfSystem());
                    if (!memMod.getCleanStatus())
                        fragmentTextViews.setMemory(memMod.getFormatMemoryValue());
                    return;
                }
            }
            Parse.textCursorStruct tcs =
                    parser.elements_addElement(buttonType, fragmentTextViews.getTextView().getSelectionStart(), String.valueOf(fragmentTextViews.getTextView().getText()));
            int posText = parser.tcsToCursorPosition(tcs, String.valueOf(fragmentTextViews.getTextView().getText()));
            Log.d("CURSORP", "POS CURSOR + " + posText);
            fragmentTextViews.getTextView().setSelection(posText);
            Information.setCurTextPos(fragmentTextViews.getTextView().getSelectionStart());
        } catch (Throwable ex) {
            SingletonLogger.getInstance().onException(ex);
        }
    }


    /**
     * метод, ответственный за сохранение данных при переходе активити в неактивное состояние (onPause())
     *
     * @param outState - Bundle - объект, в который записываются параметры которые следует сохранить
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragmentTextViews.clear();
        try {
            DrawableStyleManager.saveState(this);
            saveStateKeyboard();
            saveTextSizeState();
            saveStateVoice();
        } catch (IOException e) {
            e.printStackTrace();
        }
        outState.putStringArrayList(KEY_VALUE_VIEW, parser.getArrayListSymbs().getConvertedToDecimalList());
        outState.putString(KEY_VALUE_RESULT, Converter.convertAdapter(parser.getLastValue(), Information.getCurrentTypeOfSystem(), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL));
        outState.putString(KEY_VALUE_MEMORY, Converter.convertAdapter(memMod.getMemoryValue(), Information.getCurrentTypeOfSystem(), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL));
    }

    /**
     * Метод вызывающий отображения диалогового окна с историей вычислений
     *
     * @param id - айдишник диалога
     */
    private void showMyDialog(int id) {
        if (parser.getHistoryList().getList().isEmpty()) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "История вычислений пуста!",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else
            showDialog(id, null);

    }

    /**
     * Метод, устанавливающий тему визуального оформления
     *
     * @param tot - тип темы
     */
    private void setTheme(typeOfStyleTheme tot) {
        switch (tot) {
            case DARK_THEME:
                DrawableStyleManager.setDarkTheme(getApplicationContext());
                break;
            case LIGHT_THEME: {
                DrawableStyleManager.setLightTheme(getApplicationContext());
                break;
            }
        }
        fragmentButtonMain.setStyle(DrawableStyleManager.getParams());//dsm.getDrawableNumeric(),dsm.getDrawableOperation(),dsm.getTextNumericColor(),dsm.getTextOperationColor());
        if (!isPortrait())
            fragmentButtonLandButton.setStyle(DrawableStyleManager.getParams());//dsm.getDrawableNumeric(),dsm.getDrawableOperation(),dsm.getTextNumericColor(),dsm.getTextOperationColor(),dsm.getDrawableToggle(),dsm.getStyleRadioID());
        else
            fragmentButtonDopPortrait.setStyle(DrawableStyleManager.getParams());
        int posCurs = fragmentTextViews.getTextView().getSelectionStart();
        fragmentTextViews.setStyle(DrawableStyleManager.getParams());//dsm.getDrawableTextID(),dsm.getTextResultColor());
        parser.setColors(DrawableStyleManager.getParams().parserColorNum, DrawableStyleManager.getParams().parserColorOper, DrawableStyleManager.getParams().parserColorExpr, DrawableStyleManager.getParams().parserColorConst);
        parser.parseView_parseAndViewArrayList();
        if (!parser.getArrayListSymbs().getCurrentList().isEmpty())
            parser.parseView_calculateAndViewPreSummary();

        fragmentTextViews.getTextView().setSelection(posCurs);
        Information.setCurTextPos(fragmentTextViews.getTextView().getSelectionStart());
        for (int i = 0; i < parser.getHistoryList().getList().size(); i++)
            parser.getHistoryList().getList().get(i).setText(Parse.parseView_staticParseListToSpanString(parser.getHistoryList().getList().get(i).getArrayList(), Information.getCurrentTypeOfSystem(), null));


        navigationView.setBackgroundResource(DrawableStyleManager.getParams().drawableTextID);
        historyB.setBackgroundResource(DrawableStyleManager.getParams().drawableOperationID);
        historyB.setTextColor(DrawableStyleManager.getParams().textOperationColor);

        buttonAbout.setBackgroundResource(DrawableStyleManager.getParams().drawableOperationID);
        buttonAbout.setTextColor(DrawableStyleManager.getParams().textOperationColor);


        darkThemeB.setBackgroundResource(DrawableStyleManager.getParams().drawableForButtonTheme);
        lightThemeB.setBackgroundResource(DrawableStyleManager.getParams().drawableForButtonTheme);

        TextView twChangeTheme = findViewById(R.id.tesxtViewChangeTheme);
        twChangeTheme.setTextColor(DrawableStyleManager.getParams().textOperationColor);

        textViewSizeText.setTextColor(DrawableStyleManager.getParams().textNumericColor);
        seekBar.getProgressDrawable().setColorFilter(DrawableStyleManager.getParams().textOperationColor, PorterDuff.Mode.SRC_ATOP); // полоска
        seekBar.getThumb().setColorFilter(DrawableStyleManager.getParams().textOperationColor, PorterDuff.Mode.SRC_ATOP); // кругляшок
        TextView twTitleSeek = findViewById(R.id.menuTextSizeTitle);

        twTitleSeek.setTextColor(DrawableStyleManager.getParams().textOperationColor);

        switchBigKey.setTextColor(DrawableStyleManager.getParams().textOperationColor);
        switchBigKey.getTrackDrawable().setColorFilter(DrawableStyleManager.getParams().textNumericColor, PorterDuff.Mode.SRC_ATOP);
        switchBigKey.getThumbDrawable().setColorFilter(DrawableStyleManager.getParams().textOperationColor, PorterDuff.Mode.SRC_ATOP);


        switchVoiceKey.setTextColor(DrawableStyleManager.getParams().textOperationColor);
        switchVoiceKey.getTrackDrawable().setColorFilter(DrawableStyleManager.getParams().textNumericColor, PorterDuff.Mode.SRC_ATOP);
        switchVoiceKey.getThumbDrawable().setColorFilter(DrawableStyleManager.getParams().textOperationColor, PorterDuff.Mode.SRC_ATOP);

        rbrepl.setBackgroundResource(DrawableStyleManager.getParams().styleRadioVoiceID);
        rbadd1.setBackgroundResource(DrawableStyleManager.getParams().styleRadioVoiceID);

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{android.R.attr.state_enabled}
                },
                new int[]{
                        DrawableStyleManager.getParams().textOperationColor,
                        DrawableStyleManager.getParams().textOperationColor,
                        DrawableStyleManager.getParams().textOperationColor
                }
        );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rbadd1.setButtonTintList(colorStateList);
            rbrepl.setButtonTintList(colorStateList);
        }
//
//        ColorStateList colorStateList = new ColorStateList(
//                new int[][]{
//                        new int[]{android.R.attr.state_checked},
//                        new int[]{android.R.attr.state_enabled}
//                },
//                new int[] {
//                        DrawableStyleManager.getParams().textNumericColor,
//                        DrawableStyleManager.getParams().textOperationColor
//                }
//        );
//
//
//            if (Build.VERSION.SDK_INT > 21) {
//                rbadd1.setButtonTintList(colorStateList);
//                rbrepl.setButtonTintList(colorStateList);
//                rbadd1.setTextColor(DrawableStyleManager.getParams().textOperationColor);
//                rbrepl.setTextColor(DrawableStyleManager.getParams().textOperationColor);
//            } else {
//                rbadd1.setTextColor(DrawableStyleManager.getParams().textOperationColor);
//                rbrepl.setTextColor(DrawableStyleManager.getParams().textOperationColor);
//                // AppCompatRadioButton rba = (AppCompatRadioButton) rb; TODO: Продумать
//                // rba.setSupportButtonTintList(colorStateList);
//            }
    }

    /**
     * Метод, блокирующий нужные кнопки на двух фрагментах исходя из выбранной системы счисления
     *
     * @param tns -  система счисления
     */
    private void setBlockButtonsbyNumericSystem(typeOfNumberSystem tns) {
        ArrayList<Button> buttonsN = new ArrayList<>(16);
        for (int i = 0; i < 10; i++) {
            buttonsN.add(fragmentButtonMain.getButtonsN().get(i));
        }
        if (!isPortrait()) {
            for (int i = 0; i < 6; i++) {
                buttonsN.add(fragmentButtonLandButton.getButtonsN().get(i));
            }
        }
        for (int i = 0; i < buttonsN.size(); i++)
            buttonsN.get(i).setEnabled(true);
        switch (tns) {
            case NUMBER_SYSTEM_BINARY: {
                for (int i = 2; i < buttonsN.size(); i++)
                    buttonsN.get(i).setEnabled(false);
                break;
            }
            case NUMBER_SYSTEM_OCTAL: {
                for (int i = 8; i < buttonsN.size(); i++)
                    buttonsN.get(i).setEnabled(false);
                break;
            }
            case NUMBER_SYSTEM_DECIMAL: {
                for (int i = 10; i < buttonsN.size(); i++)
                    buttonsN.get(i).setEnabled(false);
                break;
            }
            case NUMBER_SYSTEM_HEXADECIMAL: {
                for (Button buf : buttonsN)
                    buf.setEnabled(true);
                break;
            }
        }
    }

    /**
     * Метод, вызываемый при изменении ползунка, отвечающего за размер текста
     *
     * @param progressState - позиция ползунка (0-3)
     */
    private void setSeekBarProgressState(int progressState) {
        switch (progressState) {
            case 0: {
                textViewSizeText.setText(R.string.seekbarLittle);
                changeFragmentsTextSize(textSize.TEXT_SIZE_SMALL);
                break;
            }
            case 1: {
                textViewSizeText.setText(R.string.seekbarMedium);
                changeFragmentsTextSize(textSize.TEXT_SIZE_MEDIUM);
                break;
            }
            case 2: {
                textViewSizeText.setText(R.string.seekbarBig);
                changeFragmentsTextSize(textSize.TEXT_SIZE_BIG);
                break;
            }
            case 3: {
                textViewSizeText.setText(R.string.seekbarMonster);
                changeFragmentsTextSize(textSize.TEXT_SIZE_FUCKIN_LARGE);
                break;
            }
            default: {
                textViewSizeText.setText("Средний");
                changeFragmentsTextSize(textSize.TEXT_SIZE_MEDIUM);
                break;
            }
        }
        parser.parseView_parseAndViewArrayList();
    }

    /**
     * Метод отпраляющий фрагментам нужные параметры изменения размера текста
     *
     * @param ts - тип текста
     */
    private void changeFragmentsTextSize(textSize ts) {
        fragmentTextViews.onManualChangeText(ts);
        if (!isPortrait())
            fragmentButtonLandButton.changeTextButtonSize(ts);
        else
            fragmentButtonDopPortrait.changeTextButtonSize(ts);
        fragmentButtonMain.changeTextButtonSize(ts);
        Information.setDefaultTextSize(Information.getTextSizeByName(ts));
    }

    private void setRadioVoiceBlocks(boolean isChecked) {
        if (!isChecked) {
            rbrepl.setTextColor(getResources().getColor(DrawableStyleManager.getParams().textDisabled));
            rbadd1.setTextColor(getResources().getColor(DrawableStyleManager.getParams().textDisabled));
        } else {
            rbrepl.setTextColor(DrawableStyleManager.getParams().textNumericColor);
            rbadd1.setTextColor(DrawableStyleManager.getParams().textNumericColor);
        }

    }

    private Button buttonAbout;


    public static void sendMessageActivity(Context context) {

        MailSender ms = new MailSender();
        ms.sendMail(context);
      /*  Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"blkfeedback@yandex.ru"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Отчет об ошибке");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Logger.getFilePath()));
        try {
            emailIntent.putExtra(Intent.EXTRA_TEXT, SingletonLogger.getLog());
        } catch (IOException e) {
            e.printStackTrace();
        }
        emailIntent.setType("application/octet-stream");
        try {
            context.startActivity(Intent.createChooser(emailIntent, "Send Email"));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(App.getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }*/
    }

    private void initAbout() {
        ImageView iw = findViewById(R.id.logoView);
        TextView tw = findViewById(R.id.aboutTextView);
        buttonAbout = findViewById(R.id.mesErr);
        iw.setOnClickListener(view -> {
            if (buttonAbout.getVisibility() == View.VISIBLE)
                buttonAbout.setVisibility(View.GONE);
            else buttonAbout.setVisibility(View.VISIBLE);
        });
        tw.setOnClickListener(view -> {
            if (buttonAbout.getVisibility() == View.VISIBLE)
                buttonAbout.setVisibility(View.GONE);
            else buttonAbout.setVisibility(View.VISIBLE);
        });
        buttonAbout.setOnClickListener(view -> {
            sendMessageActivity(this);
            mDrawerLayout.closeDrawers();

        });
    }

    /**
     * Инициализация бокового меню настроек с размерами текста, темы
     * дополнительной клавиатуры,и всякой другой херни
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initNavigationBar() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        historyB = this.findViewById(R.id.butHist);
        darkThemeB = this.findViewById(R.id.butDarkTheme);
        lightThemeB = this.findViewById(R.id.butLightTheme);
        switchBigKey = this.findViewById(R.id.memKeyBoard);
        switchVoiceKey = this.findViewById(R.id.voiceInputSwitch);
        seekBar = this.findViewById(R.id.seekBar);
        textViewSizeText = this.findViewById(R.id.menuTextSize);
        rbrepl = this.findViewById(R.id.rb_replace_voice);
        rbadd1 = this.findViewById(R.id.rb_add_voice);

        Button blog = this.findViewById(R.id.blog);
        Button clog = this.findViewById(R.id.clog);
        twlog = this.findViewById(R.id.twlog);

        initAbout();
        try {
            getStateKeyboard();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            getTextSizeState();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            getStateVoice();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (!isPortrait())
            Information.setOrientation(1);
        else
            Information.setOrientation(0);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setSeekBarProgressState(progress);
                setLocalStateTextSize(intToTextSize(progress));
                try {
                    saveTextSizeState();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seekBar.setProgress(textSizeToInt(getLocalStateTextSize()));
        seekBar.setOnTouchListener((v, event) -> {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow Drawer to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    break;

                case MotionEvent.ACTION_UP:
                    // Allow Drawer to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
            // Handle seekbar touch events.
            v.onTouchEvent(event);
            return true;
        });


        switchBigKey.setChecked(getLocalStateOfDopKeyboard());
        switchBigKey.setOnCheckedChangeListener((buttonView, isChecked) -> {
            fragmentButtonMain.setMemoryLayoutVisibility(isChecked);
            if (!isPortrait())
                fragmentButtonLandButton.setMemoryLayoutVisibility(isChecked);
            setStateOfDopKeyboard(isChecked);
            try {
                saveStateKeyboard();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            //mDrawerLayout.closeDrawers();
        });

        if (getLocalStateVoice() == null) {
            setLocalStateVoice(new voiceRecStruct(true, false, voiceRecStruct.methodOutputVoice.METHOD_VOICE_REPLACE));
        }

        switchVoiceKey.setOnCheckedChangeListener((buttonView, isChecked) -> {
//                    if (!isChecked)
//                        layoutVoice.setVisibility(View.GONE);
//                    else
//                        layoutVoice.setVisibility(View.VISIBLE);
            //rbadd1.setEnabled(isChecked);
            //rbrepl.setEnabled(isChecked);
            fragmentTextViews.setVoiceEnabled(isChecked);
            setRadioVoiceBlocks(isChecked);
            setLocalStateVoice(new voiceRecStruct(isChecked, getLocalStateVoice().isDontShow(), getLocalStateVoice().getDefaultVariant())); //то есть в локали меняем только параметр нужности, остальные берем старые
            try {
                saveStateVoice();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });


//        rbadd1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    setLocalStateVoice(new voiceRecStruct(getLocalStateVoice().isNeed(), getLocalStateVoice().isDontShow(), voiceRecStruct.methodOutputVoice.METHOD_VOICE_ADD));
//                    try {
//                        saveStateVoice();
//                    } catch (IOException ex) {
//                        ex.printStackTrace();
//                    }
//                }
//            }
//        });
//
//        rbrepl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    setLocalStateVoice(new voiceRecStruct(getLocalStateVoice().isNeed(), getLocalStateVoice().isDontShow(), voiceRecStruct.methodOutputVoice.METHOD_VOICE_REPLACE));
//                    try {
//                        saveStateVoice();
//                    } catch (IOException ex) {
//                        ex.printStackTrace();
//                    }
//                }
//            }
//        });

        switchVoiceKey.setChecked(getLocalStateVoice().isNeed());
        fragmentTextViews.setVoiceEnabled(getLocalStateVoice().isNeed());
//        if (!getLocalStateVoice().isNeed())
//            layoutVoice.setVisibility(View.GONE);
//        else
//            layoutVoice.setVisibility(View.VISIBLE);
        setRadioVoiceBlocks(getLocalStateVoice().isNeed());

//        switch (getLocalStateVoice().getDefaultVariant()){
//            case METHOD_VOICE_ADD: rbadd1.setChecked(true); break;
//            case METHOD_VOICE_REPLACE: rbrepl.setChecked(true);break;
//        }
        clog.setOnClickListener(v -> {
            // Logger.clearFile();
//
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"blkdev@ya.ru"});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Отчет об ошибке");
            //emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Logger.getFilePath()));
            try {
                emailIntent.putExtra(Intent.EXTRA_TEXT, SingletonLogger.getLog());
            } catch (IOException e) {
                e.printStackTrace();
            }
            emailIntent.setType("application/octet-stream");
            try {
                startActivity(Intent.createChooser(emailIntent, "Send Email"));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
            twlog.setText("");
        });
        blog.setOnClickListener(v -> {
            try {
                twlog.setText(SingletonLogger.getLog());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        historyB.setOnClickListener(v -> {
            showMyDialog(IDD_LIST_HISTORY);
            mDrawerLayout.closeDrawers();
        });
        darkThemeB.setOnClickListener(v -> {
            //generateMessage();
            setTheme(typeOfStyleTheme.DARK_THEME);
            mDrawerLayout.closeDrawers();
        });
        lightThemeB.setOnClickListener(v -> {
            setTheme(typeOfStyleTheme.LIGHT_THEME);
            mDrawerLayout.closeDrawers();
        });

        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                        Log.d(TAG, "onDrawerSlide on" + String.valueOf(slideOffset));
                    }

                    @Override
                    public void onDrawerOpened(@NonNull View drawerView) {
                        // Respond when the drawer is opened
                        Log.d(TAG, "onDrawerOpened");
                    }

                    @Override
                    public void onDrawerClosed(@NonNull View drawerView) {
                        // Respond when the drawer is closed
                        Log.d(TAG, "onDrawerClosed");
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                        Log.d(TAG, "onDrawerStateChanged");
                    }
                }
        );


    }


    /**
     * Сериализированное сохранение состояния дополнительной клавиатуры
     *
     * @throws IOException - оберточный трай для исключения
     */
    private void saveStateKeyboard() throws IOException {
        FileOutputStream fos = new FileOutputStream(this.getFilesDir() + "keyb.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeBoolean(getLocalStateOfDopKeyboard());
        oos.flush();
        oos.close();
    }

    /**
     * Десериализация состояния дополнительной клавиатуры
     *
     * @throws IOException - оберточный трай для исключения
     */
    private void getStateKeyboard() throws IOException {
        FileInputStream fis;
        try {
            fis = new FileInputStream(this.getFilesDir() + "keyb.out");
        } catch (FileNotFoundException fnf) {
            FileOutputStream fos = new FileOutputStream(this.getFilesDir() + "keyb.out");
            fos.close();
            fis = new FileInputStream(this.getFilesDir() + "keyb.out");

        }
        ObjectInputStream oin = new ObjectInputStream(fis);
        setStateOfDopKeyboard(oin.readBoolean());
    }

    private voiceRecStruct getLocalStateVoice() {
        return vcr;
    }

    private void setLocalStateVoice(voiceRecStruct vs) {
        vcr = vs;
    }

    /**
     * Сериализированное сохранение состояния голосового ввода
     *
     * @param - структура - состояние
     * @throws IOException - оберточный трай для исключения
     */
    private void saveStateVoice() throws IOException {
        FileOutputStream fos = new FileOutputStream(this.getFilesDir() + "voiceparams.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(getLocalStateVoice());
        //oos.writeBoolean(getLocalStateOfDopKeyboard());
        oos.flush();
        oos.close();
    }

    /**
     * Десериализация состояния голосового ввода
     *
     * @throws IOException - оберточный трай для исключения
     */
    private void getStateVoice() throws IOException {
        FileInputStream fis;
        try {
            fis = new FileInputStream(this.getFilesDir() + "voiceparams.out");
        } catch (FileNotFoundException fnf) {
            FileOutputStream fos = new FileOutputStream(this.getFilesDir() + "voiceparams.out");
            fos.close();
            fis = new FileInputStream(this.getFilesDir() + "voiceparams.out");
        }
        ObjectInputStream oin = new ObjectInputStream(fis);
        voiceRecStruct stru = null;
        try {
            stru = (voiceRecStruct) oin.readObject();


        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        setLocalStateVoice(stru);
    }


    /**
     * Сеттер состояния доп клавиатуры
     *
     * @param state -  состояние
     */
    private void setStateOfDopKeyboard(boolean state) {
        stateOfDopKeyboard = state;
    }

    /**
     * Геттер состояния клавиатуры
     *
     * @return - состояние
     */
    private boolean getLocalStateOfDopKeyboard() {
        return stateOfDopKeyboard;
    }

    /**
     * Конвертер числового значения(0-3) к типу текстового размера
     *
     * @param tot значение
     * @return размер текста
     */
    private textSize intToTextSize(int tot) {
        switch (tot) {
            case 0:
                return textSize.TEXT_SIZE_SMALL;
            case 1:
                return textSize.TEXT_SIZE_MEDIUM;
            case 2:
                return textSize.TEXT_SIZE_BIG;
            case 3:
                return textSize.TEXT_SIZE_FUCKIN_LARGE;
            default:
                return textSize.TEXT_SIZE_MEDIUM;
        }
    }

    /**
     * Конвертер текстового размера к числовому значению(0-3)
     *
     * @param ts - тип текстовго размера
     * @return число (0-3)
     */
    private int textSizeToInt(textSize ts) {
        if (ts == null) return 2;
        switch (ts) {
            case TEXT_SIZE_SMALL:
                return 0;
            case TEXT_SIZE_MEDIUM:
                return 1;
            case TEXT_SIZE_BIG:
                return 2;
            case TEXT_SIZE_FUCKIN_LARGE:
                return 3;
            default:
                return 2;
        }
    }

    /**
     * Сериализация состояния размера текста
     *
     * @throws IOException - оберточный трай для исключения
     */
    private void saveTextSizeState() throws IOException {
        FileOutputStream fos = new FileOutputStream(this.getFilesDir() + "textS.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeInt(textSizeToInt(getLocalStateTextSize()));
        oos.flush();
        oos.close();
    }

    /**
     * Десериализация состояния размера текста
     *
     * @throws IOException - оберточный трай для исключения
     */
    private void getTextSizeState() throws IOException {

        FileInputStream fis;
        try {
            fis = new FileInputStream(this.getFilesDir() + "textS.out");
        } catch (FileNotFoundException fnf) {
            FileOutputStream fos = new FileOutputStream(this.getFilesDir() + "textS.out");
            fos.close();
            fis = new FileInputStream(this.getFilesDir() + "textS.out");
        }


        ObjectInputStream oin = new ObjectInputStream(fis);
        setLocalStateTextSize(intToTextSize(oin.readInt()));
    }


//    public voiceRecStruct.methodOutputVoice getVrs() throws IOException {
//        FileInputStream fis;
//        try {
//            fis = new FileInputStream(this.getFilesDir() + "vrs.out");
//        }
//        catch (FileNotFoundException fnf){
//            FileOutputStream fos = new FileOutputStream( this.getFilesDir() + "vrs.out");
//            fos.close();
//            fis = new FileInputStream(this.getFilesDir() + "vrs.out");
//        }
//        ObjectInputStream oin = new ObjectInputStream(fis);
//        int val = oin.readInt();
//        switch (val){
//            case 1: Information.setVrs(voiceRecStruct.methodOutputVoice.METHOD_VOICE_ADD);
//            case 2: Information.setVrs(voiceRecStruct.methodOutputVoice.METHOD_VOICE_REPLACE);
//            case -1: Information.setVrs(voiceRecStruct.methodOutputVoice.METHOD_VOICE_NOT_INITIALIZED);
//            default: Information.setVrs(voiceRecStruct.methodOutputVoice.METHOD_VOICE_NOT_INITIALIZED);
//        }
//    }
//
//    public  void saveVrc() throws IOException {
//        FileOutputStream fos = new FileOutputStream( this.getFilesDir() + "vrs.out");
//        ObjectOutputStream oos = new ObjectOutputStream(fos);
//        oos.writeInt(Information.getVrs());
//        oos.flush();
//        oos.close();
//    }

    /**
     * Геттер размера текста (локального)
     *
     * @param ts - размер текста
     */
    private void setLocalStateTextSize(textSize ts) {
        textsize = ts;
    }

    /**
     * Сеттер размера текста
     *
     * @return размер текста
     */
    private textSize getLocalStateTextSize() {
        return textsize;
    }
//
//    @Override
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case IDD_LIST_HISTORY: {
//                historyDialog = new HistoryDialog(this);
//                return historyDialog.getBuilder().create();
//            }
//
//            default:
//                return null;
//        }
//    }

    /**
     * Инициализация диалога с историей вычислений
     * @param id - айди диалога
     * @param dialog - диалог
     */
//
//    @Override
//    protected void onPrepareDialog(int id, final Dialog dialog) {
//        super.onPrepareDialog(id, dialog);{
//            historyDialog.setDialog((AlertDialog) dialog);
//            HistoryListAdapter adapter = new HistoryListAdapter(this, parser.historyList.getList(),parser);
//            ListView lw = Objects.requireNonNull(dialog.getWindow()).findViewById(
//                    R.id.listViewsa);
//            Objects.requireNonNull(lw.getDivider()).setColorFilter(DrawableStyleManager.getParams().textOperationColor, PorterDuff.Mode.SRC_ATOP);
//            lw.setAdapter(adapter);
//            dialog.getWindow().setBackgroundDrawableResource(DrawableStyleManager.getParams().dialogHistoryBackground);
//            int textViewId = dialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
//            TextView tv =  dialog.findViewById(textViewId);
//            tv.setTextColor((getResources().getColor(DrawableStyleManager.getParams().dialogHistoryButtonColor)));
//
//            Button positiveButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_POSITIVE);
//            positiveButton.setTextColor(this.getResources().getColor(DrawableStyleManager.getParams().dialogHistoryButtonColor));
//            positiveButton.invalidate();
//
//            Button negativeButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
//            negativeButton.setTextColor(this.getResources().getColor(DrawableStyleManager.getParams().dialogHistoryButtonColor));
//            negativeButton.invalidate();
//
//            Button neutralButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_NEUTRAL);
//            neutralButton.setTextColor(this.getResources().getColor(DrawableStyleManager.getParams().dialogHistoryButtonColor));
//            neutralButton.invalidate();
//
//        }
//    }

    /**
     * Слушатель нажатия на диалоговые копки
     *
     * @param thdb - тип копки
     */
    public void onButtonDialogPress(typeOfDialogButton thdb) {
        switch (thdb) {
            case DIALOG_BUTTON_CLEAR: {
                parser.hist_getDbHelper().deleteAll();
                parser.getHistoryList().clearHistory();
                break;
            }
            case DIALOG_BUTTON_OK: {
                if (parser.getHistoryList().getList().size() != parser.getHistoryList().getDeletedList().size()) {
                    parser.getHistoryList().confirmDeletedList();
                    parser.hist_getDbHelper().replaceDBFromList(parser.getHistoryList().getList());
                }
                break;
            }
            case DIALOG_BUTTON_CANCEL: {
                parser.getHistoryList().setDeletedList(parser.getHistoryList().getList());
                break;
            }
        }
    }

    /**
     * Слушатель нажатия на диалоговые кнопки на каждый элемент списка ( методы выгрузить, и удалить)
     * @param type - тип кнопки
     * @param itemid - айдишник списка
     */
   /* public void onButtonClick(typeOfHistoryItemButton type,int itemid){
        if (type == typeOfHistoryItemButton.BUTTON_DIALOG_ITEM_DELETE){
            Log.d(TAG, "onButtonClick: 1" +  "ID = " + String.valueOf(itemid));
            parser.historyList.removeItem(itemid);
            // потому что айди в листе начинаются с 0 , а айди в базе с 1 TODO Удалять нужно не по одноиу элменту а по итогу кучей
            HistoryListAdapter adapter = new HistoryListAdapter(this, parser.historyList.getDeletedList(),parser);
            ListView lw = Objects.requireNonNull(historyDialog.getDialog().getWindow()).findViewById(
                    R.id.listViewsa);
            lw.setAdapter(adapter);
            if (parser.historyList.getDeletedList().isEmpty()) {
                onButtonDialogPress(typeOfDialogButton.DIALOG_BUTTON_OK);
                historyDialog.getDialog().cancel();
            }

        }
        else if (type == typeOfHistoryItemButton.BUTTON_DIALOG_ITEM_UPLOAD){
            Log.d(TAG, "onButtonClick: 2"+ String.valueOf(itemid));
            parser.hist_replaceFromHistory(itemid);
            if (historyDialog.getDialog() != null)
                historyDialog.getDialog().cancel();
        }
    }*/

    /**
     * Сдушатель кнопок доп клавиатуры, передат в основной слушатель для единой обработки всех кнопок
     *
     * @param buttonType - тип кнопки
     */
    @Override
    public void onButtonSelectedDop(typeOfAction buttonType) {
        onButtonSelected(buttonType);
    }


    /**
     * Слушатель парсера, реализующий переключение системы счисисления на нужную, в случае выгрузки из истории выражения с иной системой счисления, что установлена в системе.
     *
     * @param tons - тип системы счисления
     */
    @Override
    public void onTypeOfNumberSystemChanged(typeOfNumberSystem tons) {
        if (fragmentButtonLandButton != null)
            fragmentButtonLandButton.setRadioNumberSystem(tons);
    }


    @Override
    public void onButtonVoiceDialogPress(typeOfDialogVoiceButton thdb, ArrayList<String> list, boolean isDontShow) {
        Log.i("VOICEDIALOG", "VOICE" + thdb);
        // vd.hide();

        switch (thdb) {
            case DIALOG_BUTTON_ADD: {
                parser.replaceFromVoice(Parse.parseManualArrayList(list.get(0)), true);
                //setLocalStateVoice(new voiceRecStruct(true, isDontShow, voiceRecStruct.methodOutputVoice.METHOD_VOICE_ADD));
                break;
            }
            case DIALOG_BUTTON_REPLACE: {
                parser.replaceFromVoice(Parse.parseManualArrayList(list.get(0)), false);
                //setLocalStateVoice(new voiceRecStruct(true, isDontShow, voiceRecStruct.methodOutputVoice.METHOD_VOICE_REPLACE));
                break;
            }
            case DIALOG_BUTTON_TRY_AGAIN: {
                listenToSpeech();
            }
            case DIALOG_BUTTON_CANCEL:
        }
//        switch (getLocalStateVoice().getDefaultVariant()) {
//            case METHOD_VOICE_ADD:
//                rbadd1.setChecked(true);
//                break;
//            case METHOD_VOICE_REPLACE:
//                rbrepl.setChecked(true);
//                break;
//        }
        //vd.getDialog().cancel();
    }

    @Override
    public void onItemPress(typeOfHistoryItemButton thdb, int index, ArrayList<CalcList> list) {
        Log.d("NEWHISTORY", thdb + "  " + index);
        switch (thdb) {
            case BUTTON_DIALOG_ITEM_DELETE: {
                Log.d("NEWHISTORY", "ITEM DEL");
                if (list.contains(null)) {
                    Information.clearHistoryList();
                    ArrayList<CalcList> bl = new ArrayList<>();
                    for (CalcList cl : list) {
                        if (cl != null)
                            bl.add(cl);
                    }
                    Information.setHistoryList(bl);

                    parser.hist_getDbHelper().replaceDBFromList(Information.getHistoryList());
                    parser.getHistoryList().setList(Information.getHistoryList());
                }
                break;
            }
            case BUTTON_DIALOG_ITEM_UPLOAD: {
                parser.hist_replaceFromHistory(index);
                hd.dismiss();
                Log.d("NEWHISTORY", "ITEM UPLOAD");
                break;
            }
            case BUTTON_CLEAR_ALL: {
                Log.d("NEWHISTORY", "CLEAR ALL");
                Information.clearHistoryList();
                hd.dismiss();
                parser.hist_getDbHelper().replaceDBFromList(Information.getHistoryList());
                parser.getHistoryList().setList(Information.getHistoryList());
                break;
            }
            case BUTTON_CLOSE: {

                Log.d("NEWHISTORY", "CLOSE");
                hd.dismiss();
                break;
            }
        }
    }

}
