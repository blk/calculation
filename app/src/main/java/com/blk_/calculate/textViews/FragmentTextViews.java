package com.blk_.calculate.textViews;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.blk_.calculate.Information;
import com.blk_.calculate.R;
import com.blk_.calculate.enums.StateRadDegr;
import com.blk_.calculate.enums.textAction;
import com.blk_.calculate.enums.textSize;
import com.blk_.calculate.logger.SingletonLogger;
import com.blk_.calculate.styleTheme.StyleThemeParams;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FragmentTextViews extends Fragment implements View.OnClickListener {
    private final String LOG_TAG = "LOG FR TEXT";
    private static StateRadDegr stateInput = StateRadDegr.TYPE_INPUT_RADIAN;
    private EditText textView;
    private TextView textResult;
    private TextView textViewMem;
    private TextView textErrScob;
    private ImageButton btn_voice;
    private HorizontalScrollView mScrollView;
    private HorizontalScrollView mScrollView2;
    private TextView textViewRadDeg;
    //private TextView hist;
    private ImageButton hist;
    private Activity currentActivity;
    private View view;
    private OnSelectedButtonListener fragmentButtonListener;
    private OnSelectedButtonServiceListener fragmentButtonServiceListener;


    public interface OnSelectedButtonListener {
        void onButtonSelected(StateRadDegr srd);
    }

    public interface OnSelectedButtonServiceListener {
        void onButtonServiceSelected(textAction id);
    }

    private textAction textActionByID(int id) {
        switch (id) {
            case 1:
                return textAction.TEXTMEM;
            case 2:
                return textAction.TEXTPARENTHESIS;
            case 3:
                return textAction.TEXTLONGPARENTHESIS;
            case R.id.buttonHistory:
                return textAction.TEXTHISTORY;
        }
        return textAction.TEXTPARENTHESIS;
    }

    @Override
    public void onClick(View view) {

        fragmentButtonServiceListener.onButtonServiceSelected(textActionByID(view.getId()));
    }

    public EditText getTextView() {
        return textView;
    }

    private TextView getTextResult() {
        return textResult;
    }

    public TextView getTextErrScob() {
        return textErrScob;
    }


    public void onManualChangeText(textSize ts) {
        float size = Information.getTextSizeByName(ts);
        Log.d(LOG_TAG, "!!!TEXTCHANGE MANUAL = " + String.valueOf(size));
        textView.setTextSize(size * 2);
        textResult.setTextSize(size);

        //textSysDegr.setTextSize(size/2);

        //textViewRadDeg.setTextSize(size/2);
        //textErrScob.setTextSize(size/2);
        //textViewMem.setTextSize(size/2);
    }

    public void onAttach(Context context) {
        Log.d(LOG_TAG, "Fragment1 onAttach");
        Activity a;
        a = (Activity) context;
        currentActivity = a;
        super.onAttach(context);
        try {
            fragmentButtonListener = (OnSelectedButtonListener) a;
            fragmentButtonServiceListener = (OnSelectedButtonServiceListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString() + " must implement onSomeEventListener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmenttextviews, container, false);
        return view;
    }

    public boolean isPortrait() {
        return (currentActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }

    private void updateRadDegr() {
        if (Information.getIsNeedButtonRadian()) {
            textViewRadDeg.setVisibility(View.VISIBLE);
        } else
            textViewRadDeg.setVisibility(View.INVISIBLE);
    }

    private void setTextToTextView(TextView tw, TextFragmentStruct tfs) {
        if (tfs.getSpanStr() == null) {
            if (tfs.getValue() != null)
                tw.setText(tfs.getValue());
        } else
            tw.setText(tfs.getSpanStr());
    }

    public void listenerChangingText(TextFragmentStruct tfs, Animation animation) {
        switch (tfs.getField()) {
            case TEXT_VIEW:
                setTextToTextView(textView, tfs);
                if (animation != null) {
                    getTextView().startAnimation(animation);
                }
                break;
            case TEXT_RESULT:
                setTextToTextView(textResult, tfs);
                if (animation != null) {
                    getTextResult().startAnimation(animation);
                }
                break;
            case TEXT_PARENTHESIS:
                setTextToTextView(textErrScob, tfs);
                animateDegree();
                break;

            case TEXT_UPDATE:
                scrollToBottom();
                updateRadDegr();
                break;
            case TEXT_CLEAR: {
                textView.setText("");
                textResult.setText("");
                textViewRadDeg.setText("");
                textErrScob.setText("");
                break;
            }
        }
    }

    private void animateDegree() {


        if (textErrScob.getText().length() != 0) {
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(1000); //You can manage the time of the blink with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            textErrScob.startAnimation(anim);
        }
    }

    private void scrollToBottom() {
        //if (textView.getSelectionStart() == textView.getText().length())
        //Log.d(LOG_TAG,"size textview = " + String.valueOf(textView.getWidth()));
        mScrollView.post(() -> mScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT));
        mScrollView2.post(() -> mScrollView2.fullScroll(HorizontalScrollView.FOCUS_RIGHT));
    }

    public void setButtonDegreeRadian() {

        if (stateInput == StateRadDegr.TYPE_INPUT_DEGREE)
            textViewRadDeg.setText(getString(R.string.textdegree));
        if (stateInput == StateRadDegr.TYPE_INPUT_RADIAN)
            textViewRadDeg.setText(getString(R.string.textradian));
    }

    public void initElements() {

        DisplayMetrics metrics = new DisplayMetrics();
        currentActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        stateInput = StateRadDegr.TYPE_INPUT_RADIAN;
        textView = currentActivity.findViewById(R.id.textViewExpr);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, Information.getDefaultTextViewSize());
        textView.setOnLongClickListener(view -> false);
        //
        textView.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                menu.clear();
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                return false;
            }
        });

        textView.setOnClickListener(v -> {
            Log.d(LOG_TAG, "OnClickListener");
            textView.getSelectionEnd(); // если нет выделения группы текста (а его нет оно запрещено) то возвращает позицию курсора
            Log.d(LOG_TAG, String.valueOf(textView.getSelectionEnd()));
        });

        if (Build.VERSION.SDK_INT >= 21) {
            textView.setShowSoftInputOnFocus(false);
        } else
            try {
                final Method method = EditText.class.getMethod(
                        "setShowSoftInputOnFocus"
                        , boolean.class);
                method.setAccessible(true);
                method.invoke(textView, false);
            } catch (Exception e) {
                // ignore
            }

        btn_voice = view.findViewById(R.id.btn_voice);
//        try {
//            Status.setDefaultTextSize(textView.getTextSize() / metrics.density);
//        } catch (NullPointerException e)
//        {
//            Status.setDefaultTextSize(30);
//        }
        textViewMem = currentActivity.findViewById(R.id.textMemView);
        textResult = currentActivity.findViewById(R.id.textViewResultat);
        textErrScob = currentActivity.findViewById(R.id.textViewErrScob);
        textView.setMovementMethod(new ScrollingMovementMethod());
//        TextViewCompat.setAutoSizeTextTypeWithDefaults(textView,
//                TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                Log.d("SUBSTR", s.toString());
                Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
                paint.setStyle(Paint.Style.FILL);
                paint.setTextAlign(Paint.Align.CENTER);
                paint.setTextSize(textView.getTextSize()); //размер шрифта в **пикселях**
                Log.d(LOG_TAG, "  text width(px) = " + String.valueOf(paint.measureText(textView.getText().toString())) +
                        " text width(sp) = " + String.valueOf(textView.getTextSize() / currentActivity.getResources().getDisplayMetrics().scaledDensity) +
                        " screen width pixels (px) = " + String.valueOf(currentActivity.getResources().getDisplayMetrics().widthPixels) +
                        " textView width(px) =" + String.valueOf(textView.getWidth())
                );
                float pxScreen = currentActivity.getResources().getDisplayMetrics().widthPixels;
                float pxTextView = textView.getWidth();
                float pxText = paint.measureText(textView.getText().toString());
                float spText = textView.getTextSize() / currentActivity.getResources().getDisplayMetrics().scaledDensity;
                changeTextSize(spText, pxText, pxScreen, pxTextView);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.d("f", "d");


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(LOG_TAG, "onTextChanged");
            }
        });
        mScrollView = currentActivity.findViewById(R.id.scroller);
        mScrollView2 = currentActivity.findViewById(R.id.scroller2);
        textViewRadDeg = currentActivity.findViewById(R.id.butChangeRadian);
//        if (!isPortrait())
//            buttonRadDeg.setVisibility(View.VISIBLE);
//        else
//            buttonRadDeg.setVisibility(View.INVISIBLE);
        textViewRadDeg.setOnClickListener(v -> {
            if (stateInput == StateRadDegr.TYPE_INPUT_DEGREE) {
                stateInput = StateRadDegr.TYPE_INPUT_RADIAN;
                Information.setStateRadDegr(stateInput);
                textViewRadDeg.setText(R.string.textradian);
            } else {
                stateInput = StateRadDegr.TYPE_INPUT_DEGREE;
                Information.setStateRadDegr(stateInput);
                textViewRadDeg.setText(R.string.textdegree);
            }
            fragmentButtonListener.onButtonSelected(stateInput);
        });
        hist = currentActivity.findViewById(R.id.buttonHistory);
        //TextView set = (TextView) currentActivity.findViewById(R.id.buttonSettings);
        hist.setOnClickListener(this);

        textViewMem.setOnClickListener(view -> fragmentButtonServiceListener.onButtonServiceSelected(textAction.TEXTMEM));

        textErrScob.setOnClickListener(view -> fragmentButtonServiceListener.onButtonServiceSelected(textAction.TEXTPARENTHESIS));
        textErrScob.setOnLongClickListener(view -> {
            fragmentButtonServiceListener.onButtonServiceSelected(textAction.TEXTLONGPARENTHESIS);
            return false;
        });
        //set.setOnClickListener(this);
    }

    private void changeTextSize(float textSizeSP, float pxText, float pxScreen, float pxTextView) {
        // defaultTextSize больше его нельзя
        int stepChangeSP = 1;
        float sizeText = textSizeSP;
        //
        if ((textSizeSP >= Information.getMinimumTextSize()) && (pxText < pxScreen) && (textSizeSP != Information.getDefaultTextViewSize())) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
            paint.setStyle(Paint.Style.FILL);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTextSize(sizeText * currentActivity.getResources().getDisplayMetrics().scaledDensity);

            while ((paint.measureText(textView.getText().toString()) < pxScreen) && /*(paint.getText <= textView.getHeight()) &&*/ (sizeText <= Information.getDefaultTextViewSize())) {
                sizeText += stepChangeSP;
                paint.setTextSize(sizeText * currentActivity.getResources().getDisplayMetrics().scaledDensity);
                //paint.setTextSize((textSizeSP+stepChangeSP)/getResources().getDisplayMetrics().scaledDensity);
            }
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeText);
            return;
        } else if ((pxText > pxScreen) && (textSizeSP > Information.getMinimumTextSize())) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
            paint.setStyle(Paint.Style.FILL);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTextSize(sizeText * currentActivity.getResources().getDisplayMetrics().scaledDensity);

            while ((paint.measureText(textView.getText().toString()) > pxScreen) && (sizeText > Information.getMinimumTextSize())) {

                sizeText -= stepChangeSP;
                if (sizeText == 29)
                    Log.wtf("f", "sdf");
                paint.setTextSize(sizeText * currentActivity.getResources().getDisplayMetrics().scaledDensity);
                //paint.setTextSize((textSizeSP+stepChangeSP)/getResources().getDisplayMetrics().scaledDensity);
            }
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeText);
            return;

        }


    }

    public void clear() {
        textView.setText("");
        textResult.setText("");
        textErrScob.setText("");

    }

    public void setMemory(String val) {
        textViewMem.setText(val);
    }

    public void cleanMemory() {
        textViewMem.setText("");
    }

    public void setStyle(StyleThemeParams params) {//int colorTextID, int resultColor){

        //LinearLayout ll = (LinearLayout)  view.findViewById(R.id.textLayM);
        view.setBackgroundResource(params.drawableTextID);
        textResult.setTextColor(currentActivity.getResources().getColor(params.textResultColor));

        textViewRadDeg.setTextColor(params.textOperationColor);
        textErrScob.setTextColor(currentActivity.getResources().getColor(params.parserColorConst));
        textViewMem.setTextColor(params.textOperationColor);
        //btn_voice.setBackgroundResource(params.microColor);
        btn_voice.setImageResource(params.drawableMicro);
        //TextView set = (TextView) currentActivity.findViewById(R.id.buttonSettings);
        hist.setImageResource(params.drawableHistory);

        setCursorDrawableColor(textView, params.textOperationColor);

        View sl = view.findViewById(R.id.viewslide);
        sl.setBackgroundResource(params.drawableminislider);
        //set.setBackgroundResource(params.drawableNumericID);
    }

    private static void setCursorDrawableColor(EditText editText, int color) {
        try {
            Field fCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            fCursorDrawableRes.setAccessible(true);
            int mCursorDrawableRes = fCursorDrawableRes.getInt(editText);
            Field fEditor = TextView.class.getDeclaredField("mEditor");
            fEditor.setAccessible(true);
            Object editor = fEditor.get(editText);
            Class<?> clazz = editor.getClass();
            Field fCursorDrawable = clazz.getDeclaredField("mCursorDrawable");
            fCursorDrawable.setAccessible(true);
            Drawable[] drawables = new Drawable[2];
            drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            drawables[1].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            fCursorDrawable.set(editor, drawables);
        } catch (Throwable ex) {
            Log.i("gg", ex.getMessage());
            Log.i("gg", ex.getLocalizedMessage());
            //MainActivity.onException(ignored);
            SingletonLogger.getInstance().onException(ex);
        }
    }

    public void setVoiceEnabled(boolean state) {
        if (state)
            btn_voice.setVisibility(View.VISIBLE);
        else btn_voice.setVisibility(View.INVISIBLE);
    }
}
