package com.blk_.calculate.textViews;

import android.text.SpannableStringBuilder;

public class TextFragmentStruct {
    public enum textField {
        TEXT_VIEW,
        TEXT_RESULT,
        TEXT_PARENTHESIS,
        TEXT_LPARENTHESIS,
        TEXT_RADDEGR,
        TEXT_DEGREE,
        TEXT_UPDATE,
        TEXT_CLEAR
    }

    private textField field;
    private String value;

    public SpannableStringBuilder getSpanStr() {
        return spanStr;
    }

    public void setSpanStr(SpannableStringBuilder spanStr) {
        this.spanStr = spanStr;
    }

    private SpannableStringBuilder spanStr;


    public TextFragmentStruct(textField field, String value, SpannableStringBuilder str) {
        this.field = field;
        this.value = value;
        this.spanStr = str;
    }

    public textField getField() {
        return field;
    }

    public void setField(textField field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}

