//package com.blk_.calculate.Logger;
//
//import android.text.format.DateFormat;
//import android.util.Log;
//
//import com.blk_.calculate.Information;
//import com.blk_.calculate.enums.typeOfAction;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.HashSet;
//
//public class Logger {
//    private static String pathApp;
//    private static HashSet<String> abonents;
//    private static final String log1 = "log1";
//    private static final String log2 = "log2";
//    private static final String log3 = "log3";
//    private static final String log4 = "log4";
//    private static final String log5 = "log5";
//    private static String path;
//
//    public static String getFilePath() {
//        return pathApp + path;
//    }
//    public static void initPathforLog(String filedir) {
//        pathApp = filedir;
//        abonents = new HashSet<>();
//    }
//
//    public static void clearFile() {
//        File file = new File(pathApp + log1);
//        FileOutputStream fos;
//        try {
//            fos = new FileOutputStream(file);
//            fos.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void initLogger(String abonent) throws IOException {
//        if (pathApp == null)
//            throw new IOException();
//        //registrateAbonent(abonent);
//        ArrayList<String> listFiles = new ArrayList<>();
//        listFiles.add(log1);
//        listFiles.add(log2);
//        listFiles.add(log3);
//        listFiles.add(log4);
//        listFiles.add(log5);
//        // Пока с одним лог файлом работаем
//        path = listFiles.get(0);
//
//        // }
//    }
//
//    public static void checkLog() throws IOException {
//        String result = "";
//        try {
//            result = Logger.getLog();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        if (!result.isEmpty()) {
//            ArrayList<String> lst = new ArrayList<>(Arrays.asList(result.split("\n")));
//            //test
//            Log.d("LOGSIZE = ", String.valueOf(lst.size()));
//            if (lst.size() > Information.getMaxLogSize()) {
//                lst = new ArrayList<>(lst.subList((lst.size() - Information.getMaxLogSize() + Information.getMaxLogSize() / 10), lst.size()));
//                File file = new File(pathApp + path);
//                FileOutputStream fos = null;
//                try {
//                    fos = new FileOutputStream(file, false);
//                    for (String el : lst) {
//                        fos.write(el.getBytes());
//                    }
//                    //String out = new String((currentDateTimeString + "[  " + toa + "  ]" + "\n").getBytes(), "utf-8");
//                    fos.close();
//                } catch (FileNotFoundException ex) {
//                    ex.printStackTrace();
//                }
//
//            }
//        }
//    }
//
//    public static void writeException(ArrayList<String> exList) throws IOException {
//        File file = new File(pathApp + path);
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(file, true);
//            for (String el : exList) {
//                el = el + "\n";
//                fos.write(el.getBytes());
//            }
//            //String out = new String((currentDateTimeString + "[  " + toa + "  ]" + "\n").getBytes(), "utf-8");
//            fos.close();
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//        }
//
//    }
//    public static void writeToLog(String abonent, typeOfAction toa, String value) throws IOException {
//        String currentDateTimeString = (String) DateFormat.format("[dd-MM_kk:mm]", new Date());
//        if (abonents.contains(abonent)) {
//            if (value == null)
//                value = "";
//            File file = new File(pathApp + path);
//            FileOutputStream fos = new FileOutputStream(file, true);
//            String out = new String((currentDateTimeString + "[" + toa + "]" + "\n").getBytes(), "utf-8");
//            fos.write(out.getBytes());
//            fos.close();
//        }
//    }
//
//    private static String getLog() throws IOException {
//        FileInputStream fis;
//        try {
//            fis = new FileInputStream(pathApp + path);
//        } catch (FileNotFoundException fnf) {
//            FileOutputStream fos = new FileOutputStream(pathApp + path);
//            fos.close();
//            fis = new FileInputStream(pathApp + path);
//        }
//
//        byte[] t = new byte[fis.available()];
//        fis.read(t);
//        return new String(t);
//    }
//}
