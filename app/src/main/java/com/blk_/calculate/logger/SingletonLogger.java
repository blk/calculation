package com.blk_.calculate.logger;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;

import com.blk_.calculate.Information;
import com.blk_.calculate.activities.MainActivity;
import com.blk_.calculate.enums.typeOfAction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.mail.MessagingException;

public class SingletonLogger {
    private static volatile SingletonLogger instance;
    private static int countWrites;


    private static ArrayList<String> notWritedList;
    private final static String path = "calculateLog.log";
    private static String pathApp;

    public static SingletonLogger getInstance() {
        if (instance == null)
            synchronized (SingletonLogger.class) {
                if (instance == null) {
                    instance = new SingletonLogger();
                    countWrites = 0;
                    notWritedList = new ArrayList<>();
                }
            }
        return instance;
    }

    public String getFileName() {
        if (!pathApp.isEmpty())
            return pathApp + path;
        else return "";
    }

    private Context context;

    private SingletonLogger() {
    }

    public void initLogger(String pathApplication, Context context) {
        pathApp = pathApplication;
        this.context = context;
    }

    public static void clearFile() {
        File file = new File(pathApp + path);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(file);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isCritical(Throwable ex) {
        //return true;
        return !(ex.getClass().equals(NoSuchFieldException.class)
                || (ex.getClass().equals(NumberFormatException.class))
                || (ex.getClass().equals(MessagingException.class)));
    }

    public void onException(Throwable exception) {
        boolean isCritical = isCritical(exception);
        notWritedList.add(DateFormat.format("[dd-MM-kk:mm]", new Date()) + "BEGIN EXCEPTION");
        notWritedList.add(exception.getMessage());
        for (int i = 0; i < exception.getStackTrace().length; i++) {
            notWritedList.add(exception.getStackTrace()[i].toString());
        }
        notWritedList.add(DateFormat.format("[dd-MM-kk:mm]", new Date()) + "END EXCEPTION");
        boolean state = false;
        try {
            state = writeException(notWritedList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (state)
            notWritedList.clear();
        if (context != null && isCritical) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Ой! Произошла ошибка")
                    .setMessage(exception.getMessage() + "\n" +
                            "\nОтправить сообщение об ошибке разработчикам?")
                    .setCancelable(false)
                    .setNegativeButton("Не,так норм",
                            (dialog, id) -> dialog.cancel())
                    .setPositiveButton("Они обязательно должны знать!",
                            (dialog, id) -> {
                                dialog.cancel();
                                MainActivity.sendMessageActivity(context);
                            });

            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private static boolean writeException(ArrayList<String> exList) throws IOException {
        if (!pathApp.isEmpty()) {
            File file = new File(pathApp + path);
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(file, true);
                for (String el : exList) {
                    el = el + "\n";
                    fos.write(el.getBytes());
                }
                //String out = new String((currentDateTimeString + "[  " + toa + "  ]" + "\n").getBytes(), "utf-8");
                fos.close();

                countWrites++;
                if (countWrites > Information.getMaxLogSize()) {
                    try {
                        checkLog();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return true;

            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                return false;
            }
        } else {
            Log.d("LOGGER", "Try to write to not-inited Logger!");
            return false;
        }
    }

    public static void writeToLog(typeOfAction toa, String value) throws IOException {

        String currentDateTimeString = (String) DateFormat.format("[ddMMkkmm]", new Date());
        if (value == null)
            value = "";

        notWritedList.add(new String((currentDateTimeString + "[" + typeOfAction.getShortValue(toa) + "]" + "[" + Information.getCurTextPos() + "]" +
                (Information.getOrientation() == 0 ? "[P]" : "[L]")
                + "\n").getBytes(), "utf-8"));
        if (!pathApp.isEmpty()) {
            File file = new File(pathApp + path);
            FileOutputStream fos = new FileOutputStream(file, true);
            for (String el : notWritedList)
                fos.write(el.getBytes());

            fos.close();

            countWrites++;
            if (countWrites > Information.getMaxLogSize()) {
                try {
                    checkLog();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Log.d("LOGGER", "Try to write to not-inited Logger!");
        }
    }

    private static void checkLog() throws IOException {
        String result = "";
        try {
            result = getLog();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!result.isEmpty()) {
            ArrayList<String> lst = new ArrayList<>(Arrays.asList(result.split("\n")));
            //test
            Log.d("LOGSIZE = ", String.valueOf(lst.size()));
            if (lst.size() > Information.getMaxLogSize()) {
                lst = new ArrayList<>(lst.subList((lst.size() - Information.getMaxLogSize() + Information.getMaxLogSize() / 10), lst.size()));
                File file = new File(pathApp + path);
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file, false);
                    for (String el : lst) {
                        fos.write(el.getBytes());
                    }
                    fos.close();
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }

            }
        }
    }

    public static boolean isLogAvailable() {
        try {
            return !(pathApp.isEmpty() || getLog().isEmpty());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;

    }

    public static String getLog() throws IOException {
        if (!pathApp.isEmpty()) {
            FileInputStream fis;
            try {
                fis = new FileInputStream(pathApp + path);
            } catch (FileNotFoundException fnf) {
                FileOutputStream fos = new FileOutputStream(pathApp + path);
                fos.close();
                fis = new FileInputStream(pathApp + path);
            }

            byte[] t = new byte[fis.available()];
            fis.read(t);
            return new String(t);
        }
        return "";
    }
}
