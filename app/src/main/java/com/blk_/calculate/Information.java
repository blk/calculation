package com.blk_.calculate;

import android.util.Log;

import com.blk_.calculate.enums.StateRadDegr;
import com.blk_.calculate.enums.textSize;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.historylist.CalcList;
import com.blk_.calculate.math.Calculate;
import com.blk_.calculate.math.Converter;

import java.util.ArrayList;

/**
 * Класс - носитель статичных данных будь то состояния кнопок нужных при перезапуске , размер текста текущий, ресурсы кнопок, разные статичные конвертеры
 */
public final class Information {

    private static typeOfNumberSystem currentTypeOfSystem = typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL;
    private static int orientation = 0;
    private static StateRadDegr stateRadDegr = StateRadDegr.TYPE_INPUT_RADIAN;
    private static boolean isNeedButtonRadian = false;
    private static float defaultTextSize = 40;
    private static float minimumTextSize = 30;
    private static final float portraitSmallTextSize = 20;
    private static final float portraitMediumTextSize = 25;
    private static final float portraitBigTextSize = 30;
    private static final float portraitFLargeTextSize = 35;

    private static final float landscapeSmallTextSize = 8;
    private static final float landscapeMediumTextSize = 11;
    private static final float landscapeBigTextSize = 14;
    private static final float landscapeFLargeTextSize = 17;

    private static final int maxLogSize = 2000;

    public static int getMaxLogSize() {
        return maxLogSize;
    }

    /**
     * Геттер текущей системы счисления
     *
     * @return - текущая система счисления
     */
    public static typeOfNumberSystem getCurrentTypeOfSystem() {
        Log.d("STATUS", "public static typeOfNumberSystem getCurrentTypeOfSystem()");
        return currentTypeOfSystem;
    }

    /**
     * Сеттер текущей системы счисления
     *
     * @param currentTypeOfSystem - система счисления
     */
    public static void setCurrentTypeOfSystem(typeOfNumberSystem currentTypeOfSystem) {
        Log.d("STATUS", "public static setCurrentTypeOfSystem()");
        Information.currentTypeOfSystem = currentTypeOfSystem;
    }

    /**
     * Сеттер ориентации
     *
     * @param or - ориентация (0-портретная, 1- альбомная))
     */
    public static void setOrientation(int or) {
        orientation = or;//0-portrait
    }

    public static int getOrientation() {
        return orientation;
    }

    /**
     * геттер stateRadDegree
     *
     * @return - состояние отображающая тип используемых чисел для тригонметрии - радианы или градусы
     */
    public static StateRadDegr getStateRadDegr() {
        Log.d("STATUS", "public static StateRadDegr getStateRadDegr()");
        return stateRadDegr;
    }

    /**
     * Сеттер состояния радиан - градусов
     *
     * @param stateRadDegr - состояние
     */
    public static void setStateRadDegr(StateRadDegr stateRadDegr) {
        Log.d("STATUS", "public static setStateRadDegr(StateRadDegr stateRadDegr) ");
        Information.stateRadDegr = stateRadDegr;
    }


    /**
     * Сеттер признака нужно ли отображать кнопку радиан - градусов
     *
     * @param status признак
     */
    public static void setIsNeedButtonRadian(boolean status) {
        isNeedButtonRadian = status;
    }

    /**
     * Геттер признака отображения кнопки радиан градусов
     *
     * @return -  признак отображения кнопки радиан - градусов
     */
    public static boolean getIsNeedButtonRadian() {
        return isNeedButtonRadian;
    }

    /**
     * Сеттер деволтного размера текста
     *
     * @param defaultTextSize - размер текста
     */
    public static void setDefaultTextSize(float defaultTextSize) {
        Information.defaultTextSize = defaultTextSize;
    }

    /**
     * геттер дефолтного размера текста для отображения
     *
     * @return размер текста
     */
    public static float getDefaultTextViewSize() { // Возвращает размер текста только для вьюшки, не для всего остального
        return defaultTextSize * 2;
    }

    /**
     * Геттер минимального размера текста
     *
     * @return - размер текста
     */
    public static float getMinimumTextSize() {
        return minimumTextSize;
    }


    /**
     * Возвращает размер текста в соответствии с его названием
     *
     * @param ts - название
     * @return - размер текста
     */
    public static float getTextSizeByName(textSize ts) {
        switch (ts) {
            case TEXT_SIZE_SMALL:
                return orientation == 0 ? portraitSmallTextSize : landscapeSmallTextSize;
            case TEXT_SIZE_MEDIUM:
                return orientation == 0 ? portraitMediumTextSize : landscapeMediumTextSize;
            case TEXT_SIZE_BIG:
                return orientation == 0 ? portraitBigTextSize : landscapeBigTextSize;
            case TEXT_SIZE_FUCKIN_LARGE:
                return orientation == 0 ? portraitFLargeTextSize : landscapeFLargeTextSize;
        }
        return orientation == 0 ? portraitMediumTextSize : landscapeMediumTextSize;
    }

    /**
     * Возвращает тип элемента, строковое представление которого передается в параметрах
     *
     * @param element - элемент
     * @return - тип
     */
    public static typeOfAction getTypeOFAction(String element) {
        if (!element.isEmpty()) {
            if (Calculate.isDegree(element)) return typeOfAction.ACTION_DEGREE;
            if (Calculate.isPlus(element)) return typeOfAction.ACTION_PLUS;
            if (Calculate.isMinus(element)) return typeOfAction.ACTION_MINUS;
            if (Calculate.isUmnoz(element)) return typeOfAction.ACTION_UMNOZ;
            if (Calculate.isDelen(element)) return typeOfAction.ACTION_DELEN;
            //if (Calculate.isCommand(element)) return typeOfAction.ACTION_OPERATION;
            if (Calculate.isFactorial(element)) return typeOfAction.ACTION_FACTORIAL;
            if (Calculate.isPercent(element)) return typeOfAction.ACTION_PERCENT;
            if (Calculate.isSQRTelement(element)) return typeOfAction.ACTION_SQRT;
            if (Calculate.isSin(element)) return typeOfAction.ACTION_SINX;
            if (Calculate.isCos(element)) return typeOfAction.ACTION_COSX;
            if (Calculate.isLn(element)) return typeOfAction.ACTION_LN;
            if (Calculate.isLg(element)) return typeOfAction.ACTION_LG;
            if (Calculate.isConstPi(element)) return typeOfAction.ACTION_CONST_PI;
            if (Calculate.isConstExp(element)) return typeOfAction.ACTION_CONST_E;
            if (Calculate.isOpeningParenthesis(element))
                return typeOfAction.ACTION_OPENING_PARENTHESIS;
            if (Calculate.isClosingParenthesis(element))
                return typeOfAction.ACTION_CLOSING_PARENTHESIS;
            if (element.equals(".")) return typeOfAction.ACTION_FLOAT;
            if (element.equals("CL")) return typeOfAction.ACTION_CLEAR_LAST;
            if (element.equals("CLA")) return typeOfAction.ACTION_CLEAR;
            if (element.equals("1DX")) return typeOfAction.ACTION_1DX;
            if (element.equals("XSQ")) return typeOfAction.ACTION_SUBOPERATION_X2;
            if (Calculate.isNumeric(element)) return typeOfAction.ACTION_NUMERIC;
        }
        return typeOfAction.UNKNOWN_ACTION;
    }

    /**
     * Функция перевода целочисленного айди в тип typeOfActon (тип действия)
     *
     * @param id - айдишник
     * @return - тип действия которому соответствует айдишник
     */
    public static typeOfAction translateIdToIndex(int id) {
        typeOfAction index = typeOfAction.UNKNOWN_ACTION;
        switch (id) {

            case R.id.buttonNumericA:
                return typeOfAction.ACTION_NUMERICA;
            case R.id.buttonNumericB:
                return typeOfAction.ACTION_NUMERICB;
            case R.id.buttonNumericC:
                return typeOfAction.ACTION_NUMERICC;
            case R.id.buttonNumericD:
                return typeOfAction.ACTION_NUMERICD;
            case R.id.buttonNumericE:
                return typeOfAction.ACTION_NUMERICE;
            case R.id.buttonNumericF:
                return typeOfAction.ACTION_NUMERICF;
            case R.id.radioButtonBin:
                return typeOfAction.ACTION_RADIO_BIN;
            case R.id.radioButtonOct:
                return typeOfAction.ACTION_RADIO_OCT;
            case R.id.radioButtonDec:
                return typeOfAction.ACTION_RADIO_DEC;
            case R.id.radioButtonHex:
                return typeOfAction.ACTION_RADIO_HEX;
            case R.id.buttonOperationLg:
                return typeOfAction.ACTION_LG;
            case R.id.buttonOperationLn:
                return typeOfAction.ACTION_LN;
            case R.id.buttonNumericConstPi:
                return typeOfAction.ACTION_CONST_PI;
            case R.id.buttonNumericConstE:
                return typeOfAction.ACTION_CONST_E;
            case R.id.buttonOperationOpeningParenthesis:
                return typeOfAction.ACTION_OPENING_PARENTHESIS;
            case R.id.buttonOperationClosingParenthesis:
                return typeOfAction.ACTION_CLOSING_PARENTHESIS;
            case R.id.buttonOperationPlusMinus:
                return typeOfAction.ACTION_MINUS/*ACTION_NUMERIC_PLUS_MINUS*/;
            case R.id.buttonOperationFactorial:
                return typeOfAction.ACTION_FACTORIAL;
            case R.id.buttonOperationSin:
                return typeOfAction.ACTION_SINX;
            case R.id.buttonOperationCos:
                return typeOfAction.ACTION_COSX;
            case R.id.buttonOperationSQRT:
                return typeOfAction.ACTION_SQRT;
            case R.id.buttonOperationStepen:
                return typeOfAction.ACTION_DEGREE;
            case R.id.buttonOperationXDegrMinusOne:
                return typeOfAction.ACTION_1DX;
            case R.id.buttonOperationSQRT2X:
                return typeOfAction.ACTION_SUBOPERATION_X2;
            case R.id.buttonNumeric0:
                return typeOfAction.ACTION_NUMERIC0;
            case R.id.buttonNumeric1:
                return typeOfAction.ACTION_NUMERIC1;
            case R.id.buttonNumeric2:
                return typeOfAction.ACTION_NUMERIC2;
            case R.id.buttonNumeric3:
                return typeOfAction.ACTION_NUMERIC3;
            case R.id.buttonNumeric4:
                return typeOfAction.ACTION_NUMERIC4;
            case R.id.buttonNumeric5:
                return typeOfAction.ACTION_NUMERIC5;
            case R.id.buttonNumeric6:
                return typeOfAction.ACTION_NUMERIC6;
            case R.id.buttonNumeric7:
                return typeOfAction.ACTION_NUMERIC7;
            case R.id.buttonNumeric8:
                return typeOfAction.ACTION_NUMERIC8;
            case R.id.buttonNumeric9:
                return typeOfAction.ACTION_NUMERIC9;
            case R.id.buttonOperationUmnoz:
                return typeOfAction.ACTION_UMNOZ;
            case R.id.buttonOperationPlus:
                return typeOfAction.ACTION_PLUS;
            case R.id.buttonOperationMinus:
                return typeOfAction.ACTION_MINUS;
            case R.id.buttonOperationDelen:
                return typeOfAction.ACTION_DELEN;
            case R.id.buttonOperationPercent:
                return typeOfAction.ACTION_PERCENT;
            case R.id.buttonOperationSumary:
                return typeOfAction.ACTION_SUMMARY;
            case R.id.buttonClear:
                return typeOfAction.ACTION_CLEAR;
            case R.id.buttonNumericZ:
                return typeOfAction.ACTION_FLOAT;
            case R.id.buttonDeleteLastSymbol:
                return typeOfAction.ACTION_CLEAR_LAST;
            //case R.id.buttonSettings:                   return typeOfAction.BUTTON_SETTINGS;
            case R.id.buttonMemoryUpload:
                return typeOfAction.BUTTON_MEMORY_UPLOAD;
            case R.id.buttonMemoryClear:
                return typeOfAction.BUTTON_MEMORY_CLEAR;
            case R.id.buttonMemoryPLus:
                return typeOfAction.BUTTON_MEMORY_PLUS;
            case R.id.buttonMemoryMinus:
                return typeOfAction.BUTTON_MEMORY_MINUS;
        }
        return index;
    }

    public static typeOfAction getNumeric(String el) {
        switch (el) {
            case "0":
            case "нуль":
            case "ноль":
                return typeOfAction.ACTION_NUMERIC0;
            case "1":
            case "один":
                return typeOfAction.ACTION_NUMERIC1;
            case "2":
            case "два":
                return typeOfAction.ACTION_NUMERIC2;
            case "3":
            case "три":
                return typeOfAction.ACTION_NUMERIC3;
            case "4":
            case "четыре":
                return typeOfAction.ACTION_NUMERIC4;
            case "5":
            case "пять":
                return typeOfAction.ACTION_NUMERIC5;
            case "6":
            case "шесть":
                return typeOfAction.ACTION_NUMERIC6;
            case "7":
            case "семь":
                return typeOfAction.ACTION_NUMERIC7;
            case "8":
            case "восемь":
                return typeOfAction.ACTION_NUMERIC8;
            case "9":
            case "девять":
                return typeOfAction.ACTION_NUMERIC9;
            case "A":
                return typeOfAction.ACTION_NUMERICA;
            case "B":
                return typeOfAction.ACTION_NUMERICB;
            case "C":
                return typeOfAction.ACTION_NUMERICC;
            case "D":
                return typeOfAction.ACTION_NUMERICD;
            case "E":
                return typeOfAction.ACTION_NUMERICE;
            default:
                return typeOfAction.ACTION_NUMERIC;
        }
    }

    /**
     * Возвращает строковое представление элемента по его типу
     *
     * @param toa - тип элемента
     * @return - строковое представление
     */
    public static String getStringByTypeOfAction(typeOfAction toa) {
        switch (toa) {
            case ACTION_NUMERIC0:
                return Converter.convertAdapter("0", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC1:
                return Converter.convertAdapter("1", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC2:
                return Converter.convertAdapter("2", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC3:
                return Converter.convertAdapter("3", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC4:
                return Converter.convertAdapter("4", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC5:
                return Converter.convertAdapter("5", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC6:
                return Converter.convertAdapter("6", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC7:
                return Converter.convertAdapter("7", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC8:
                return Converter.convertAdapter("8", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERIC9:
                return Converter.convertAdapter("9", typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERICA:
                return Converter.convertAdapter("A", typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERICB:
                return Converter.convertAdapter("B", typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERICC:
                return Converter.convertAdapter("C", typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERICD:
                return Converter.convertAdapter("D", typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERICE:
                return Converter.convertAdapter("E", typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_NUMERICF:
                return Converter.convertAdapter("F", typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL, Information.getCurrentTypeOfSystem());
            case ACTION_PLUS:
                return "+";
            case ACTION_MINUS:
                return "-";
            case ACTION_UMNOZ:
                return "\u00D7";
            case ACTION_DELEN:
                return "/";
            case ACTION_DEGREE:
                return "^";
            case ACTION_SQRT:
                return "\u221A";
            case ACTION_PERCENT:
                return "%";
            case ACTION_NUMERIC_PLUS_MINUS:
                return "-";
            case ACTION_FLOAT:
                return ".";
            case ACTION_OPENING_PARENTHESIS:
                return "(";
            case ACTION_CLOSING_PARENTHESIS:
                return ")";
            case ACTION_FACTORIAL:
                return "!";
            case ACTION_SINX:
                return "sin";
            case ACTION_COSX:
                return "cos";
            case ACTION_LG:
                return "lg";
            case ACTION_LN:
                return "ln";
            case ACTION_CONST_PI:
                return "\u03C0";
            case ACTION_CONST_E:
                return "exp";
            case ACTION_SUMMARY:
                return "=";
            case ACTION_CLEAR_LAST:
                return "CL";
            case ACTION_CLEAR:
                return "CLA";
            case ACTION_1DX:
                return "1DX";
            case ACTION_SUBOPERATION_X2:
                return "XSQ";
        }
        return "";
    }

    /**
     * Возвращает строковое представление констангты по её названию
     *
     * @param toa - название
     * @return - стрковое представление
     */
    public static String getStringForConstByTypeOfAction(typeOfAction toa) {
        switch (toa) {
            case ACTION_CONST_PI: {
                if (Information.getStateRadDegr() == StateRadDegr.TYPE_INPUT_RADIAN)
                    return "\u03C0";
                else return "\u03C0";
            }
            case ACTION_CONST_E:
                return "e";
        }
        return "";
    }

    /**
     * возвращает строковое представление системы счисления по её типу
     * (нужно для тоаст уведомления при выгрузке из истории выражения в системе счисления, некорректной по отношению к текущей ориентации экрана
     *
     * @param tons- тип системы счисления
     * @return - строковое представление
     */
    public static String getStringByTypeOfNumSystem(typeOfNumberSystem tons) {
        switch (tons) {
            case NUMBER_SYSTEM_BINARY:
                return App.getContext().getResources().getString(R.string.tosBINString);
            case NUMBER_SYSTEM_OCTAL:
                return App.getContext().getResources().getString(R.string.tosOSCString);
            case NUMBER_SYSTEM_DECIMAL:
                return App.getContext().getResources().getString(R.string.tosDECString);
            case NUMBER_SYSTEM_HEXADECIMAL:
                return App.getContext().getResources().getString(R.string.tosHEXString);
            default:
                return "";
        }
    }

    private static ArrayList<CalcList> historyList;

    public static String getStringForViewNaN() {
        return App.getContext().getResources().getString(R.string.nanString);
    }

    public static String getStringForViewInfinity() {
        return App.getContext().getResources().getString(R.string.infinityString);
    }

    public static int getAccuracy() {
        return 10;
    }

    private static final int capacityDatabase = 100;

    public static int getCapacityDatabase() {
        return capacityDatabase;
    }

    public static double getAprocsimateCoefficient() {
        return 0.9948862056176134;
    }

    public static void setHistoryList(ArrayList<CalcList> lst) {
        historyList = new ArrayList<>(lst);
    }

    public static void clearHistoryList() {
        historyList.clear();
    }

    private static int pos = -1;

    public static int getCurTextPos() {
        return pos;
    }

    public static void setCurTextPos(int posf) {
        pos = posf;
    }

    public static ArrayList<CalcList> getHistoryList() {
        return new ArrayList<>(historyList);
    }
}
