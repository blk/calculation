package com.blk_.calculate.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.blk_.calculate.Information;
import com.blk_.calculate.R;
import com.blk_.calculate.enums.textSize;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.logger.SingletonLogger;
import com.blk_.calculate.styleTheme.StyleThemeParams;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by blk_ on 22.03.2018.
 * Фрагмент.
 * Отвечает за основную раскладку кнопок при портретном режиме и альбомном, имеет кнопочки
 */

public class FragmentBasicSetButtons extends Fragment implements View.OnClickListener {
    private final String LOG_TAG = "LOG_MAIN";              //Строковая константа для печати логов(тэг)
    private ArrayList<Button> buttonsN;                     // TODO массив числовых кнопок (не сказал бы что знаю зачем но пусть пока будет
    private ArrayList<Button> buttonsO;                     // TODO массив числовых кнопок (не сказал бы что знаю зачем но пусть пока будет
    private View view;                                      // ссылка на текущую view ,оно нам нужно чтобы мы смогли найти элементы лэйаута по айди
    private OnSelectedButtonListener fragmentMainLiistener; // слушатель нажатий кнопки , с которым работает внешний слушатель
    private LinearLayout memoryLayout;                      // скрываемая клавиатура памяти (м+ м- мс мр)
    private Button btnslider;                               // кнопка - язычок для вытягивания дополнительной клавиатуры
    private DrawerLayout drawerLayout;                      // раскладка клавиатуры дополнительных функций , выезжающая справа
    private static final String LOGGER_TAG = "FRAGMENT BASIC SET BUTTONS";

    /**
     * Геттер для раскладки клавиатуры доп функций
     *
     * @return сама раскладка
     */
    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    /**
     * Геттер для раскладки кнопок с цифрами(только)
     *
     * @return коллекция кнопок
     */
    public ArrayList<Button> getButtonsN() {
        Log.d(LOG_TAG, " getButtonsN() {");
        return buttonsN;
    }

    /**
     * @param view -  цветовые параметры
     *             Обработчик клика на кнопку айдишник переводит в тип typeOfAction и передает слушателю (в данном проекте -  активити)
     */
    @Override
    public void onClick(View view) {
        Log.d(LOG_TAG, "onClick(View view)");
        fragmentMainLiistener.onButtonSelected(Information.translateIdToIndex(view.getId()));
        try {
            SingletonLogger.writeToLog(Information.translateIdToIndex(view.getId()), "");
            //Logger.writeToLog(LOGGER_TAG, Information.translateIdToIndex(view.getId()), "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Интерфейс для внешнего слушателя
     */
    public interface OnSelectedButtonListener {
        void onButtonSelected(typeOfAction buttonType);
    }

    /**
     * Переопределенный метод класса Фрагмент , вроде как вызывается при создании (по циклу жизни фрагмента)
     *
     * @param context context
     */
    @Override
    public void onAttach(Context context) {
        Log.d(LOG_TAG, "onAttach(Context context) {");
        Activity a;
        a = (Activity) context;
//        try {
//            Logger.initLogger(LOGGER_TAG);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        super.onAttach(context);
        try {
            fragmentMainLiistener = (OnSelectedButtonListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString() + " must implement onSomeEventListener");
        }
    }

    /**
     * Переопределенный метод класса Фрагмент.
     *
     * @param inflater           inflater
     * @param container          container
     * @param savedInstanceState savedInstanceState
     * @return view
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(LOG_TAG, "onCreateView(LayoutInflater inflater, ViewGroup container,\n" +
                "                             Bundle savedInstanceState) {");
        view = inflater.inflate(R.layout.fragmentbasicsetbuttons, container, false);
        return view;
    }

    /**
     * Еще один "при создании" сюда передается Бандл, в котором можно сохранить состояние приложения при смене орентации
     *
     * @param savedInstanceState bundle
     */
    public void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreate(Bundle savedInstanceState");
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");
    }

    /**
     * внизу куча переопределенных методов, хз зачем
     *
     * @param savedInstanceState bundle
     */
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "onActivityCreated");
    }

    /**
     * Инициализация и установка слушателей элементов экрана фрагмента.
     */
    @SuppressLint("CutPasteId")
    public void initialElements() {
        drawerLayout = (DrawerLayout) view;
        Log.d(LOG_TAG, "шnitialElements()");
        buttonsN = new ArrayList<>(16);
        buttonsN.add(view.findViewById(R.id.buttonNumeric0));
        buttonsN.add(view.findViewById(R.id.buttonNumeric1));
        buttonsN.add(view.findViewById(R.id.buttonNumeric2));
        buttonsN.add(view.findViewById(R.id.buttonNumeric3));
        buttonsN.add(view.findViewById(R.id.buttonNumeric4));
        buttonsN.add(view.findViewById(R.id.buttonNumeric5));
        buttonsN.add(view.findViewById(R.id.buttonNumeric6));
        buttonsN.add(view.findViewById(R.id.buttonNumeric7));
        buttonsN.add(view.findViewById(R.id.buttonNumeric8));
        buttonsN.add(view.findViewById(R.id.buttonNumeric9));
        buttonsO = new ArrayList<>();
        buttonsO.add(view.findViewById(R.id.buttonNumericZ));
        buttonsO.add(view.findViewById(R.id.buttonOperationDelen));
        buttonsO.add(view.findViewById(R.id.buttonOperationMinus));
        buttonsO.add(view.findViewById(R.id.buttonOperationSumary));
        buttonsO.add(view.findViewById(R.id.buttonOperationPlus));
        buttonsO.add(view.findViewById(R.id.buttonOperationUmnoz));
        buttonsO.add(view.findViewById(R.id.buttonOperationPercent));
        buttonsO.add(view.findViewById(R.id.buttonDeleteLastSymbol));
        buttonsO.add(view.findViewById(R.id.buttonClear));
        buttonsO.add(view.findViewById(R.id.buttonMemoryClear));
        buttonsO.add(view.findViewById(R.id.buttonMemoryPLus));
        buttonsO.add(view.findViewById(R.id.buttonMemoryMinus));
        buttonsO.add(view.findViewById(R.id.buttonMemoryUpload));
        btnslider = view.findViewById(R.id.buttonOperationSlider);
        memoryLayout = view.findViewById(R.id.memoryLayout);

        if (view.getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Button b = view.findViewById(R.id.buttonOperationSlider);
            b.setVisibility(View.GONE);
        }

        for (int i = 0; i < 10; i++) {
            buttonsN.get(i).setOnClickListener(this);
        }
        for (Button buf : buttonsO) {
            buf.setOnClickListener(this);
            if (buf.getId() == R.id.buttonDeleteLastSymbol) {
                buf.setOnLongClickListener(v -> {
                    buttonsO.get(buttonsO.indexOf(view.findViewById(R.id.buttonClear))).callOnClick();
                    return false;
                });
            }
        }
        btnslider.setOnClickListener(v -> drawerLayout.openDrawer(Gravity.END));
    }

    /**
     * метод меняющий размер текста кнопок пользователем
     *
     * @param ts - размер текста
     */
    public void changeTextButtonSize(textSize ts) {
        for (Button buf : buttonsN)
            buf.setTextSize(Information.getTextSizeByName(ts) * 15 / 10);
        for (Button buf : buttonsO)
            buf.setTextSize(Information.getTextSizeByName(ts) * 15 / 10);

    }

    /**
     * Метод установки текущей визуальной темы
     *
     * @param params -  цветовые параметры
     */
    public void setStyle(StyleThemeParams params) {
        for (Button buf : buttonsN) {
            buf.setBackgroundResource(params.drawableNumericID);
            buf.setTextColor(params.textNumericColor);
        }
        for (Button buf : buttonsO) {
            buf.setBackgroundResource(params.drawableOperationID);
            buf.setTextColor(params.textOperationColor);
        }
        Button buttonNumericZ = view.findViewById(R.id.buttonNumericZ);
        buttonNumericZ.setBackgroundResource(params.drawableNumericID);
        buttonNumericZ.setTextColor(params.textNumericColor);
        btnslider.setBackgroundResource(params.drawableForSlider);
        btnslider.setText("");
    }

    /**
     * Метод устанавливает видимость
     *
     * @param visibility - visibility
     */
    public void setMemoryLayoutVisibility(boolean visibility) {
        if (visibility)
            memoryLayout.setVisibility(View.VISIBLE);
        else
            memoryLayout.setVisibility(View.GONE);
    }
}