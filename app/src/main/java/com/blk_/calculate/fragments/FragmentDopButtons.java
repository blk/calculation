package com.blk_.calculate.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.blk_.calculate.Information;
import com.blk_.calculate.R;
import com.blk_.calculate.enums.textSize;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.logger.SingletonLogger;
import com.blk_.calculate.styleTheme.StyleThemeParams;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by blk_ on 20.03.2018.
 */

public class FragmentDopButtons extends Fragment implements View.OnClickListener {
    private final String LOG_TAG = "LOG_DOP";                           // Строковая константа для печати логов(тэг)
    private ArrayList<Button> buttonsN;                                 // TODO массив числовых кнопок (не сказал бы что знаю зачем но пусть пока будет
    private ArrayList<Button> buttonsO;                                 // TODO массив числовых кнопок (не сказал бы что знаю зачем но пусть пока будет
    private View view;                                                  // Ссылка на текущую view ,оно нам нужно чтобы мы смогли найти элементы лэйаута по айди

    private RadioButton radioButtonSystemNumeric[];                   // Массив радио кнопок
    private static typeOfNumberSystem currentNumberSystem               // Текущая система счисления
            = typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL;
    //private Button buttonOperationDegreeNum;                      // Ссылка на кнопку возведения в степень
    private OnSelectedButtonListener fragmentLandLiistener;             // слушатель нажатий кнопки , с которым работает внешний слушатель
    private LinearLayout memoryLayout;                                  // раскладка с кнопками памяти (м+ м- мс мр)
    private static final String LOGGER_TAG = "FRAGMENT BASIC SET LAND BUTTONS";

    /**
     * Установка видимости раскладки с кнопками памяти
     *
     * @param visibility - видимость
     */
    public void setMemoryLayoutVisibility(boolean visibility) {
        if (visibility)
            memoryLayout.setVisibility(View.VISIBLE);
        else
            memoryLayout.setVisibility(View.GONE);
    }

    /**
     * Программная установка текущей системы счисления. работает при выгрузке данных из бд пользователю на лицо
     *
     * @param tons - тип системы счисления
     */
    public void setRadioNumberSystem(typeOfNumberSystem tons) {
        switch (tons) {
            case NUMBER_SYSTEM_UNKNOWN:
                break;
            case NUMBER_SYSTEM_BINARY:
                radioButtonSystemNumeric[0].callOnClick();
                radioButtonSystemNumeric[0].setChecked(true);
                break;
            case NUMBER_SYSTEM_OCTAL:
                radioButtonSystemNumeric[1].callOnClick();
                radioButtonSystemNumeric[1].setChecked(true);
                break;
            case NUMBER_SYSTEM_DECIMAL:
                radioButtonSystemNumeric[2].callOnClick();
                radioButtonSystemNumeric[2].setChecked(true);
                break;
            case NUMBER_SYSTEM_HEXADECIMAL:
                radioButtonSystemNumeric[3].callOnClick();
                radioButtonSystemNumeric[3].setChecked(true);
                break;
        }
    }

    /**
     * Метод изменяющий размер текста кнопок пользователем.
     *
     * @param ts - размер текста
     */
    public void changeTextButtonSize(textSize ts) {
        for (Button buf : buttonsN) {
            buf.setTextSize(Information.getTextSizeByName(ts) * 15 / 10);
        }
        for (Button buf : buttonsO) {
            buf.setTextSize(Information.getTextSizeByName(ts) * 15 / 10);
        }
        for (RadioButton rb : radioButtonSystemNumeric)
            rb.setTextSize(Information.getTextSizeByName(ts));
        //buttonOperationDegreeNum.setTextSize(Information.getTextSizeByName(ts)*15/10);
    }

    /**
     * Геттер для кнопки степени
     * @return кнопка
     */
//    public Button getButtonOperationDegreeNum() {
//        Log.d(LOG_TAG,"getButtonOperationDegreeNum()");
//        //return buttonOperationDegreeNum;
//    }

    /**
     * Геттер для кнопок с цифрами.лол
     *
     * @return кнопки
     */
    public ArrayList<Button> getButtonsN() {
        Log.d(LOG_TAG, "getButtonsN() {");
        return buttonsN;
    }

    /**
     * Обработчик клика на кнопку (не на радио кнопку) айдишник переводит в тип typeOfAction и передает слушателю (в данном проекте -  активити)
     */
    @Override
    public void onClick(View view) {
        Log.d(LOG_TAG, "onClick(View view)");
        typeOfAction toa = Information.translateIdToIndex(view.getId());
        try {
            // Logger.writeToLog(LOGGER_TAG, Information.translateIdToIndex(view.getId()), "");
            SingletonLogger.writeToLog(Information.translateIdToIndex(view.getId()), "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        fragmentLandLiistener.onButtonSelected(toa);

    }

    /**
     * Интерфейс для внешнего слушателя
     */
    public interface OnSelectedButtonListener {
        void onButtonSelected(typeOfAction buttonType);
    }

    /**
     * Переопределенный метод класса Фрагмент , вроде как вызывается при создании (но это не точно, их три как минимум, чем они отличаются я хз)
     *
     * @param context context
     */
    public void onAttach(Context context) {
        Log.d(LOG_TAG, "onAttach(Context context) {");
        Activity a;
        a = (Activity) context;

        super.onAttach(context);
        try {
            fragmentLandLiistener = (FragmentDopButtons.OnSelectedButtonListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString() + " must implement onSomeEventListener");
        }
        this.setRetainInstance(false);
    }

    /**
     * Установка цветовой темы для этого фрагмента
     *
     * @param params структура параметров
     */
    public void setStyle(StyleThemeParams params) {
        for (Button buf : buttonsN) {
            buf.setBackgroundResource(params.drawableNumericID);
            buf.setTextColor(params.textNumericColor);
        }
        for (Button buf : buttonsO) {
            buf.setBackgroundResource(params.drawableOperationID);
            buf.setTextColor(params.textOperationColor);
        }
        // buttonOperationDegreeNum.setBackgroundResource(params.drawableToggleID);
        //  buttonOperationDegreeNum.setTextColor(params.textOperationColor);


        RadioGroup rg = view.findViewById(R.id.radioGroup);
        rg.setBackgroundResource(params.styleRadioID);
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{android.R.attr.state_enabled}
                },
                new int[]{
                        params.textNumericColor,
                        params.textOperationColor
                }
        );


        for (RadioButton rb : radioButtonSystemNumeric) {
            if (Build.VERSION.SDK_INT > 21) {
                rb.setButtonTintList(colorStateList);
                rb.setTextColor(params.textOperationColor);
            } else {
                rb.setTextColor(params.textOperationColor);
                // AppCompatRadioButton rba = (AppCompatRadioButton) rb; TODO: Продумать
                // rba.setSupportButtonTintList(colorStateList);
            }
        }

    }

    /**
     * Переопределенный метод класса Фрагмент. Вызывается при первой(?) отрисовке.
     *
     * @param inflater           inflater
     * @param container          container
     * @param savedInstanceState savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreateView");
        view = inflater.inflate(R.layout.fragmentdopbuttons, container, false);
        return view;
    }

    public void onStart() {
        super.onStart();
        radioButtonSystemNumeric[2].setChecked(true);
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    /**
     * Инициализация и установка слушателей элементов экрана фрагмента.
     */
    public void initialElements() {
        Log.d(LOG_TAG, "initialElements()");
        buttonsN = new ArrayList<>(6);
        buttonsN.add(view.findViewById(R.id.buttonNumericA));
        buttonsN.add(view.findViewById(R.id.buttonNumericB));
        buttonsN.add(view.findViewById(R.id.buttonNumericC));
        buttonsN.add(view.findViewById(R.id.buttonNumericD));
        buttonsN.add(view.findViewById(R.id.buttonNumericE));
        buttonsN.add(view.findViewById(R.id.buttonNumericF));
        buttonsO = new ArrayList<>();
        buttonsO.add(view.findViewById(R.id.buttonOperationOpeningParenthesis));
        buttonsO.add(view.findViewById(R.id.buttonOperationClosingParenthesis));
        buttonsO.add(view.findViewById(R.id.buttonOperationFactorial));
        buttonsO.add(view.findViewById(R.id.buttonOperationPlusMinus));
        buttonsO.add(view.findViewById(R.id.buttonOperationSin));
        buttonsO.add(view.findViewById(R.id.buttonOperationCos));
        buttonsO.add(view.findViewById(R.id.buttonOperationSQRT));
        buttonsO.add(view.findViewById(R.id.buttonOperationSQRT2X));
        buttonsO.add(view.findViewById(R.id.buttonOperationXDegrMinusOne));
        buttonsO.add(view.findViewById(R.id.buttonNumericConstPi));
        buttonsO.add(view.findViewById(R.id.buttonNumericConstE));
        buttonsO.add(view.findViewById(R.id.buttonOperationLg));
        buttonsO.add(view.findViewById(R.id.buttonOperationLn));
        buttonsO.add(view.findViewById(R.id.buttonOperationStepen));
        //buttonOperationDegreeNum = view.findViewById(R.id.buttonOperationStepen);
        memoryLayout = view.findViewById(R.id.memoryLayout);
        for (int i = 0; i < 6; i++)
            buttonsN.get(i).setOnClickListener(this);
        //buttonOperationDegreeNum.setOnClickListener(this);
        for (Button buf : buttonsO)
            buf.setOnClickListener(this);

        radioButtonSystemNumeric = new RadioButton[4];
        radioButtonSystemNumeric[0] = view.findViewById(R.id.radioButtonBin);
        radioButtonSystemNumeric[1] = view.findViewById(R.id.radioButtonOct);
        radioButtonSystemNumeric[2] = view.findViewById(R.id.radioButtonDec);
        radioButtonSystemNumeric[3] = view.findViewById(R.id.radioButtonHex);
        currentNumberSystem = typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL;
        View.OnClickListener radioButtonClickListener = v -> {
            Log.d(LOG_TAG, "OnClicKRadio");
            RadioButton rb = (RadioButton) v;
            switch (rb.getId()) {
                case R.id.radioButtonHex:
                    currentNumberSystem = typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL;
                    break;
                case R.id.radioButtonDec:
                    currentNumberSystem = typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL;
                    break;
                case R.id.radioButtonOct:
                    currentNumberSystem = typeOfNumberSystem.NUMBER_SYSTEM_OCTAL;
                    break;
                case R.id.radioButtonBin:
                    currentNumberSystem = typeOfNumberSystem.NUMBER_SYSTEM_BINARY;
                    break;

                default:
                    break;
            }
            Information.setCurrentTypeOfSystem(currentNumberSystem);
            try {
                SingletonLogger.writeToLog(Information.translateIdToIndex(rb.getId()), "");
            } catch (IOException e) {
                e.printStackTrace();
            }
            fragmentLandLiistener.onButtonSelected(Information.translateIdToIndex(rb.getId()));
        };
        for (RadioButton rb : radioButtonSystemNumeric) {
            rb.setOnClickListener(radioButtonClickListener);
            rb.setOnCheckedChangeListener((buttonView, isChecked) ->
                    Log.d(LOG_TAG, "Checked change on button " + buttonView.getText().toString() + "state " + String.valueOf(isChecked)));
        }
    }
}