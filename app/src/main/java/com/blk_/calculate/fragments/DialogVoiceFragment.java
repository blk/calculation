package com.blk_.calculate.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blk_.calculate.R;
import com.blk_.calculate.enums.typeOfDialogVoiceButton;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.parsing.Parse;
import com.blk_.calculate.styleTheme.DrawableStyleManager;

import java.util.ArrayList;

public class DialogVoiceFragment extends DialogFragment implements OnClickListener {

    private final String LOG_TAG = "myLogs";
    private ArrayList<String> list;

    public void setList(ArrayList<String> lst) {
        list = lst;
    }

    public interface OnButtonVoiceDialogPress {
        void onButtonVoiceDialogPress(typeOfDialogVoiceButton thdb, ArrayList<String> list, boolean isDontShow);
    }

    public OnButtonVoiceDialogPress getOnButtonListener() {
        return onButtonListener;
    }

    public void onAttach(Context context) {
        Log.d(LOG_TAG, "Fragment1 onAttach");
        Activity a;
        a = (Activity) context;
        super.onAttach(context);
        try {
            onButtonListener = (OnButtonVoiceDialogPress) a;
            //fragmentButtonServiceListener = (OnSelectedButtonServiceListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString() + " must implement onSomeEventListener");
        }
    }

    private CheckBox cb;
    private OnButtonVoiceDialogPress onButtonListener;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (list == null)
            list = new ArrayList<>();

        getDialog().setTitle("Title!");
        View v = inflater.inflate(R.layout.dialogvoicelayout, null);

        LinearLayout lay = v.findViewById(R.id.lll);

        TextView tw = v.findViewById(R.id.textviewvoice);
        ArrayList<String> voiceList = new ArrayList<>();
        if (list != null)
            voiceList = Parse.parseManualArrayList(list.get(0));

        tw.setText(
                Parse.parseView_staticParseListToSpanString
                        (voiceList, typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, null));
        lay.setMinimumHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        lay.setBackgroundResource(DrawableStyleManager.getParams().dialogHistoryBackground);


        TextView ttitle = v.findViewById(R.id.textTitle);
        ttitle.setTextColor(DrawableStyleManager.getParams().textOperationColor);

        Button btnAdd = v.findViewById(R.id.btn_voice_add);
        btnAdd.setTextColor(DrawableStyleManager.getParams().textOperationColor);
        Button btnreplace = v.findViewById(R.id.btn_voice_replace);
        btnreplace.setTextColor(DrawableStyleManager.getParams().textOperationColor);
        Button btnCancel = v.findViewById(R.id.btn_voice_cancel);
        btnCancel.setTextColor(DrawableStyleManager.getParams().textOperationColor);

        Button btnTryAgain = v.findViewById(R.id.btn_voice_tryagain);
        btnTryAgain.setTextColor(DrawableStyleManager.getParams().textOperationColor);


        cb = v.findViewById(R.id.check_voice);

        cb.setTextColor(DrawableStyleManager.getParams().textOperationColor);

        btnTryAgain.setOnClickListener(v14 -> {
            onButtonListener.onButtonVoiceDialogPress(typeOfDialogVoiceButton.DIALOG_BUTTON_TRY_AGAIN, list, cb.isChecked());
            dismiss();
        });
        btnAdd.setOnClickListener(v13 -> {
            //onButtonListener.OnButtonVoiceDialogPress(typeOfDialogButton.DIALOG_BUTTON_OK);
            onButtonListener.onButtonVoiceDialogPress(typeOfDialogVoiceButton.DIALOG_BUTTON_ADD, list, cb.isChecked());
            dismiss();
            //dialog.cancel();
        });

        btnreplace.setOnClickListener((View v12) -> {
            onButtonListener.onButtonVoiceDialogPress(typeOfDialogVoiceButton.DIALOG_BUTTON_REPLACE, list, cb.isChecked());
            //onButtonListener.OnButtonVoiceDialogPress(typeOfDialogButton.DIALOG_BUTTON_CLEAR);
            // dialog.cancel();
            dismiss();
        });
        btnCancel.setOnClickListener(v1 -> {
            onButtonListener.onButtonVoiceDialogPress(typeOfDialogVoiceButton.DIALOG_BUTTON_CANCEL, list, cb.isChecked());
            //onButtonListener.OnButtonVoiceDialogPress(typeOfDialogButton.DIALOG_BUTTON_CANCEL);
            //dialog.cancel();
            dismiss();
        });
        return v;
    }

    public void onClick(View v) {
        Log.d(LOG_TAG, "Dialog 1: " + ((Button) v).getText());
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Log.d(LOG_TAG, "Dialog 1: " + ((Button) dialog).getText());
        dismiss();
    }
}