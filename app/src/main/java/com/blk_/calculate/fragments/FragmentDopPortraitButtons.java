package com.blk_.calculate.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.blk_.calculate.Information;
import com.blk_.calculate.R;
import com.blk_.calculate.enums.textSize;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.logger.SingletonLogger;
import com.blk_.calculate.styleTheme.StyleThemeParams;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by blk_
 * Фрагмент.
 * Отвечает за дополнительную раскладку клавиатуры в портретном режиме, включает в себя все функции которые есть в альбомном режиме кроме перехода между различными системами счисления.
 */
public class FragmentDopPortraitButtons extends Fragment implements View.OnClickListener {
    private final String LOG_TAG = "LOG_DOP_PORT";                      // Строковая константа для печати логов(тэг)
    private ArrayList<Button> buttonsO;                                 // TODO массив числовых кнопок (не сказал бы что знаю зачем но пусть пока будет
    private View view;                                                  // Ссылка на текущую view ,оно нам нужно чтобы мы смогли найти элементы лэйаута по айди
    //private Button buttonOperationDegreeNum;                      // Ссылка на кнопку возведения в степень
    private Button buttonBack;                                          // Ссылка на кнопку возведения в степень
    private OnSelectedButtonListenerDop fragmentLandLiistener;          // слушатель нажатий кнопки , с которым работает внешний слушатель
    private DrawerLayout mDrawerLayout;                                // лейаут для кнопок
    private static final String LOGGER_TAG = "FRAGMENT BASIC SET PORTRAIT BUTTONS";
    /**
     * Геттер для кнопки степени
     * @return кнопка
     */
//    public Button getButtonOperationDegreeNum() {
//        Log.d(LOG_TAG,"getButtonOperationDegreeNum()");
//        return buttonOperationDegreeNum;
//    }


    /**
     * метод меняющий размер текста кнопок пользователем
     *
     * @param ts - размер текста
     */
    public void changeTextButtonSize(textSize ts) {
        // buttonOperationDegreeNum.setTextSize(Information.getTextSizeByName(ts));
        for (Button buf : buttonsO) {
            buf.setTextSize(Information.getTextSizeByName(ts));
        }
    }

    /**
     * Инициализация навигационной панели, если что весь фрагмент выезжающий
     *
     * @param dl - лейаут
     */
    public void initNavigationBar(DrawerLayout dl) {
        mDrawerLayout = dl;
        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(@NonNull View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(@NonNull View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );
    }

    /**
     * Обработчик клика на кнопку (не на радио кнопку) айдишник переводит в тип typeOfAction и передает слушателю (в данном проекте -  активити)
     */
    @Override
    public void onClick(View view) {

        Log.d(LOG_TAG, "onClick(View view)");
        //mDrawerLayout.closeDrawers();
        typeOfAction toa = Information.translateIdToIndex(view.getId());
        try {
            //Logger.writeToLog(LOGGER_TAG, Information.translateIdToIndex(view.getId()), "");
            SingletonLogger.writeToLog(Information.translateIdToIndex(view.getId()), "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        fragmentLandLiistener.onButtonSelectedDop(toa);
    }

    /**
     * Интерфейс для внешнего слушателя
     */
    public interface OnSelectedButtonListenerDop {
        void onButtonSelectedDop(typeOfAction buttonType);
    }

    /**
     * Переопределенный метод класса Фрагмент , вроде как вызывается при создании (но это не точно, их три как минимум, чем они отличаются я хз)
     *
     * @param context контекст
     */
    public void onAttach(Context context) {
        Log.d(LOG_TAG, "onAttach(Context context) {");
        Activity a;
        a = (Activity) context;
        super.onAttach(context);
        try {
            fragmentLandLiistener = (OnSelectedButtonListenerDop) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString() + " must implement onSomeEventListener");
        }
        this.setRetainInstance(false);
    }

    /**
     * Установка новых цветов на элементы в соответствии с новым стилем
     *
     * @param params - класс-структура с параметрами стиля
     */
    public void setStyle(StyleThemeParams params) {
        for (Button buf : buttonsO) {
            buf.setBackgroundResource(params.drawableOperationID);
            buf.setTextColor(params.textOperationColor);
        }
        // buttonOperationDegreeNum.setBackgroundResource(params.drawableToggleID);
        // buttonOperationDegreeNum.setTextColor(params.textOperationColor);
        buttonBack.setBackgroundResource(params.drawableOperationID);
        buttonBack.setTextColor(params.textResultColor);
    }

    /**
     * Переопределенный метод класса Фрагмент. Вызывается при первой(?) отрисовке.
     *
     * @param inflater           inflater
     * @param container          container
     * @param savedInstanceState savedInstanceState
     * @return мшуц
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreateView");
        view = inflater.inflate(R.layout.dopbuttonsportrait, container, false);
        return view;
    }

    /**
     * Инициализация и установка слушателей элементов экрана фрагмента.
     */
    public void initialElements() {
        Log.d(LOG_TAG, "initialElements()");
        buttonsO = new ArrayList<>();
        buttonsO.add(view.findViewById(R.id.buttonOperationOpeningParenthesis));
        buttonsO.add(view.findViewById(R.id.buttonOperationClosingParenthesis));
        buttonsO.add(view.findViewById(R.id.buttonOperationFactorial));
        buttonsO.add(view.findViewById(R.id.buttonOperationPlusMinus));
        buttonsO.add(view.findViewById(R.id.buttonOperationSin));
        buttonsO.add(view.findViewById(R.id.buttonOperationCos));
        buttonsO.add(view.findViewById(R.id.buttonOperationSQRT));
        buttonsO.add(view.findViewById(R.id.buttonOperationSQRT2X));
        buttonsO.add(view.findViewById(R.id.buttonOperationXDegrMinusOne));
        buttonsO.add(view.findViewById(R.id.buttonNumericConstPi));
        buttonsO.add(view.findViewById(R.id.buttonNumericConstE));
        buttonsO.add(view.findViewById(R.id.buttonOperationLg));
        buttonsO.add(view.findViewById(R.id.buttonOperationLn));
        buttonsO.add(view.findViewById(R.id.buttonOperationStepen));
        //buttonsO.add( view.findViewById(R.id.buttonOperationBack));
        // buttonOperationDegreeNum = view.findViewById(R.id.buttonOperationStepen);
        buttonBack = view.findViewById(R.id.buttonOperationBack);
        // buttonOperationDegreeNum.setOnClickListener(this);
        buttonBack.setOnClickListener(v -> mDrawerLayout.closeDrawers());
        for (Button buf : buttonsO)
            buf.setOnClickListener(this);
    }

}

