package com.blk_.calculate.test;

import android.util.Log;

import com.blk_.calculate.Information;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.math.BigDecimalBlk;
import com.blk_.calculate.parsing.AddingElement;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by blk on 11.03.2018.
 */
public class Test {
    private class expression {
        private final ArrayList<String> list;
        private final String result;

        expression(ArrayList<String> lst, String res) {
            list = lst;
            result = res;
        }

        String getResult() {
            return result;
        }

        ArrayList<String> getList() {
            return list;
        }
    }

    private ArrayList<expression> list;
    private static final String TAG = "TESTER";

    private void readTestFile(String filePath) {
        if (list == null)
            list = new ArrayList<>();
        try (FileInputStream fin = new FileInputStream(filePath)) {
            System.out.printf("File size: %d bytes \n", fin.available());
            ArrayList<String> outlist = new ArrayList<>();
            String res = "";
            byte[] t = new byte[fin.available()];
            fin.read(t);
            String str = new String(t);
            fin.close();
            //ArrayList <String>blist = str.split("\n");
            //char [] c = (char [])t;
            str = str.replace("−", "-");
            ArrayList<String> blist = new ArrayList<>(Arrays.asList(str.split("\n")));

            for (String bEl : blist) {
                ArrayList<String> bb = new ArrayList<>(Arrays.asList(bEl.split("")));
                int i = 0;
                boolean isRavno = false;
                while (i < bb.size()) {
                    typeOfAction toa = Information.getTypeOFAction(bb.get(i));
                    if (bb.get(i).equals("="))
                        isRavno = true;
                    if (!isRavno) {
                        if ((toa == typeOfAction.ACTION_NUMERIC) || (toa == typeOfAction.ACTION_FLOAT)) {
                            //outlist = (AddingElement.elements_addOperationToList(Information.getNumeric(bb.get(i)), new ListWithCusorPos(outlist, new Parse.textCursorStruct(outlist.size() - 1, 0)))).getList();
                            if (outlist.size() > 1) {
                                if (Information.getTypeOFAction(outlist.get(outlist.size() - 1)) == typeOfAction.ACTION_NUMERIC)
                                    outlist.set(outlist.size() - 1, outlist.get(outlist.size() - 1) + bb.get(i));
                                else
                                    outlist.add(bb.get(i));
                            } else {
                                outlist.add(bb.get(i));
                            }
                            i++;
                            continue;
                        } else if (Information.getTypeOFAction(bb.get(i)) != typeOfAction.UNKNOWN_ACTION) {
                            toa = Information.getTypeOFAction(bb.get(i));
                        } else {
                            if (i < bb.size() - 2)
                                if ((bb.get(i) + bb.get(i + 1)).contains("ex"))
                                    toa = typeOfAction.ACTION_CONST_E;
                                else if ((bb.get(i) + bb.get(i + 1)).contains("pi"))
                                    toa = typeOfAction.ACTION_CONST_PI;
                                else if ((bb.get(i) + bb.get(i + 1)).contains("si"))
                                    toa = typeOfAction.ACTION_SINX;
                                else if ((bb.get(i) + bb.get(i + 1)).contains("co"))
                                    toa = typeOfAction.ACTION_COSX;
                                else if ((bb.get(i) + bb.get(i + 1)).contains("sq"))
                                    toa = typeOfAction.ACTION_SQRT;
                        }
                        outlist.add(Information.getStringByTypeOfAction(toa));// = (AddingElement.elements_addOperationToList(toa,new ListWithCusorPos(outlist,new Parse.textCursorStruct(outlist.size()-1,0)))).getList();
                    } else {
                        if (!bb.get(i).equals("="))
                            res = res.concat(bb.get(i));
                    }

                    i++;
                }
                list.add(new expression(AddingElement.tryToFuckExpressiontoGood(outlist), res.replace(',', '.')));
                if (list.size() == 6)
                    Log.i("d", "d");
                outlist.clear();
                res = "";
            }
        } catch (FileNotFoundException ex) {
            Log.d("TEST", "FNF!");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void runTests() {
        for (expression ex : list) {
            BigDecimalBlk bd = null;// Calculate.calculate(new ArrayList<>(ex.list));
            bd.setValue(bd.getDoubleValue());
            BigDecimal val1 = bd.getValue().setScale(7, BigDecimal.ROUND_HALF_UP);
            BigDecimalBlk exVal = new BigDecimalBlk(Double.valueOf(ex.getResult()));
            BigDecimal val2 = exVal.getValue().setScale(7, BigDecimal.ROUND_HALF_UP);

            Log.d("TESTER", "TEST WITH " + ex.getList().toString() + " result = " + val2
                    .toString() + " resultCalculate = " + val1.toString() + " STATE = " +
                    (val2.equals(val1) ? "PASSED" : "FAILED"));

            // log("",ex.list.toString(),ex.result, Calculate.calculate((ArrayList<String>)ex.list.clone()).toString().contains(ex.result));
        }
    }

    private void testBD() {
        ArrayList<String> list = new ArrayList<>();
        int capacity = 15;
        for (int i = 0; i < capacity; i++) {
            list.add(String.valueOf(i));
        }

        list.add("q1");
        list.add("q2");
        list.add("q3");
        while (list.size() > capacity) {
            list.remove(0);
        }
    }

    private final String path;

    public Test(String dir) {
        path = dir;
    }

    public void startTests() {
        testBD();
        readTestFile(path + "/test1");
        runTests();
        Log.d("TESTINIT", "RR");
    }
    //cnv = new Converter();

    // ArrayList <String> list = new ArrayList <String>();

    //list.addAll(Arrays.asList("(", "(","(","2", "+","7", ")", "+", "2",")", "+","7", ")","*", "2","+", "(","2", "+","3", ")","^", "(","7", "-","6", ")"));
    // for (int i = 0 ; i < list.size()-1 ; i++)
    // {
    //p.findExpressionShortLengthAroundIndex(i,list);
    // }
//        log("hex-dec","ABCB1234.2032498",cnv.convertHexDec("ABCB1234.2032498").contains("2882212404.125"));
//        log("hex-dec","0.2032498",cnv.convertHexDec("0.2032498").contains("0.1257673203945"));
//        log("hex-dec","0.0",cnv.convertHexDec("0.0").equals("0"));
//        log("hex-dec","0",cnv.convertHexDec("0").equals("0"));
//        log("hex-dec","AaaC",cnv.convertHexDec("AaaC").equals("43692"));
//        log("hex-dec","12345245756",cnv.convertHexDec("12345245756").equals("1250995492694"));
//
//        log("hex-bin","ABCB1234.2032498",cnv.convertHexBin("ABCB1234.2032498").contains("10101011110010110001001000110100.00100000001"));
//        log("hex-bin","0.2032498",cnv.convertHexBin("0.2032498").contains("0.00100000001"));
//        log("hex-bin","0.0",cnv.convertHexBin("0.0").equals("0"));
//        log("hex-bin","0",cnv.convertHexBin("0").equals("0"));
//        log("hex-bin","AaaC",cnv.convertHexBin("AaaC").equals("1010101010101100"));
//        log("hex-bin","12345245756",cnv.convertHexBin("12345245756").equals("10010001101000101001001000101011101010110"));
//
//        log("hex-oct","ABCB1234.2032498",cnv.convertHexOct("ABCB1234.2032498").contains("25362611064.1003111"));
//        log("hex-oct","0.2032498",cnv.convertHexOct("0.2032498").contains("0.100311114"));
//        log("hex-oct","0.0",cnv.convertHexOct("0.0").equals("0"));
//        log("hex-oct","0",cnv.convertHexOct("0").equals("0"));
//        log("hex-oct","AaaC",cnv.convertHexOct("AaaC").equals("125254"));
//        log("hex-oct","12345245756",cnv.convertHexOct("12345245756").equals("22150511053526"));


    //        cnv.convertHexBin("ABCB1234.2032498");
//        cnv.convertHexBin("0.2032498");
//        cnv.convertHexBin("0.0");
//        cnv.convertHexBin("0");
//        cnv.convertHexBin("AaaC");
//        cnv.convertHexBin("12345245756");
//
//        cnv.convertHexOct("ABCB1234.2032498");
//        cnv.convertHexOct("0.2032498");
//        cnv.convertHexOct("0.0");
//        cnv.convertHexOct("0");
//        cnv.convertHexOct("AaaC");
//        cnv.convertHexOct("12345245756");
//
//
//        cnv.convertDecOct("148657.9");
//        cnv.convertBinDec("01010101110.010111011010");
//        cnv.convertDecOct("148657.9");
//        cnv.convertOctDec("7643142457614");
//        cnv.convertHexDec("ABA1290E");
    //   }
    private void log(String name, String inputval, String result, boolean passed) {
        String t;
        if (passed) t = "TEST PASSED";
        else t = "TEST FAILED";
        Log.d(TAG, "TEST " + name + " with input value = " + inputval + "with result value = " + result + " ended with state: " + t);
    }
}
