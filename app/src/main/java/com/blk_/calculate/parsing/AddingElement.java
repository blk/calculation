package com.blk_.calculate.parsing;

import com.blk_.calculate.Information;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.logger.SingletonLogger;
import com.blk_.calculate.math.Calculate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class AddingElement {
    public static ListWithCusorPos addNumericExpression(ArrayList<String> list, String element, Parse.textCursorStruct tcs) {
        if (Information.getTypeOFAction(element) != typeOfAction.ACTION_NUMERIC)
            return null;
        else {
            if (list.isEmpty()) {
                list.add(element);
                tcs.setIndexInArrayList(0);
                return new ListWithCusorPos(list, tcs);
            }
            ListWithCusorPos llwcp = new ListWithCusorPos(list, tcs);
            ArrayList<String> lst = new ArrayList<>(Arrays.asList(element.split("")));
            removeEmptyElements(lst);

            for (String el : lst) {
                if (Information.getTypeOFAction(el) == typeOfAction.ACTION_FLOAT) {
                    llwcp = addFloat(llwcp.getList(), typeOfAction.ACTION_FLOAT, llwcp.getTcs());
                } else {
                    llwcp = addNumericElement(llwcp.getList(), Information.getNumeric(el), llwcp.getTcs());
                }
            }
            return llwcp;
        }
    }

    private static ListWithCusorPos addNumericElement(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        ArrayList<String> outList = new ArrayList<>(list);
        ListWithCusorPos wlcp;
        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));

            if (localtoa == typeOfAction.ACTION_NUMERIC) {
                String b = outList.get(tcs.getIndexInArrayList());
                String b1 = outList.get(tcs.getIndexInArrayList());
                b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                b = b.concat(Information.getStringByTypeOfAction(toa)) + b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                outList.set(tcs.getIndexInArrayList(), b);
                removeEmptyElements(outList);
                return new ListWithCusorPos(outList, tcs);
            } else if (/*isBigCommand(localtoa)||*/isDegree(localtoa) && (tcs.getIndexInArrayList() != list.size() - 1)) {
                // это степень и не последний индекс массива
                if (tcs.getIndexInArrayList() == 0) {
                    outList.add(0, Information.getStringByTypeOfAction(toa));
                    tcs.setIndexInArrayList(0);
                    tcs.setCursorPositionFromEnd(0);
                    return new ListWithCusorPos(outList, tcs);
                } else {
                    outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
                    tcs.setCursorPositionFromEnd(0);
                    tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                    return new ListWithCusorPos(outList, tcs);
                }

            } else if (isBigCommand(localtoa)) {
                // это степень и не последний индекс массива
                if (tcs.getIndexInArrayList() == 0) {
                    if (tcs.getCursorPositionFromEnd() != 0) {
                        outList.add(0, Information.getStringByTypeOfAction(toa));
                        tcs.setIndexInArrayList(0);
                        tcs.setCursorPositionFromEnd(0);
                    } else {
                        outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
                        tcs.setCursorPositionFromEnd(0);
                        tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                    }
                    return new ListWithCusorPos(outList, tcs);
                } else {
                    outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
                    tcs.setCursorPositionFromEnd(0);
                    tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                    return new ListWithCusorPos(outList, tcs);
                }
            } else if (isCommand(localtoa) || isOpenParentesis(localtoa)) {
                if (tcs.getIndexInArrayList() + 1 <= outList.size() - 1 && (tcs.getCursorPositionFromEnd() <= 1)) {
                    // добавляем цифры справа
                    if ((Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList() + 1)) == typeOfAction.ACTION_NUMERIC)) {
                        tcs.setCursorPositionFromEnd(outList.get(tcs.getIndexInArrayList() + 1).length());
                        tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                        return addNumericElement(outList, toa, tcs);
                    } else {
                        outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
                        tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                        tcs.setCursorPositionFromEnd(0);
                        return new ListWithCusorPos(outList, tcs);
                    }
                } else if (tcs.getIndexInArrayList() - 1 >= 0 && (tcs.getCursorPositionFromEnd() > 1)) {
                    if ((Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList() - 1)) == typeOfAction.ACTION_NUMERIC) && (tcs.getCursorPositionFromEnd() > 1)) {
                        tcs.setCursorPositionFromEnd(0);
                        tcs.setIndexInArrayList(tcs.getIndexInArrayList() - 1);
                        return addNumericElement(outList, toa, tcs);
                    } else {
                        outList.add(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(toa));
                        tcs.setIndexInArrayList(tcs.getIndexInArrayList());
                        tcs.setCursorPositionFromEnd(0);
                        return new ListWithCusorPos(outList, tcs);
                    }
                } else if (tcs.getIndexInArrayList() == 0 && (tcs.getCursorPositionFromEnd() > 1)) {
                    outList.add(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(toa));
                    tcs.setIndexInArrayList(tcs.getIndexInArrayList());
                    tcs.setCursorPositionFromEnd(0);
                    return new ListWithCusorPos(outList, tcs);
                }
            } else if (isClosedParenthesis(localtoa)) {
                if (tcs.getIndexInArrayList() > 0) {
                    outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
                    tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                    tcs.setCursorPositionFromEnd(0);
                } else {
                    outList.add(0, Information.getStringByTypeOfAction(toa));
                    tcs.setIndexInArrayList(0);
                    tcs.setCursorPositionFromEnd(0);
                }
                return new ListWithCusorPos(outList, tcs);
            }
            if (isOpenParentesis(localtoa) && (tcs.getIndexInArrayList() == 0)) {
                outList.add(Information.getStringByTypeOfAction(toa));
                tcs.setIndexInArrayList(tcs.getIndexInArrayList());
                tcs.setCursorPositionFromEnd(0);
                return new ListWithCusorPos(outList, tcs);
            }
        }
        outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
        tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
        tcs.setCursorPositionFromEnd(0);
        return new ListWithCusorPos(outList, tcs);

    }


    private static ListWithCusorPos addComand(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        ArrayList<String> outList = new ArrayList<>(list);
        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));
            if (localtoa == typeOfAction.ACTION_NUMERIC) {
                String b = outList.get(tcs.getIndexInArrayList());
                String b1 = outList.get(tcs.getIndexInArrayList());
                b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                b1 = b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                //b = b.concat(Information.getStringByTypeOfAction(toa)) + b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                outList.set(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(toa));
                outList.add(tcs.getIndexInArrayList(), b);
                outList.add(tcs.getIndexInArrayList() + 2, b1);
                removeEmptyElements(outList);
                if ((tcs.getIndexInArrayList() == 0) && (tcs.getCursorPositionFromEnd() > 0) && (b.isEmpty()))
                    tcs.setIndexInArrayList(0);
                else
                    tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                tcs.setCursorPositionFromEnd(0);
                return new ListWithCusorPos(outList, tcs);
//                }else if (isBigCommand(localtoa)||isDegree(localtoa)){
//                    tcs.setCursorPositionFromEnd(0);
//                    tcs.setIndexInArrayList(tcs.getIndexInArrayList()+1);
//                    return addComand(outList,toa,tcs);
            }
            if (!isBigCommand(localtoa) && !isOpenParentesis(localtoa) && !isClosedParenthesis(localtoa)) {
                if (tcs.getCursorPositionFromEnd() <= 1) //справа или слева от элемента(будь то скобка(наверно) будь то команда)
                    outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
                else
                    outList.add(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(toa));
                tcs.setIndexInArrayList(tcs.getCursorPositionFromEnd() <= 1 ? tcs.getIndexInArrayList() + 1 : tcs.getIndexInArrayList());
            } else {
                if (tcs.getCursorPositionFromEnd() == 0) {
                    outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
                } else {
                    outList.add(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(toa));
                }
                tcs.setIndexInArrayList(tcs.getCursorPositionFromEnd() == 0 ? tcs.getIndexInArrayList() + 1 : tcs.getIndexInArrayList());
            }
            tcs.setCursorPositionFromEnd(0);
            return new ListWithCusorPos(outList, tcs);
        } else {
            tcs.setCursorPositionFromEnd(0);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
            outList.add(Information.getStringByTypeOfAction(toa));
        }

        return new ListWithCusorPos(outList, tcs);
    }


    private static ListWithCusorPos addFloat(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        ArrayList<String> outList = new ArrayList<>(list);

        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));
            if (localtoa == typeOfAction.ACTION_NUMERIC) {
                if (!outList.get(tcs.getIndexInArrayList()).contains(Information.getStringByTypeOfAction(typeOfAction.ACTION_FLOAT))) {
                    String b = outList.get(tcs.getIndexInArrayList());
                    String b1 = outList.get(tcs.getIndexInArrayList());
                    b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                    b = b.concat(Information.getStringByTypeOfAction(toa)) + b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                    outList.set(tcs.getIndexInArrayList(), b);
                    return new ListWithCusorPos(outList, tcs);
                } else
                    return new ListWithCusorPos(outList, tcs);
            }
//                else if (isBigCommand(localtoa)||isDegree(localtoa)){
//                    tcs.setCursorPositionFromEnd(outList.get(tcs.getIndexInArrayList()+1).length());
//                    tcs.setIndexInArrayList(tcs.getIndexInArrayList()+1);
//                    return addFloat(outList,toa,tcs);
//                }
            if (isCommand(localtoa) && (tcs.getIndexInArrayList() < 2)) {
                if (tcs.getIndexInArrayList() + 1 < list.size()) {
                    if (Information.getTypeOFAction(list.get(tcs.getIndexInArrayList() + 1)) == typeOfAction.ACTION_NUMERIC) {
                        tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                        tcs.setCursorPositionFromEnd(list.get(tcs.getIndexInArrayList()).length());
                        return addFloat(outList, toa, tcs);
                    }
                }

            }
            outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0) + Information.getStringByTypeOfAction(typeOfAction.ACTION_FLOAT));
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
            tcs.setCursorPositionFromEnd(0);
            return new ListWithCusorPos(outList, tcs);

        } else {
            outList.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0) + Information.getStringByTypeOfAction(typeOfAction.ACTION_FLOAT));
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
            tcs.setCursorPositionFromEnd(0);
        }

        return new ListWithCusorPos(outList, tcs);
    }

    private static ListWithCusorPos addBigCommand(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        ArrayList<String> outList = new ArrayList<>(list);

        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));
            if (localtoa == typeOfAction.ACTION_NUMERIC) {
                String b = outList.get(tcs.getIndexInArrayList());
                String b1 = outList.get(tcs.getIndexInArrayList());
                b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                b1 = b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                //b = b.concat(Information.getStringByTypeOfAction(toa)) + b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                //b = b.concat(Information.getStringByTypeOfAction(toa)) + b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                outList.set(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(toa));
                outList.add(tcs.getIndexInArrayList(), b);
                // outList.add(tcs.getIndexInArrayList()+2,Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
                //outList.add(tcs.getIndexInArrayList()+3,Information.getStringByTypeOfAction(typeOfAction.ACTION_CLOSING_PARENTHESIS));
                outList.add(tcs.getIndexInArrayList() + 2, b1);
                removeEmptyElements(outList);
                tcs.setCursorPositionFromEnd(1);
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                // STATE CURSOR TODO
                return new ListWithCusorPos(outList, tcs);
            } else if (isBigCommand(localtoa) || isDegree(localtoa)) {
                tcs.setCursorPositionFromEnd(outList.get(tcs.getIndexInArrayList() + 1).length());
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                return addBigCommand(outList, toa, tcs);
            }
            outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
            //outList.add(tcs.getIndexInArrayList()+2,Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
            //outList.add(tcs.getIndexInArrayList()+3,Information.getStringByTypeOfAction(typeOfAction.ACTION_CLOSING_PARENTHESIS));
            tcs.setCursorPositionFromEnd(1);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
            return new ListWithCusorPos(outList, tcs);
        } else {
            outList.add(Information.getStringByTypeOfAction(toa));
            //outList.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
            //outList.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_CLOSING_PARENTHESIS));
            tcs.setCursorPositionFromEnd(1);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);

        }
        return new ListWithCusorPos(outList, tcs);
    }


    public static ArrayList<String> addSmartOneClosingParenthesis(ArrayList<String> lst) {
        int i = 0;
        int countOp = 0;
        int countCl = 0;
        if (!lst.isEmpty()) {
            while (i < lst.size()) {
                if (Information.getTypeOFAction(lst.get(i)) == typeOfAction.ACTION_CLOSING_PARENTHESIS) {
                    countCl++;
                }
                if (Information.getTypeOFAction(lst.get(i)) == typeOfAction.ACTION_OPENING_PARENTHESIS) {
                    countOp++;
                }
                if (countCl > countOp) {
                    lst.add(0, Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
                    return lst;
                }
                i++;
            }
            if (countOp > countCl) {
                lst.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_CLOSING_PARENTHESIS));
                return lst;
            }
        }
        return lst;
    }

    private static ListWithCusorPos addPastCommand(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        ArrayList<String> outList = new ArrayList<>(list);

        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));
            if (localtoa == typeOfAction.ACTION_NUMERIC) {
                String b = outList.get(tcs.getIndexInArrayList());
                String b1 = outList.get(tcs.getIndexInArrayList());
                b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                b1 = b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                //b = b.concat(Information.getStringByTypeOfAction(toa)) + b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                //b = b.concat(Information.getStringByTypeOfAction(toa)) + b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                outList.set(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(toa));
                outList.add(tcs.getIndexInArrayList(), b);
                outList.add(tcs.getIndexInArrayList() + 2, b1);
                removeEmptyElements(outList);
                // STATE CURSOR TODO
                tcs.setCursorPositionFromEnd(0);
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                return new ListWithCusorPos(outList, tcs);
            }
//                else if (isBigCommand(localtoa)||isDegree(localtoa)){
//                    tcs.setCursorPositionFromEnd(outList.get(tcs.getIndexInArrayList()+1).length());
//                    tcs.setIndexInArrayList(tcs.getIndexInArrayList()+1);
//                    return addPastCommand(outList,toa,tcs);
//                }
            outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(toa));
            tcs.setCursorPositionFromEnd(0);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
            return new ListWithCusorPos(outList, tcs);

        } else {
            outList.add(Information.getStringByTypeOfAction(toa));
            tcs.setCursorPositionFromEnd(0);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
        }
        return new ListWithCusorPos(outList, tcs);
    }

    private static ListWithCusorPos clearLast(ArrayList<String> list, Parse.textCursorStruct tcs) {
        ArrayList<String> outList = new ArrayList<>(list);

        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));
            if (localtoa == typeOfAction.ACTION_NUMERIC) {
                if ((tcs.getIndexInArrayList() == 0) && (list.get(tcs.getIndexInArrayList()).length() == tcs.getCursorPositionFromEnd())) {
                    return new ListWithCusorPos(list, tcs);//если самое начало, эт сложно
                }
                if (outList.get(tcs.getIndexInArrayList()).length() == 1) {
                    outList.remove(tcs.getIndexInArrayList());
                    tcs.setCursorPositionFromEnd(0);
                    tcs.setIndexInArrayList(tcs.getIndexInArrayList() - 1);
                    tcs = concatNumericsIfNeed(tcs.getIndexInArrayList(), outList, tcs);
                    return new ListWithCusorPos(outList, tcs);
                } else {
                    String b = outList.get(tcs.getIndexInArrayList());
                    String b1 = outList.get(tcs.getIndexInArrayList());
                    b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                    b1 = b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                    outList.set(tcs.getIndexInArrayList(), b.substring(0, b.length() - 1) + b1);
                    removeEmptyElements(outList);
                    tcs.setCursorPositionFromEnd(outList.get(tcs.getIndexInArrayList()).length() - (b.length() - 1));
                    tcs.setIndexInArrayList(tcs.getIndexInArrayList());
                    tcs = concatNumericsIfNeed(tcs.getIndexInArrayList(), outList, tcs);
                    return new ListWithCusorPos(outList, tcs);
                }
            } else if (isCommand(localtoa)) {
                if (tcs.getIndexInArrayList() == 0) {
                    if (tcs.getCursorPositionFromEnd() > 1)
                        return new ListWithCusorPos(outList, tcs);
                } else {
                    if (tcs.getCursorPositionFromEnd() > 1) {
                        tcs.setIndexInArrayList(tcs.getIndexInArrayList() - 1);
                        tcs.setCursorPositionFromEnd(0);
                        clearLast(list, tcs);
                    }
                }
            } else if (isBigCommand(localtoa) || isDegree(localtoa)) {
                outList.remove(tcs.getIndexInArrayList());
                tcs.setCursorPositionFromEnd(0);
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() - 1);
                tcs = concatNumericsIfNeed(tcs.getIndexInArrayList(), outList, tcs);
                return new ListWithCusorPos(outList, tcs);
            }
//                else if (isDegree(localtoa)){
//                    outList.remove(tcs.getIndexInArrayList());
//                    if (outList.size()>1) {
//                        outList.remove(tcs.getIndexInArrayList() - 1);
//                        tcs.setIndexInArrayList(tcs.getIndexInArrayList()-1);
//                    }
//                    tcs.setCursorPositionFromEnd(0);
//                    tcs.setIndexInArrayList(tcs.getIndexInArrayList()-1);
//                    return new ListWithCusorPos(outList,tcs);
//                }
//                else if(isOpenParentesis(localtoa)){
//                    if (tcs.getIndexInArrayList()>0){
//                        if (isBigCommand(Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()-1)))){
//                            outList.remove(tcs.getIndexInArrayList()-1);
//                            outList.remove(tcs.getIndexInArrayList()-1);
//                            tcs.setCursorPositionFromEnd(0);
//                            tcs.setIndexInArrayList(tcs.getIndexInArrayList()-2);
//                            return new ListWithCusorPos(outList,tcs);
//                        }
//                    }
//                }
            outList.remove(tcs.getIndexInArrayList());
            tcs.setCursorPositionFromEnd(0);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() - 1);
            tcs = concatNumericsIfNeed(tcs.getIndexInArrayList(), outList, tcs);
            return new ListWithCusorPos(outList, tcs);
        }
        return new ListWithCusorPos(outList, tcs);
    }

    private static ListWithCusorPos addOpeningParenthesis(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        return addComand(list, toa, tcs);
    }

    private static ListWithCusorPos addClosingParenthesis(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        return addComand(list, toa, tcs);
    }

    public static ListWithCusorPos elements_addOperationToList(typeOfAction toa, ListWithCusorPos lwcp) {
        ArrayList<String> outList = new ArrayList<>(lwcp.getList());
        ListWithCusorPos locLWCP = new ListWithCusorPos(outList, lwcp.getTcs());
        try {
            Date current = Calendar.getInstance().getTime();
            long begin = current.getTime();
            switch (Information.getTypeOFAction(Information.getStringByTypeOfAction(toa))) {

                case ACTION_SUBOPERATION_X2:
                    locLWCP = addSimpleCommand(typeOfAction.ACTION_DEGREE, locLWCP);
                    locLWCP = addSimpleCommand(typeOfAction.ACTION_NUMERIC2, locLWCP);
                    break;
                case ACTION_1DX:
                    locLWCP = addSimpleCommand(typeOfAction.ACTION_DEGREE, locLWCP);
                    locLWCP = addSimpleCommand(typeOfAction.ACTION_OPENING_PARENTHESIS, locLWCP);
                    locLWCP = addSimpleCommand(typeOfAction.ACTION_MINUS, locLWCP);
                    locLWCP = addSimpleCommand(typeOfAction.ACTION_NUMERIC1, locLWCP);
                    locLWCP = addSimpleCommand(typeOfAction.ACTION_CLOSING_PARENTHESIS, locLWCP);
                    break;
                case ACTION_PLUS:
                case ACTION_MINUS:
                case ACTION_NUMERIC:
                case ACTION_DELEN:
                case ACTION_PERCENT:
                case ACTION_FLOAT:
                case ACTION_FACTORIAL:
                case ACTION_UMNOZ:
                case ACTION_CONST_PI:
                case ACTION_NUMERIC_PLUS_MINUS:
                case ACTION_CONST_E:
                case ACTION_DEGREE:
                case ACTION_SINX:
                case ACTION_OPENING_PARENTHESIS:
                case ACTION_CLOSING_PARENTHESIS:
                case ACTION_COSX:
                case ACTION_SQRT:
                case ACTION_LG:
                case ACTION_LN: {
                    locLWCP = addSimpleCommand(toa, locLWCP);
                    break;
                }
                case ACTION_CLEAR_LAST: {
                    return clearLast(locLWCP.getList(), locLWCP.getTcs());
                }
                case ACTION_CLEAR: {
                    locLWCP.getList().clear();
                    locLWCP.getTcs().setCursorPositionFromEnd(0);
                    locLWCP.getTcs().setIndexInArrayList(0);
                    return locLWCP;
                }
            }
            Date current2 = Calendar.getInstance().getTime();
            long end = current2.getTime();
            //Log.d("TIMERADDING","TIME OF WORK = " + String.valueOf(end-begin));
        } catch (Throwable ex) {
            SingletonLogger.getInstance().onException(ex);
        }
        return locLWCP;
    }

    private static ListWithCusorPos addSimpleCommand(typeOfAction toa, ListWithCusorPos lwcp) {

        if (lwcp.getList() != null) {
            if (Information.getTypeOFAction(Information.getStringByTypeOfAction(toa)) == typeOfAction.ACTION_NUMERIC) {
                return addNumericElement(lwcp.getList(), toa, lwcp.getTcs());
            }
            if (isConst(toa)) {
                return addComand(lwcp.getList(), toa, lwcp.getTcs());
            }

            if (isCommand(toa)) {
                return addComand(lwcp.getList(), toa, lwcp.getTcs());
            }
            if (isBigCommand(toa) || isDegree(toa)) {
                return addComand(lwcp.getList(), toa, lwcp.getTcs());
            }

            if (isPastCommand(toa)) {
                return addPastCommand(lwcp.getList(), toa, lwcp.getTcs());
            }
            if (isOpenParentesis(toa)) {
                return addOpeningParenthesis(lwcp.getList(), toa, lwcp.getTcs());
            }
            if (isClosedParenthesis(toa)) {
                return addClosingParenthesis(lwcp.getList(), toa, lwcp.getTcs());
            }
            if (isFloat(toa)) {
                return addFloat(lwcp.getList(), toa, lwcp.getTcs());
            }
            if (isPlusMinus(toa)) {
                return addPlusMinus(lwcp.getList(), toa, lwcp.getTcs());
            }
//                if (isSubX2(toa)){
//                    return addX2(listWithPos.getList(),toa,listWithPos.getTcs());
//                }
            if (isSub1DX(toa)) {
                return add1DX(lwcp.getList(), toa, lwcp.getTcs());
            }
        }
        return lwcp;
    }

    private static ListWithCusorPos addX2(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        ArrayList<String> outList = new ArrayList<>(list);

        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));
            if ((localtoa == typeOfAction.ACTION_NUMERIC) || (localtoa == typeOfAction.ACTION_CLOSING_PARENTHESIS)) {
                if (Parse.exp_isExpressionUnderDegreeBeforeIndex(tcs.getIndexInArrayList(), outList)) {
                    ExpressionStatus esA = new ExpressionStatus();
                    ExpressionStatus esB = new ExpressionStatus();
                    if (tcs.getIndexInArrayList() > 0)
                        if (Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList() - 1)) == typeOfAction.ACTION_DEGREE) {
                            esA.setBeginIndex(tcs.getIndexInArrayList());
                            esA.setEndIndex(esA.getBeginIndex());
                        } else {
                            esA = Parse.exp_getExpressionStatusShortLengthAroundIndex(tcs.getIndexInArrayList(), outList);
                            esB = new ExpressionStatus();
                        }
                    if (esA.getBeginIndex() > 1) {
                        if (Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList() - 2)) == typeOfAction.ACTION_NUMERIC) {
                            esB.setBeginIndex(tcs.getIndexInArrayList() - 2);
                            esB.setEndIndex(tcs.getIndexInArrayList() - 2);
                        } else
                            esB = Parse.exp_getExpressionStatusAroundIndex(esA.getBeginIndex() - 2, outList);
                    } else {
                        esB.setBeginIndex(0);
                    }
                    outList.add(esB.getBeginIndex(), Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
                    outList.add(esA.getEndIndex() + 2, Information.getStringByTypeOfAction(typeOfAction.ACTION_CLOSING_PARENTHESIS));
                    outList.add(esA.getEndIndex() + 3, Information.getStringByTypeOfAction(typeOfAction.ACTION_DEGREE));
                    outList.add(esA.getEndIndex() + 4, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC2));
                    removeEmptyElements(outList);
                    tcs.setCursorPositionFromEnd(0);
                    tcs.setIndexInArrayList(esA.getEndIndex() + 4);
                    return new ListWithCusorPos(outList, tcs);
                }
                String b = outList.get(tcs.getIndexInArrayList());
                String b1 = outList.get(tcs.getIndexInArrayList());
                b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                b1 = b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                outList.set(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(typeOfAction.ACTION_DEGREE));
                outList.add(tcs.getIndexInArrayList(), b);
                outList.add(tcs.getIndexInArrayList() + 2, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC2));
                outList.add(tcs.getIndexInArrayList() + 3, b1);
                removeEmptyElements(outList);
                tcs.setCursorPositionFromEnd(0);
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 2);
                return new ListWithCusorPos(outList, tcs);
                // STATE CURSOR TODO
            } else if (isBigCommand(localtoa)) {
                tcs.setCursorPositionFromEnd(outList.get(tcs.getIndexInArrayList() + 1).length());
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                return addX2(outList, toa, tcs);
            }
            outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_DEGREE));
            outList.add(tcs.getIndexInArrayList() + 2, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC2));
            tcs.setCursorPositionFromEnd(0);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 2);
            return new ListWithCusorPos(outList, tcs);
        } else {
            outList.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_DEGREE));
            outList.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC2));
            tcs.setCursorPositionFromEnd(0);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 2);

        }
        return new ListWithCusorPos(outList, tcs);
    }

    private static ListWithCusorPos add1DX(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        ArrayList<String> outList = new ArrayList<>(list);

        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));
            if ((localtoa == typeOfAction.ACTION_NUMERIC) || (localtoa == typeOfAction.ACTION_CLOSING_PARENTHESIS)) {
                if (Parse.exp_isExpressionUnderDegreeBeforeIndex(tcs.getIndexInArrayList(), outList)) {
                    ExpressionStatus esA = new ExpressionStatus();
                    ExpressionStatus esB = new ExpressionStatus();
                    if (tcs.getIndexInArrayList() > 0)
                        if (Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList() - 1)) == typeOfAction.ACTION_DEGREE) {
                            esA.setBeginIndex(tcs.getIndexInArrayList());
                            esA.setEndIndex(esA.getBeginIndex());
                        } else {
                            esA = Parse.exp_getExpressionStatusShortLengthAroundIndex(tcs.getIndexInArrayList(), outList);
                            esB = new ExpressionStatus();
                        }
                    if (esA.getBeginIndex() > 1) {
                        if (Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList() - 2)) == typeOfAction.ACTION_NUMERIC) {
                            esB.setBeginIndex(tcs.getIndexInArrayList() - 2);
                            esB.setEndIndex(tcs.getIndexInArrayList() - 2);
                        } else
                            esB = Parse.exp_getExpressionStatusAroundIndex(esA.getBeginIndex() - 2, outList);
                    } else {
                        esB.setBeginIndex(0);
                    }
                    outList.add(esB.getBeginIndex(), Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
                    outList.add(esA.getEndIndex() + 2, Information.getStringByTypeOfAction(typeOfAction.ACTION_CLOSING_PARENTHESIS));
                    outList.add(esA.getEndIndex() + 3, Information.getStringByTypeOfAction(typeOfAction.ACTION_DEGREE));
                    outList.add(esA.getEndIndex() + 4, Information.getStringByTypeOfAction(typeOfAction.ACTION_MINUS));
                    outList.add(esA.getEndIndex() + 5, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC2));
                    removeEmptyElements(outList);
                    tcs.setCursorPositionFromEnd(0);
                    tcs.setIndexInArrayList(esA.getEndIndex() + 4);
                    return new ListWithCusorPos(outList, tcs);
                }
                String b = outList.get(tcs.getIndexInArrayList());
                String b1 = outList.get(tcs.getIndexInArrayList());
                b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                b1 = b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                outList.set(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(typeOfAction.ACTION_DEGREE));
                outList.add(tcs.getIndexInArrayList(), b);
                outList.add(tcs.getIndexInArrayList() + 2, Information.getStringByTypeOfAction(typeOfAction.ACTION_MINUS));
                outList.add(tcs.getIndexInArrayList() + 3, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC2));
                outList.add(tcs.getIndexInArrayList() + 3, b1);
                removeEmptyElements(outList);
                tcs.setCursorPositionFromEnd(0);
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 2);
                return new ListWithCusorPos(outList, tcs);
                // STATE CURSOR TODO
            } else if (isBigCommand(localtoa)) {
                tcs.setCursorPositionFromEnd(outList.get(tcs.getIndexInArrayList() + 1).length());
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
                return addX2(outList, toa, tcs);
            }
            outList.add(tcs.getIndexInArrayList() + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_DEGREE));
            outList.add(tcs.getIndexInArrayList() + 2, Information.getStringByTypeOfAction(typeOfAction.ACTION_MINUS));
            outList.add(tcs.getIndexInArrayList() + 3, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC1));
            tcs.setCursorPositionFromEnd(0);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 2);
            return new ListWithCusorPos(outList, tcs);
        } else {
            outList.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_DEGREE));
            outList.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_MINUS));
            outList.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC1));
            tcs.setCursorPositionFromEnd(0);
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 2);

        }
        return new ListWithCusorPos(outList, tcs);
    }


    private static ListWithCusorPos addPlusMinus(ArrayList<String> list, typeOfAction toa, Parse.textCursorStruct tcs) {
        // Наверно временно сделаю плюс минус через 0- а не через смену знака.эх
        ArrayList<String> outList = new ArrayList<>(list);

        if (!outList.isEmpty()) {

            typeOfAction localtoa;
            if (tcs.getIndexInArrayList() > 0)
                localtoa = Information.getTypeOFAction(outList.get(tcs.getIndexInArrayList()));
            else
                localtoa = Information.getTypeOFAction(outList.get(0));
            if ((localtoa == typeOfAction.ACTION_NUMERIC) || (isConst(localtoa))) // || ) const
            {
//                    String b = outList.get(tcs.getIndexInArrayList());
//                    if (!b.contains(Information.getStringByTypeOfAction(typeOfAction.ACTION_MINUS)))
//                    {
//                    b = "-" + b;
//                    }else {
//                        b = b.substring(1,b.length());
//                    }
//                    outList.set(tcs.getIndexInArrayList(), b);
//                    removeEmptyElements(outList);
//                    return outList;
                String b = outList.get(tcs.getIndexInArrayList());
                String b1 = outList.get(tcs.getIndexInArrayList());
                b = b.substring(0, b.length() - tcs.getCursorPositionFromEnd());
                b1 = b1.substring(b1.length() - tcs.getCursorPositionFromEnd());
                if (b1.isEmpty()) {
                    b1 = b;
                    b = "";
                }
                outList.set(tcs.getIndexInArrayList(), b1);
                outList.add(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(typeOfAction.ACTION_MINUS));
                outList.add(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0));
                outList.add(tcs.getIndexInArrayList(), Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
                outList.add(tcs.getIndexInArrayList(), b);
                outList.add(tcs.getIndexInArrayList() + 5, Information.getStringByTypeOfAction(typeOfAction.ACTION_CLOSING_PARENTHESIS));
                removeEmptyElements(outList);
                tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 4);
                tcs.setCursorPositionFromEnd(0);
                return new ListWithCusorPos(outList, tcs);
                // STATE CURSOR TODO

            }

        } else {
            outList.add(Information.getStringByTypeOfAction(toa));
            tcs.setIndexInArrayList(tcs.getIndexInArrayList() + 1);
            tcs.setCursorPositionFromEnd(0);
        }
        return new ListWithCusorPos(outList, tcs);
    }


    private static boolean isDegree(typeOfAction toa) {
        return toa == typeOfAction.ACTION_DEGREE;
    }

    private static boolean isBigCommand(typeOfAction toa) {
        return ((toa == typeOfAction.ACTION_SINX) ||
                (toa == typeOfAction.ACTION_COSX) || (toa == typeOfAction.ACTION_SQRT)
                || (toa == typeOfAction.ACTION_LG) || (toa == typeOfAction.ACTION_LN));
    }

    private static boolean isCommand(typeOfAction toa) {
        return ((toa == typeOfAction.ACTION_MINUS) || (toa == typeOfAction.ACTION_PLUS) ||
                (toa == typeOfAction.ACTION_UMNOZ) || (toa == typeOfAction.ACTION_DELEN));
    }

    private static boolean isConst(typeOfAction toa) {
        return ((toa == typeOfAction.ACTION_CONST_PI) || (toa == typeOfAction.ACTION_CONST_E));
    }

    private static boolean isPastCommand(typeOfAction toa) {
        return ((toa == typeOfAction.ACTION_FACTORIAL) || (toa == typeOfAction.ACTION_PERCENT));
    }

    private static boolean isOpenParentesis(typeOfAction toa) {
        return (toa == typeOfAction.ACTION_OPENING_PARENTHESIS);
    }

    private static boolean isClosedParenthesis(typeOfAction toa) {
        return (toa == typeOfAction.ACTION_CLOSING_PARENTHESIS);
    }

    private static boolean isFloat(typeOfAction toa) {
        return (toa == typeOfAction.ACTION_FLOAT);
    }

    private static void removeEmptyElements(ArrayList<String> lst) {
        int i = 0;
        while (i < lst.size()) {
            if (lst.get(i).isEmpty())
                lst.remove(i);
            else i++;
        }
    }

    private static boolean isPlusMinus(typeOfAction toa) {
        return (toa == typeOfAction.ACTION_NUMERIC_PLUS_MINUS);
    }

    private static boolean isSubX2(typeOfAction toa) {
        return (toa == typeOfAction.ACTION_SUBOPERATION_X2);
    }

    private static boolean isSub1DX(typeOfAction toa) {
        return (toa == typeOfAction.ACTION_1DX);
    }


    public static ArrayList<String> tryToFuckExpressiontoGood(ArrayList<String> lst) {

        ArrayList<String> outList = new ArrayList<>(lst);
        Date current = Calendar.getInstance().getTime();
        long begin = current.getTime();

        int maxCount = 10;
        int i = 0;
        try {
            while (i < maxCount) {
                removeEmptyElements(outList);
                remakeBadFloats(outList);
                removeBadBigCommands(outList);
                removeBadPastOperations(outList);
                removeEmptyParenthesis(outList);
                removeBadOperations(outList);
                addParethesisForParse(outList);
                remakeOperandOperationBalanceInList(outList);
                try {
                    Calculate.calculate(outList);
                } catch (Throwable ex) {
                    i++;
                    continue;
                }
                return outList;
            }
        } catch (Throwable ex) {
            SingletonLogger.getInstance().onException(ex);
        }

        return outList;

    }

    private static boolean isOperand(typeOfAction toa) {
        return ((toa == typeOfAction.ACTION_NUMERIC) ||
                (isConst(toa)));
    }

    private static void remakeBadFloats(ArrayList<String> lst) {
        for (int i = 0; i < lst.size(); i++) {
            if (Information.getTypeOFAction(lst.get(i)) == typeOfAction.ACTION_NUMERIC) {
                String el = lst.get(i);
                if (el.charAt(0) == '.') {
                    el = Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0) + el;
                } else if (el.charAt(el.length() - 1) == '.') {
                    el = el + Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0);
                }
                lst.set(i, el);
            }
        }
    }

    public static int getLastIndexOfBigCommand(ArrayList<String> lst) {
        int i = 0;
        int lastIdx = -1;
        while (i < lst.size()) {
            if (isBigCommand(Information.getTypeOFAction(lst.get(i))) || isDegree(Information.getTypeOFAction(lst.get(i))))
                lastIdx = i;
            i++;
        }
        return lastIdx;
    }


    private static void removeBadBigCommands(ArrayList<String> lst) {

        ArrayList<String> blist = lst;
        int li = blist.size();

        while ((li = getLastIndexOfBigCommand(new ArrayList<>(blist.subList(0, li)))) != -1) {  // такой себе вариант конечно же, с точки зрения утечки данных и индекса выходящего за пределы массива
            typeOfAction toaLocal = Information.getTypeOFAction(lst.get(li));
            if (li == lst.size() - 1) {
                lst.remove(li);
                continue;
            }
            if (li + 1 < lst.size()) {
                typeOfAction toaP = Information.getTypeOFAction(lst.get(li + 1));
                if (isCommand(toaP) || isClosedParenthesis(toaP) || isPastCommand(toaP) || isDegree(toaP)) {
                    if ((toaP == typeOfAction.ACTION_MINUS) && (li < lst.size() - 2)) {
                        lst.add(li + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
                        lst.add(li + 2, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0));
                        continue;
                    } else {
                        lst.remove(li);
                        continue;
                    }
                } else if ((isOpenParentesis(toaP)) && (li + 1 == lst.size() - 1)) {
                    lst.remove(li);
                    lst.remove(li);
                    continue;
                } else if (li + 2 < lst.size()) {
                    if (isOpenParentesis(toaP) &&
                            Information.getTypeOFAction(lst.get(li + 2)) == typeOfAction.ACTION_CLOSING_PARENTHESIS) {
                        lst.remove(li);
                        lst.remove(li);
                        lst.remove(li);
                        continue;
                    }
                }
            }
            if (isDegree(toaLocal)) {
                if (li == 0) {
                    lst.remove(li);
                    continue;
                } else {
                    typeOfAction tt = Information.getTypeOFAction(lst.get(li - 1));
                    if (isCommand(tt) || isDegree(tt) || isBigCommand(tt) || isOpenParentesis(tt)) {
                        lst.remove(li);
                        continue;
                    }
                }
            }
            blist = new ArrayList<>(lst.subList(0, li));
        }

    }

    private static void removeBadOperations(ArrayList<String> lst) {
        int i = 0;
        while (i < lst.size()) {
            if (isCommand(Information.getTypeOFAction(lst.get(i)))) {
                if (i + 1 < lst.size()) {
                    if (isCommand(Information.getTypeOFAction(lst.get(i + 1))) || isClosedParenthesis(Information.getTypeOFAction(lst.get(i + 1)))) {
                        lst.remove(i);
                        continue;
                    }

                } else if (i == lst.size() - 1) {
                    lst.remove(i);
                    continue;
                } else if (i - 1 > 0) {
                    if (isOpenParentesis(Information.getTypeOFAction(lst.get(i + 1)))) {
                        lst.remove(i);
                        continue;
                    }
                }

            }
            i++;
        }
    }

    private static void removeBadPastOperations(ArrayList<String> lst) {
        int i = 0;
        while (i < lst.size()) {
            if (isPastCommand(Information.getTypeOFAction(lst.get(i)))) {
                if (i == 0) {
                    lst.remove(i);
                    continue;
                } else {
                    if ((!isOperand(Information.getTypeOFAction(lst.get(i - 1))) && !isClosedParenthesis(Information.getTypeOFAction(lst.get(i - 1)))) || (isOpenParentesis(Information.getTypeOFAction(lst.get(i - 1))))) {
                        lst.remove(i);
                        continue;
                    }
                }
            }
            i++;
        }
    }

    private static void removeEmptyParenthesis(ArrayList<String> lst) {
        int i;


        boolean isBad = true;

        while (isBad) {
            int k;
            for (k = 0; k < lst.size() - 1; k++) {
                typeOfAction toaK = Information.getTypeOFAction(lst.get(k));
                typeOfAction toaK1 = Information.getTypeOFAction(lst.get(k + 1));
                if ((toaK == typeOfAction.ACTION_OPENING_PARENTHESIS) && (toaK1 == typeOfAction.ACTION_CLOSING_PARENTHESIS)) {
                    isBad = true;
                    break;
                } else {
                    isBad = false;
                }
            }
            if (k == 0)
                break;
            i = 0;
            while (i < lst.size()) {
                if (isOpenParentesis(Information.getTypeOFAction(lst.get(i)))) {
                    if (i + 1 < lst.size()) {
                        if (isClosedParenthesis(Information.getTypeOFAction(lst.get(i + 1)))) {
                            lst.remove(i);
                            lst.remove(i);
                            continue;
                        }
                    }
                }
                i++;
            }

        }
    }

    public static boolean isCountParenthesisValid(ArrayList<String> lst) {
        int i = 0;
        int countOp = 0;
        int countCl = 0;
        if (!lst.isEmpty()) {
            while (i < lst.size()) {
                if (Information.getTypeOFAction(lst.get(i)) == typeOfAction.ACTION_CLOSING_PARENTHESIS) {
                    countCl++;
                }
                if (Information.getTypeOFAction(lst.get(i)) == typeOfAction.ACTION_OPENING_PARENTHESIS) {
                    countOp++;
                }
                if (countCl > countOp)
                    return false;
                i++;
            }
            return countOp <= countCl;
        }
        return true;
    }

    private static void addParethesisForParse(ArrayList<String> lst) {
        int i = 0;
        int countOp = 0;
        int countCl = 0;

        if (!lst.isEmpty()) {
            while (i < lst.size()) {
                if (Information.getTypeOFAction(lst.get(i)) == typeOfAction.ACTION_CLOSING_PARENTHESIS) {
                    countCl++;
                }
                if (Information.getTypeOFAction(lst.get(i)) == typeOfAction.ACTION_OPENING_PARENTHESIS) {
                    countOp++;
                }
                if (countCl > countOp) {
                    lst.add(0, Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
                    countOp++;
                    i++;
                }
                i++;
            }
            if (countOp > countCl) {
                for (i = 0; i < countOp - countCl; i++) {
                    lst.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_CLOSING_PARENTHESIS));
                }
            }
        }
    }

    // спаривает числа если это нужно (при удалении знака между ними к примеру)
    private static Parse.textCursorStruct concatNumericsIfNeed(int index, ArrayList<String> lst, Parse.textCursorStruct tcs) {
        if ((index < lst.size() - 1) && (index >= 0)) {
            String elLeft = lst.get(index);
            String elRight = lst.get(index + 1);
            typeOfAction toaL = Information.getTypeOFAction(elLeft);
            typeOfAction toaR = Information.getTypeOFAction(elRight);
            if ((toaL == typeOfAction.ACTION_NUMERIC) && (toaR == typeOfAction.ACTION_NUMERIC)) {
                String outString;
                if (elLeft.contains(".")) {
                    if (elRight.contains(".")) {
                        elRight = elRight.replace(".", "");
                    }
                }
                outString = elLeft.concat(elRight);
                lst.remove(index + 1);
                lst.set(index, outString);
                tcs.setCursorPositionFromEnd(elRight.length());
            }
        }
        return tcs;
    }

//    private static void remakeOperandOperationBalance(ArrayList<String> lst){
//        int i = 0 ;
//        while(i < lst.size()){
//            typeOfAction curToa = Information.getTypeOFAction(lst.get(i));
//           if (isCommand(curToa)){
//               if ((i==0)||(i==lst.size()-1)){
//                   lst.remove(i);
//                   continue;
//               }else if (i>0){
//                  // if ()
//               }
//           }
//           i++;
//        }
//    }

    private static void remakeOperandOperationBalanceInList(ArrayList<String> list) {
        // по идее сюда залетает выражение с одной парой скобок(по краям) и всё. его надо распарсить.
        int i = 0;
        while (i < list.size()) {
            typeOfAction toa = Information.getTypeOFAction(list.get(i));
            if (toa == typeOfAction.ACTION_NUMERIC ||
                    isConst(toa)) {
                // Смотрим слева
                if (i > 0) {
                    typeOfAction leftToa = Information.getTypeOFAction(list.get(i - 1));
                    if (isConst(leftToa) || leftToa == typeOfAction.ACTION_NUMERIC || isClosedParenthesis(leftToa)) {
                        list.add(i, Information.getStringByTypeOfAction(typeOfAction.ACTION_UMNOZ));
                        continue;
                    }
                }//А теперь справа
                if (i + 1 < list.size()) {
                    typeOfAction rightToa = Information.getTypeOFAction(list.get(i + 1));
                    if (isConst(rightToa) || rightToa == typeOfAction.ACTION_NUMERIC || isOpenParentesis(rightToa) || isBigCommand(rightToa)) {
                        list.add(i + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_UMNOZ));
                        continue;
                    }
                }
                String el = list.get(i);
                if (el.contains("E")) {
                    if ((el.indexOf("E") == el.length() - 1) || ((el.indexOf("E") == el.length() - 2))) {
                        el = el.replace("E+", "");
                        el = el.replace("E", "");
                        list.set(i, el);
                    }
                }
            }
            if (isPastCommand(toa)) {
                // тут по идее только справа
                if (i + 1 < list.size()) {
                    typeOfAction rightToa = Information.getTypeOFAction(list.get(i + 1));
                    if (isConst(rightToa) || rightToa == typeOfAction.ACTION_NUMERIC || isOpenParentesis(rightToa) || isBigCommand(rightToa)) {
                        list.add(i + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_UMNOZ));
                        continue;
                    }
                }
                if (i - 1 > 0) {
                    typeOfAction leftToa = Information.getTypeOFAction(list.get(i - 1));
                    if (isPastCommand(leftToa) || isCommand(leftToa) || isBigCommand(leftToa) || isOpenParentesis(leftToa)) {
                        list.remove(i);
                        i -= 1;
                        continue;
                    }
                }
            }
            if (isCommand(toa)) {
                // Смотрим слева
                if (i > 0) {
                    typeOfAction leftToa = Information.getTypeOFAction(list.get(i - 1));
                    if (isOpenParentesis(leftToa)) {
                        if (Information.getTypeOFAction(list.get(i)) == typeOfAction.ACTION_MINUS)
                            list.add(i, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0));
                        else
                            list.remove(i);
                        continue;
                    } else if (isBigCommand(leftToa) || isDegree(leftToa)) {
                        if (Information.getTypeOFAction(list.get(i)) == typeOfAction.ACTION_MINUS) {
                            list.add(i, Information.getStringByTypeOfAction(typeOfAction.ACTION_OPENING_PARENTHESIS));
                            list.add(i, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0));
                            continue;
                        } else {
                            list.remove(i);
                            continue;
                        }
                    }
                } else {
                    if (Information.getTypeOFAction(list.get(0)) == typeOfAction.ACTION_MINUS) {
                        list.add(0, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0));
                        continue;
                    } else {
                        list.remove(0);
                        continue;
                    }
                }
                //А теперь справа
                if (i + 1 < list.size()) {
                    typeOfAction rightToa = Information.getTypeOFAction(list.get(i + 1));
                    if (isClosedParenthesis(rightToa)) {
                        list.remove(i);
                        continue;
                    }
                }
            }
            if (isClosedParenthesis(toa)) {
                // Смотрим слева
                if (i > 0) {
                    typeOfAction leftToa = Information.getTypeOFAction(list.get(i - 1));
                    if (isCommand(leftToa) || isOpenParentesis(leftToa)) {
                        list.remove(i - 1);
                        continue;
                    }
                }//А теперь справа
                if (i + 1 < list.size()) {
                    typeOfAction rightToa = Information.getTypeOFAction(list.get(i + 1));
                    if (isOpenParentesis(rightToa) || rightToa == typeOfAction.ACTION_NUMERIC || isConst(rightToa) || isBigCommand(rightToa)) {
                        list.add(i + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_UMNOZ));
                        continue;
                    }
                }
            }
            if (isOpenParentesis(toa) || isBigCommand(toa)) {
                // Смотрим слева
                if (i > 0) {
                    typeOfAction leftToa = Information.getTypeOFAction(list.get(i - 1));
                    if (isClosedParenthesis(leftToa) || leftToa == typeOfAction.ACTION_NUMERIC || isConst(leftToa)) {
                        list.add(i - 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_UMNOZ));
                        continue;
                    }
                }//А теперь справа
                if (i + 1 < list.size()) {
                    typeOfAction rightToa = Information.getTypeOFAction(list.get(i + 1));
                    if (isCommand(rightToa) || isClosedParenthesis(rightToa)) {
                        if (rightToa == typeOfAction.ACTION_MINUS)
                            list.add(i + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_NUMERIC0));
                        else
                            list.remove(i + 1);
                        continue;
                        //list.remove(i + 1);
                    }
                }
            }
            if (isDegree(toa)) {
                // Смотрим слева
                if (i > 0) {
                    typeOfAction leftToa = Information.getTypeOFAction(list.get(i - 1));
                    if (!isClosedParenthesis(leftToa) && leftToa != typeOfAction.ACTION_NUMERIC && !isConst(leftToa) && !isPastCommand(leftToa)) {
                        if (i > 1) {
                            if (Information.getTypeOFAction(list.get(i - 1)) == typeOfAction.ACTION_NUMERIC) {
                                list.remove(i);
                                list.remove(i);
                                continue;
                            } else if (Information.getTypeOFAction(list.get(i + 1)) == typeOfAction.ACTION_OPENING_PARENTHESIS) {
                                ExpressionStatus es = Parse.exp_getExpressionStatusAroundIndex(i + 1, list);
                                for (int j = 0; j <= es.getLength(); j++) {
                                    list.remove(i);
                                }
                                continue;
                            } else {
                                list.remove(i);
                                continue;
                            }


                        }
                    }
                } else {
                    list.remove(i);
                    continue;
                }

                //А теперь справа
                if (i + 1 < list.size()) {
                    typeOfAction rightToa = Information.getTypeOFAction(list.get(i + 1));
                    if (isCommand(rightToa)) {
                        list.remove(i + 1);
                        continue;
                    }
                }
            }
            i++;
        }
    }
}
