package com.blk_.calculate.parsing;

import java.io.Serializable;

public class voiceRecStruct implements Serializable {
    public enum methodOutputVoice {
        METHOD_VOICE_ADD,
        METHOD_VOICE_REPLACE,
        METHOD_VOICE_NOT_INITIALIZED
    }

    public boolean isNeed() {
        return isNeed;
    }

    public void setNeed(boolean need) {
        isNeed = need;
    }

    public boolean isDontShow() {
        return isDontShow;
    }

    public void setDontShow(boolean dontShow) {
        isDontShow = dontShow;
    }

    public methodOutputVoice getDefaultVariant() {
        return defaultVariant;
    }

    public void setDefaultVariant(methodOutputVoice defaultVariant) {
        this.defaultVariant = defaultVariant;
    }

    private boolean isNeed;
    private boolean isDontShow;
    private methodOutputVoice defaultVariant;

    public voiceRecStruct(boolean _isNeed, boolean _isDontShow, methodOutputVoice _mov) {
        this.isNeed = _isNeed;
        this.isDontShow = _isDontShow;
        this.defaultVariant = _mov;
    }
}
