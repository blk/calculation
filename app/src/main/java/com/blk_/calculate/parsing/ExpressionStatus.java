package com.blk_.calculate.parsing;

/**
 * Created by blk_ on 06.03.2018.
 * Класс - структура , отображающая состояние выражения в коллекции (к самой коллекции доступа естественно нет)
 * beginIndex - индекс начала выражения
 * endIndex - индекс окончания выражения
 * length - длина выражения
 * isActive - законченное ли оно (кажется)
 */

public class ExpressionStatus {
    private int beginIndex;

    /**
     * Геттеры сеттеры структуры
     */
    public int getBeginIndex() {
        return beginIndex;
    }

    public void setBeginIndex(int beginIndex) {
        this.beginIndex = beginIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

//    public boolean isActive() {
//        return isActive;
//    }

    public void setActive(boolean active) {
        isActive = active;
    }

    private int endIndex;
    private int length;
    private boolean isActive;

    /**
     * Конструктор. Инициализация пустыми значениями структуры.
     */
    public ExpressionStatus() {
        length = -1;
        isActive = false;
        beginIndex = -1;
        endIndex = -1;
    }
}
