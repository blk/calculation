package com.blk_.calculate.parsing;


import com.blk_.calculate.Information;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.math.Converter;

import java.util.ArrayList;

/**
 * Created by blk_ on 13.03.2018.
 * Класс предназначенный для хранения 6-ти массивов данных и работы с ними
 * 4 массива это выражение в 2-ой, 8-ой, 10-ой, 16-ой системе счисления
 * пятый массив предназначен для хранения данных в текущей системе счисления
 * шестой предназначен для хранения уже переведенного выражения в десятичной системе счисления в случае если текущая система счисления не десятичная
 * <p>
 * Такое количество массивов нужно для корректного перехода между CC , например 10.9 (10) -> 1010.1110011001 (2) -> 10.8994140625(10)
 * это происходит из за способа перевода дробных чисел в другую СС, поэтому если пользователь вводил 10.9 в десятичной, перешел в двоичную и обратно в десятичную,
 * ничего не вводя и не стирая(важно!) , то он увидит на экране 10.9 а не 10.8994140625, если пользоователь что-либо поменяет в выражении, то, безусловно число изменится
 */

public class ArraysCalculation {
    private ArrayList<String> binaryList;               // Коллекция из элементов выражения в двоичной СС
    private ArrayList<String> octalList;                // Коллекция из элементов выражения в восьмеричной СС
    private ArrayList<String> decimalList;              // Коллекция из элементов выражения в десятичной СС
    private ArrayList<String> hexodecimalList;          // Коллекция из элементов выражения в шестнадцатеричной СС
    private ArrayList<String> convertedToDecimalList;   // Коллекция из элементов выражения в десятичной СС, переведенный массив из массива в текущей СС
    private ArrayList<String> currentList;              // По сути ссылка на текущий массив ( в текущей системе счисления)

    /**
     * Конструктор класса ArrayCalculation
     * Происходит инициализация всех массивов данных
     */
    public ArraysCalculation() {
        binaryList = new ArrayList<>();
        octalList = new ArrayList<>();
        decimalList = new ArrayList<>();
        hexodecimalList = new ArrayList<>();
        convertedToDecimalList = new ArrayList<>();
        currentList = decimalList;
    }


    /**
     * Setter current list
     *
     * @param currentList массив
     */
    public void setCurrentList(ArrayList<String> currentList) {
        this.currentList = currentList;
    }

    /**
     * Getter converted to decimal list
     *
     * @return converted to decimal list
     */
    public ArrayList<String> getConvertedToDecimalList() {
        return convertedToDecimalList;
    }

    /**
     * Getter current list
     *
     * @return current list
     */
    public ArrayList<String> getCurrentList() {
        return currentList;
    }

    /**
     * Setter current numeric system
     * Происходит установка текущей системы счисления и происходит перерасчет массивов
     */
    public void setCurrentNumericSystem() {
        typeOfNumberSystem currentNumericSystem = Information.getCurrentTypeOfSystem();
        switch (currentNumericSystem) {
            case NUMBER_SYSTEM_BINARY:
                currentList = binaryList;
                break;
            case NUMBER_SYSTEM_OCTAL:
                currentList = octalList;
                break;
            case NUMBER_SYSTEM_DECIMAL:
                currentList = decimalList;
                break;
            case NUMBER_SYSTEM_HEXADECIMAL:
                currentList = hexodecimalList;
                break;
        }
        reCalculateArrays();
    }

    /**
     * Перерасчет массивов, происходт при смене текущей системы счисления.
     * Converted to decimal массив пересчитывается , т.е. берем массив соответствующий новой СС и переводим его в конвертед ту децимал массив
     */
    private void reCalculateArrays() {
        getCurrentList().clear();
        if (!getConvertedToDecimalList().isEmpty()) {
            for (String buf : getConvertedToDecimalList()) {
                typeOfAction tof = Information.getTypeOFAction(buf);
                if (tof == typeOfAction.ACTION_NUMERIC) {
                    getCurrentList().add(Converter.convertAdapter(buf, typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem()));
                } else
                    getCurrentList().add(buf);
            }
        }
    }

    /**
     * Функция пересчета массива из новой системы счисления в десятичную
     */
    public void convertToDecimalList() {
        getConvertedToDecimalList().clear();
        if (!getCurrentList().isEmpty()) {
            for (String buf : getCurrentList()) {
                typeOfAction tof = Information.getTypeOFAction(buf);
                if (tof == typeOfAction.ACTION_NUMERIC) {
                    getConvertedToDecimalList().add(Converter.convertAdapter(buf, Information.getCurrentTypeOfSystem(), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL));
                } else
                    getConvertedToDecimalList().add(buf);
            }
        }
    }

}
