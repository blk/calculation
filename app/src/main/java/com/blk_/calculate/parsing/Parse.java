package com.blk_.calculate.parsing;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.SuperscriptSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.blk_.calculate.Information;
import com.blk_.calculate.R;
import com.blk_.calculate.activities.MainActivity;
import com.blk_.calculate.db.DatabaseHandler;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.historylist.CalcList;
import com.blk_.calculate.historylist.HistoryList;
import com.blk_.calculate.logger.SingletonLogger;
import com.blk_.calculate.math.BigDecimalBlk;
import com.blk_.calculate.math.Calculate;
import com.blk_.calculate.math.Converter;
import com.blk_.calculate.styleTheme.ColorExpressionScheme;
import com.blk_.calculate.textViews.TextFragmentStruct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import static com.blk_.calculate.math.Calculate.getBigDecimalValue;
import static com.blk_.calculate.math.Calculate.isCommand;

/**
 * Created by blk_ on 22.03.2018.
 * Парсер. самый большой класс приложения
 * Здеся(тута) происходит анализ нажатых клавиш , возможность размещения элементов исходя из условий. сохранение и формирования истории вычислений.
 * Нажимание и отжимание кнопок если надо, короче делает сука всё что я не смог распихать по разным другим классам
 */

public class Parse {
    private final ArraysCalculation arrayListSymbs;
    private static ExpressionStatus exStatus;

    public HistoryList getHistoryList() {
        return historyList;
    }

    HistoryList historyList;
    private static int colorOperation, colorNumeric, colorExpression, colorConst;
    private static final String TAG = "PARSER";

    private OnTextViewChange fragmentTextListener;
    private OnTypeOfNumberSystemChanged numberSystemListener;
    private String lastValue = "";
    private typeOfAction lastAction;
    private final DatabaseHandler dbHelper;
    private final Activity activity;

    /**
     * Геттер для последнего посчитанного значения
     *
     * @return значение
     */
    public String getLastValue() {
        return lastValue;
    }


    /**
     * Геттер для массивов хранения данных
     *
     * @return массивы
     */
    public ArraysCalculation getArrayListSymbs() {
        return arrayListSymbs;
    }

    /**
     * Сеттер для текущей системы счисления, передается в класс управляющий массивами хранения данных. Передает классу событие что система счисления поменялась
     */
    public void setCurrentTypeOfNumericSystem() {
        getArrayListSymbs().setCurrentNumericSystem();
    }

    /**
     * сеттер для параметра цветовой отрисовки операции в текстовом поле
     *
     * @param _colorOperation - целое значение (взятое из ресурсов) цвета
     */
    private void setColorOperation(int _colorOperation) {
        colorOperation = _colorOperation;
    }

    /**
     * сеттер для параметра цветовой отрисовки чисел в текстовом поле
     *
     * @param _colorNumeric - целое значение (взятое из ресурсов) цвета
     */
    private void setColorNumeric(int _colorNumeric) {
        colorNumeric = _colorNumeric;
    }

    /**
     * сеттер для параметра цветовой отрисовки невалидных (незаконченных) выражений в текстовом поле
     *
     * @param _colorExpression - целое значение (взятое из ресурсов) цвета
     */
    private void setColorExpression(int _colorExpression) {
        colorExpression = _colorExpression;
    }

    /**
     * сеттер для параметра цветовой отрисовки констант в текстовом поле
     *
     * @param _colorConst - целое значение (взятое из ресурсов) цвета
     */
    private void setColorConst(int _colorConst) {
        colorConst = _colorConst;
    }

    /**
     * Паблик сеттер сразу для всех четырех цветов отображения текста
     *
     * @param num  - цвет чисел
     * @param oper - цвет операций
     * @param expr - цвет невалидного выражения
     * @param cnst - цвет констант
     */
    public void setColors(int num, int oper, int expr, int cnst) {
        if (Build.VERSION.SDK_INT >= 23) {
            setColorNumeric(activity.getResources().getColor(num, null));
            setColorOperation(activity.getResources().getColor(oper, null));
            setColorExpression(activity.getResources().getColor(expr, null));
            setColorConst(activity.getResources().getColor(cnst, null));
        } else {
            setColorNumeric(activity.getResources().getColor(num));
            setColorOperation(activity.getResources().getColor(oper));
            setColorExpression(activity.getResources().getColor(expr));
            setColorConst(activity.getResources().getColor(cnst));
        }
    }


    /**
     * Конструктор
     * Инициализация листов хранения данных
     * Инициализация историй
     * Инициализация дб
     *
     * @param activity активити
     */
    public Parse(Activity activity) {
        arrayListSymbs = new ArraysCalculation();
        hist_initHistory();
        this.activity = activity;
        exStatus = new ExpressionStatus();
        lastAction = typeOfAction.UNKNOWN_ACTION;
        setCurrentTypeOfNumericSystem();
        dbHelper = new DatabaseHandler(activity);
        dbHelper.initDB(this);
        historyList.setList(dbHelper.getAllList());
        Information.setHistoryList(dbHelper.getAllList());
        //testList = new ArrayList<String>();
    }

    /**
     * Добавление листенера
     *
     * @param otvc - листенер
     */
    public void addTextListener(OnTextViewChange otvc) {
        fragmentTextListener = otvc;
    }

    /**
     * листенер для изменения системы счисления , передается во фрагмент через маин активити
     *
     * @param otvc - листенер
     */
    public void addNumberSystemListener(OnTypeOfNumberSystemChanged otvc) {
        numberSystemListener = otvc;
    }

    /**
     * метод который вызывается при переворачивании и всё такое, из бандл цепляются данные, нужные для парсера
     *
     * @param sis -  переменная бандл
     */
    public void onSaveInstance(Bundle sis) {
        if (sis != null) {
            getArrayListSymbs().setCurrentList(sis.getStringArrayList(MainActivity.KEY_VALUE_VIEW));
            getArrayListSymbs().convertToDecimalList();
            ArrayList<String> bufList = getArrayListSymbs().getCurrentList();

            if (!bufList.isEmpty())
                lastAction = Information.getTypeOFAction(bufList.get(bufList.size() - 1));
            else
                lastAction = typeOfAction.UNKNOWN_ACTION;
            if (sis.getString(MainActivity.KEY_VALUE_RESULT) != null)
                if (!Objects.requireNonNull(sis.getString(MainActivity.KEY_VALUE_RESULT)).isEmpty())
                    parseView_addSummaryToResult(getBigDecimalValue(sis.getString(MainActivity.KEY_VALUE_RESULT)));
        }
    }

    public static ArrayList<String> parseManualArrayList(String listAsString) {
        try {

            Log.d("PARSEVOICE", "INPUT STRING   = " + listAsString);
            listAsString = listAsString.replace(",", ".");
            ArrayList<String> outlist = new ArrayList<>(Arrays.asList(listAsString.split(" ")));
            outlist = preparse(outlist);
            ArrayList<String> l = new ArrayList<>();
            ListWithCusorPos lwcp = new ListWithCusorPos(l, new Parse.textCursorStruct(-1, 0));

            for (String el : outlist) {
                typeOfAction toa = Information.getTypeOFAction(el.toLowerCase());

                switch (toa) {
                    case ACTION_NUMERIC: {
                        lwcp = AddingElement.addNumericExpression(lwcp.getList(), el, lwcp.getTcs());
                        break;
                    }
                    case UNKNOWN_ACTION:
                        break;
                    default: {
                        lwcp = AddingElement.elements_addOperationToList(toa, lwcp);
                        break;
                    }
                }
            }
            return lwcp.getList();
        } catch (Throwable ex) {
            SingletonLogger.getInstance().onException(ex);
        }
        return null;
    }

    private static ArrayList<String> preparse(ArrayList<String> lst) {
        int i = 0;
        while (i < lst.size()) {
            if (i + 1 < lst.size()) {
                if (lst.get(i).equals("натуральный"))
                    if (lst.get(i + 1).equals("логарифм")) {
                        lst.set(i, "натуральный логарифм");
                        lst.remove(i + 1);
                        continue;
                    }
                if (lst.get(i).equals("десятичный"))
                    if (lst.get(i + 1).equals("логарифм")) {
                        lst.set(i, "десятичный логарифм");
                        lst.remove(i + 1);
                        continue;
                    }
            }
            if (lst.get(i).equals("логарифм"))
                lst.set(i, "десятичный логарифм");
            if (lst.get(i).contains("%")) {
                {
                    if (lst.get(i).length() > 1) {
                        lst.add(i, lst.get(i).replace("%", ""));
                        lst.set(i + 1, Information.getStringByTypeOfAction(typeOfAction.ACTION_PERCENT));
                    }

                }
            }
            i++;
        }
        return lst;
    }

    public void replaceFromVoice(ArrayList<String> list, boolean needToConca) {
//        CalcList pastedList = new CalcList
//                (list,typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL,parseView_staticParseListToSpanString(list,typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL,null)
//                        ,parseView_removeInsignificantZerosFromElement(String.valueOf(Calculate.calculate(
//                        AddingElement.tryToFuckExpressiontoGood((ArrayList<String>) list.clone())
//                ).getValue())),null);
        //CalcList pastedList = historyList.getList().get(id);
        if (!list.isEmpty()) {
            if (hist_isNeedToSaveHistory())
                hist_writeToHistory();

            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_VIEW, "", null), null);
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_PARENTHESIS, "", null), null);
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_RESULT, "", null), null);
            if (needToConca) {
                ArrayList<String> bufList = getArrayListSymbs().getCurrentList();
                if (!bufList.isEmpty()) {
                    if (Information.getTypeOFAction(bufList.get(bufList.size() - 1)) == typeOfAction.ACTION_NUMERIC)
                        if (Information.getTypeOFAction(list.get(0)) == typeOfAction.ACTION_NUMERIC) {
                            String el = bufList.get(bufList.size() - 1) + list.get(0);
                            bufList.set(bufList.size() - 1, el);
                            list.remove(0);
                        }
                }
                bufList.addAll(list);
                getArrayListSymbs().setCurrentList(bufList);
            } else {
                getArrayListSymbs().setCurrentList(list);
            }
            getArrayListSymbs().convertToDecimalList();
            ArrayList<String> bufList = getArrayListSymbs().getCurrentList();

            if (!bufList.isEmpty())
                lastAction = Information.getTypeOFAction(bufList.get(bufList.size() - 1));
            else
                lastAction = typeOfAction.UNKNOWN_ACTION;

            parseView_parseAndViewArrayList();
            parseView_calculateAndViewPreSummary();

            Information.setCurrentTypeOfSystem(typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL);
            Information.setIsNeedButtonRadian(parse_isNeedToShowRadian(null));
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_UPDATE, null, null), null);
            numberSystemListener.onTypeOfNumberSystemChanged(typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL);
        }
    }

    /**
     * Метод записи в историю
     */
    private void hist_writeToHistory() {
        if (historyList.addNewHistory(new ArrayList<>(getArrayListSymbs().getCurrentList()), Information.getCurrentTypeOfSystem(), parseView_staticParseListToSpanString(getArrayListSymbs().getCurrentList(), Information.getCurrentTypeOfSystem(), null), lastValue)) {
            dbHelper.addCalcList(historyList.getList().get(historyList.getList().size() - 1));
            Information.setHistoryList(historyList.getList());
        }
    }

    /**
     * Инициализация истории
     */
    private void hist_initHistory() {
        historyList = new HistoryList();
    }

    /**
     * Пока пустой метод возвращающий true
     * нужно ли сохранение истории
     *
     * @return -  true/false
     */
    private boolean hist_isNeedToSaveHistory() {
        //TODO - если нужно доработать (наверно нужно)
        return true;
    }

    /**
     * Метод выгружающий выражение из истории
     *
     * @param id - айдишник ( по сути номер листитема)
     */
    public void hist_replaceFromHistory(int id) {
        Log.d(TAG, "REPLACER" + String.valueOf(id));

        CalcList pastedList = historyList.getList().get(id);
        if (pastedList.getViewableSystem() != typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL) {
            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                String msg = Information.getStringByTypeOfNumSystem(pastedList.getViewableSystem()) + " система счисления не поддерживается в данной ориентации экрана" +
                        ", пожалуйста поверните устройство";
                Toast toast = Toast.makeText(activity.getApplicationContext(),
                        msg,
                        Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;
            }
        }
        if (hist_isNeedToSaveHistory())
            hist_writeToHistory();

        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_VIEW, "", null), null);
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_PARENTHESIS, "", null), null);
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_RESULT, "", null), null);
        //parseView_setButtonDPressed(false);
        getArrayListSymbs().setCurrentList(pastedList.getArrayList());
        getArrayListSymbs().convertToDecimalList();
        ArrayList<String> bufList = getArrayListSymbs().getCurrentList();
        if (!bufList.isEmpty())
            lastAction = Information.getTypeOFAction(bufList.get(bufList.size() - 1));
        else
            lastAction = typeOfAction.UNKNOWN_ACTION;
        parseView_addSummaryToResult(getBigDecimalValue(Converter.convertAdapter(pastedList.getResult(), pastedList.getViewableSystem(), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL)));

        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_VIEW, null, pastedList.getText()), null);
        Information.setCurrentTypeOfSystem(pastedList.getViewableSystem());

        Information.setIsNeedButtonRadian(parse_isNeedToShowRadian(null));
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_UPDATE, null, null), null);
        numberSystemListener.onTypeOfNumberSystemChanged(pastedList.getViewableSystem());
        //parseView_parseAndViewArrayList();
        //ТУТ НАЖИМАТЬ КНОПКУ ПРОКИНУВ СЛУШАТЕЛЬ В КЛАСС ДОП КНОПОК И РЕАЛИЗОВАВ ЕГО ТАМ,А ТУТ ДОБАВИТЬ ИНТЕРФЕЙС! TODO : DONE
    }

    /**
     * Геттер управляющего базы данных
     *
     * @return - dbhelper
     */
    public DatabaseHandler hist_getDbHelper() {
        return dbHelper;
    }

    /**
     * Интерфейс изменения текста, слушается фрагментом с текстовыми полями, и перерисовывается текст
     */
    public interface OnTextViewChange {
        void onTextViewChange(TextFragmentStruct tfs, Animation animation);
    }

    /**
     * Интерфейс изменения системы счисления через парсер (при выгрузке из истории)
     */
    public interface OnTypeOfNumberSystemChanged {
        void onTypeOfNumberSystemChanged(typeOfNumberSystem tons);
    }


    /**
     * Метод удаляет нензначащие нули вначале и в конце выражения
     *
     * @param element - входящая строка
     * @return - отформатированная строка
     */
    public static String parseView_removeInsignificantZerosFromElement(String element) {
        String returnedString = element;
        if (Information.getTypeOFAction(element) == typeOfAction.ACTION_NUMERIC) {
            // Проверяем нули в конце(после запятой)
            int idxZ = element.indexOf(".");
            if (idxZ == -1)
                returnedString = element;
            else {
                if (idxZ != element.length() - 1) {
                    String eTail = "";
                    String bufString = element.substring(idxZ + 1);
                    if (bufString.contains("E")) {
                        eTail = bufString.substring(bufString.lastIndexOf("E"));
                        bufString = bufString.substring(0, bufString.lastIndexOf('E'));

                    }

                    int i = bufString.length() - 1;
                    int count = 0;
                    while (i >= 0) {
                        if (bufString.charAt(i) == '0')
                            count++;
                        else break;
                        i--;
                    }
                    if (count != 0) {
                        if (count == bufString.length())
                            return element.substring(0, idxZ);
                        returnedString = element.substring(0, idxZ) + "." + bufString.substring(0, bufString.length() - count) + eTail;
                    } else
                        returnedString = element;
                }
            }
            // А теперь спереди
            String bufString = returnedString;
            if ((bufString.length() > 1) && (((bufString.charAt(0) == '0') || (bufString.charAt(0) == '-')) && (bufString.charAt(1) != '.'))) {
                int endIndex = bufString.indexOf('.') > 1 ? bufString.indexOf('.') - 1 : bufString.length() - 1;
                int i = 0;
                int countZer = 0;
                while (i < endIndex) {
                    if (bufString.charAt(i) == '0')
                        countZer++;
                    else if (bufString.charAt(i) != '-')
                        break;
                    i++;
                }
                if (bufString.charAt(0) == '-') {
                    returnedString = "-" + bufString.substring(countZer + 1, bufString.length());
                } else {
                    returnedString = bufString.substring(countZer, bufString.length());
                }

            }
        }
        return returnedString;
    }

    /**
     * Метод удаляющий незначащие нули из массива элементов
     *
     * @param isFullExpression - полное ли выражение
     * @param list             - массив
     */
    private static void parseView_removeInsignificantZerosFromList(boolean isFullExpression, ArrayList<String> list, int notDelPosition) {
        if (!list.isEmpty()) {
            int length;
//            if (!isFullExpression)
//                length = list.size();
//            else
//                length = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                if (i != notDelPosition)
                    list.set(i, parseView_removeInsignificantZerosFromElement(list.get(i)));
            }
        }
    }

    /**
     * Обертка парсинга и вывода на экран текущего листа
     */
    public void parseView_parseAndViewArrayList() {
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_VIEW, "", null), null);
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_VIEW, null, parseView_parseArrayList(null, Information.getCurrentTypeOfSystem())), null);
    }

    /**
     * Основной парсер элементов
     *
     * @param list - список элементов для вывода
     * @param tos  - система счисления
     * @return - spannable строка для вывода на экран
     */
    private SpannableStringBuilder parseView_parseArrayList(ArrayList<String> list, typeOfNumberSystem tos) {
        //textView.setText("");
        SpannableStringBuilder textToView = new SpannableStringBuilder();
        typeOfNumberSystem currentListTos = tos == null ? Information.getCurrentTypeOfSystem() : tos;
        int counterDegree = 0;
        boolean isListNull = false;
        if (list == null) {
            list = getArrayListSymbs().getCurrentList();
            isListNull = true;
        }
        if (isListNull) {
            Information.setIsNeedButtonRadian(parse_isNeedToShowRadian(null));
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_UPDATE, null, null), null);
        }
        exStatus = exp_getExpressionStatusAroundIndex(list.size() - 1, list); // TODO заменить на короткий
        int indx = lwcp == null ? list.size() : lwcp.getTcs().getIndexInArrayList();
        parseView_removeInsignificantZerosFromList(isListNull, list, indx);
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                String bufElement = list.get(i);
                typeOfAction tof = Information.getTypeOFAction(bufElement);
                switch (tof) {
                    case ACTION_DEGREE: {
                        counterDegree++;
                        textToView.append(parseView_viewStringElement(bufElement, tof, i, currentListTos, null));

                        break;
                    }
                    case ACTION_PERCENT: {
                        textToView.append(parseView_viewStringElement(bufElement, typeOfAction.ACTION_PERCENT, i, currentListTos, null));
                        counterDegree = 0;
                        break;
                    }
                    case ACTION_SQRT:
                    case ACTION_SINX:
                    case ACTION_COSX:
                    case ACTION_LN:
                    case ACTION_LG:
                    case ACTION_FLOAT:
                    case ACTION_PLUS:
                    case ACTION_MINUS:
                    case ACTION_UMNOZ:
                    case ACTION_DELEN:
                    case ACTION_FACTORIAL:
                    case ACTION_OPENING_PARENTHESIS:
                    case ACTION_CLOSING_PARENTHESIS:
                    case ACTION_CONST_PI:
                    case ACTION_CONST_E:
                    case UNKNOWN_ACTION:
                    case ACTION_NUMERIC: {
                        if (!exp_isExpressionUnderDegreeBeforeIndex(i, list)) {
                            textToView.append(parseView_viewStringElement(bufElement, tof, i, currentListTos, null));
                            counterDegree = 0;
                        } else {
                            textToView.append(parseView_viewUpperStringElement(bufElement, tof, i, currentListTos, null));
                            counterDegree++;
                        }
                        break;
                    }
                }
            }

        }
        return textToView;
    }

    /**
     * Аналогичный парсер элементов, но без нажимания кнопочек и всего такого , просто лист - просто строка на выходе. и да, он статик
     *
     * @param inputList - лист
     * @param tos       - тип системы счисления
     * @param cs        - цветовая схема
     * @return - spannable строка
     */
    public static SpannableStringBuilder parseView_staticParseListToSpanString(ArrayList<String> inputList, typeOfNumberSystem tos, ColorExpressionScheme cs) {
        ArrayList<String> list = new ArrayList<>(inputList);
        SpannableStringBuilder textToView = new SpannableStringBuilder();
        typeOfNumberSystem currentListTos = tos == null ? Information.getCurrentTypeOfSystem() : tos;
        if (list == null) {
            return textToView;
        }
        exStatus = exp_getExpressionStatusAroundIndex(list.size() - 1, list); // TODO заменить на короткий
        parseView_removeInsignificantZerosFromList(true, list, list.size() - 1);
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                String bufElement = list.get(i);
                typeOfAction tof = Information.getTypeOFAction(bufElement);
                switch (tof) {
                    case ACTION_DEGREE:
                        break;
                    case ACTION_PERCENT: {
                        textToView.append(parseView_viewStringElement(bufElement, typeOfAction.ACTION_PERCENT, i, currentListTos, cs));
                        break;
                    }
                    case ACTION_SQRT:
                    case ACTION_SINX:
                    case ACTION_COSX:
                    case ACTION_LN:
                    case ACTION_LG:
                    case ACTION_FLOAT:
                    case ACTION_PLUS:
                    case ACTION_MINUS:
                    case ACTION_UMNOZ:
                    case ACTION_DELEN:
                    case ACTION_FACTORIAL:
                    case ACTION_OPENING_PARENTHESIS:
                    case ACTION_CLOSING_PARENTHESIS:
                    case ACTION_CONST_PI:
                    case ACTION_CONST_E:
                    case ACTION_NUMERIC: {
                        if (!exp_isExpressionUnderDegreeBeforeIndex(i, list)) {
                            textToView.append(parseView_viewStringElement(bufElement, tof, i, currentListTos, cs));
                        } else {
                            textToView.append(parseView_viewUpperStringElement(bufElement, tof, i, currentListTos, cs));
                        }
                        break;
                    }
                }
            }
        }
        return textToView;
    }


    /**
     * Метод для парсера. Красит и формирует обычную строку
     *
     * @param bufElement - элемент
     * @param toa        - действие , которое обозначает этот элемент
     * @param index      - индекс в листе (для выборки по индексу, заморожено)
     * @param _tos       - тип системы счисления
     * @param cs         - цветовая  схема
     * @return - spannable строка
     */
    private static SpannableStringBuilder parseView_viewStringElement(String bufElement, typeOfAction toa, int index, typeOfNumberSystem _tos, ColorExpressionScheme cs) {
        String outputString;
        int colorCode;
        int lgt;
        switch (toa) {
            case ACTION_PLUS:
            case ACTION_MINUS:
            case ACTION_UMNOZ:
            case ACTION_DELEN:
                outputString = " " + bufElement + " ";
                colorCode = cs == null ? colorOperation : cs.getColorOperation();
                break;
            case ACTION_CONST_E:
            case ACTION_CONST_PI:
                outputString = Information.getStringForConstByTypeOfAction(toa);
                colorCode = cs == null ? colorConst : cs.getColorConst();
                break;

            default:
                colorCode = cs == null ? colorNumeric : cs.getColorNumeric();
                outputString = bufElement;
                break;
        }
//        if ((index >= exStatus.getBeginIndex())&&(index <= exStatus.getEndIndex())&&(exStatus.isActive()))
//            colorCode = cs==null?colorExpression:cs.getColorExpression();
        if ((toa == typeOfAction.ACTION_NUMERIC) || (toa == typeOfAction.ACTION_FLOAT)) {
            outputString = parseView_addSeparatorsToString(outputString, _tos);
        }
        lgt = outputString.length();
        final SpannableStringBuilder text = new SpannableStringBuilder(outputString);
        final ForegroundColorSpan style = new ForegroundColorSpan(colorCode);
        if (toa != typeOfAction.ACTION_DEGREE)
            text.setSpan(style, 0, lgt, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        else {
            text.setSpan(new SuperscriptSpan(), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new RelativeSizeSpan(0.4f), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(style, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return text;
    }

    /**
     * Метод для парсера. Красит и формирует строку верхнего регистра (для степени)
     *
     * @param bufElement - элемент
     * @param toa        - действие , которое обозначает этот элемент
     * @param index      - индекс в листе (для выборки по индексу, заморожено)
     * @param _tos       - тип системы счисления
     * @param cs         - цветовая схема
     * @return - spannable строка
     */
    private static SpannableStringBuilder parseView_viewUpperStringElement(String bufElement, typeOfAction toa, int index, typeOfNumberSystem _tos, ColorExpressionScheme cs) {
        int colorCode;
        switch (toa) {
            case ACTION_PLUS:
            case ACTION_MINUS:
            case ACTION_UMNOZ:
            case ACTION_DELEN:
                colorCode = cs == null ? colorOperation : cs.getColorOperation();
                break;
            case ACTION_CONST_E:
            case ACTION_CONST_PI:
                bufElement = Information.getStringForConstByTypeOfAction(toa);
                colorCode = cs == null ? colorConst : cs.getColorConst();
                break;
            default:
                colorCode = cs == null ? colorNumeric : cs.getColorNumeric();
                break;
        }
//        if ((index >= exStatus.getBeginIndex())&&(index <= exStatus.getEndIndex())&&(exStatus.isActive()))
//            colorCode = cs==null?colorExpression:cs.getColorExpression();
        final ForegroundColorSpan style = new ForegroundColorSpan(colorCode);
        if ((toa == typeOfAction.ACTION_NUMERIC) || (toa == typeOfAction.ACTION_FLOAT)) {
            bufElement = parseView_addSeparatorsToString(bufElement, _tos);
        }
        SpannableStringBuilder text = new SpannableStringBuilder(bufElement);
        if ((toa == typeOfAction.ACTION_PLUS) ||
                (toa == typeOfAction.ACTION_MINUS) ||
                (toa == typeOfAction.ACTION_UMNOZ) ||
                (toa == typeOfAction.ACTION_DELEN)) {
            text.insert(0, " ");
            text.append(" ");
        }
        text.setSpan(new SuperscriptSpan(), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(0.7f), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(style, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return text;
    }

    /**
     * метод добавляющий разрядные разделители для чисел исходя из системы счисления
     *
     * @param buf  - строка входящая
     * @param _tos - система счисления
     * @return строка с разделителями
     */
    public static String parseView_addSeparatorsToString(String buf, typeOfNumberSystem _tos) {
        if (buf.equals(Information.getStringForViewNaN()) || (buf.equals(Information.getStringForViewInfinity())))
            return buf;
        String returnedString = buf;
        if (buf.isEmpty())
            return "";
        if (_tos == null)
            return "";
        int lengthWordSeparator = 4;
        switch (_tos) {
            case NUMBER_SYSTEM_BINARY:
            case NUMBER_SYSTEM_HEXADECIMAL: {
                lengthWordSeparator = 4;
                break;
            }
            case NUMBER_SYSTEM_OCTAL:
            case NUMBER_SYSTEM_DECIMAL: {
                lengthWordSeparator = 3;
                break;
            }
        }
        String bufStringR = "";
        int i;
        if (buf.indexOf('.') > 0) {
            i = buf.indexOf('.') - 1;
            bufStringR = buf.substring(i + 1, buf.length());
            buf = buf.substring(0, i + 1);
        } else
            i = buf.length() - 1;

        if (buf.length() <= lengthWordSeparator)
            return returnedString;
        else {
            String bufString = "";
            while (i >= 0) {
                if ((i + 1) < lengthWordSeparator)
                    lengthWordSeparator = i + 1;
                String str = returnedString.substring(i - lengthWordSeparator + 1, i + 1);
                bufString = bufString.isEmpty() ? str : str + " " + bufString;
                i -= lengthWordSeparator;
            }
            returnedString = bufString + bufStringR;
        }
        return returnedString;
    }

    /**
     * Подсчет и вывод результата в текстовое поле результата
     */
    public void parseView_calculateAndViewPreSummary() {
        parseView_addSummaryToResult(parse_calculatePreSum());
    }

    /**
     * Увеличение размера текста (единоразово) при нажатии кнопки равно, текст переносится из поля результата в поле выражения
     * Пока заморожено за (скорее всего) ненадобностью
     */
//    private void parseView_setTextViewBigSize() {
//        Log.d(TAG,"line count = "+String.valueOf(textView.getLineCount()));
//        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,Information.getDefaultTextViewSize()*((float)1.25));
//        textView.setTextColor(colorNumeric);
//        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_UPDATE,null,null));
//    }
    private String convertBigDecimalToDoubleView(BigDecimalBlk bd) {
        try {

            if ((bd.isInfinity()) || (bd.isNaN()))
                return bd.isInfinity() ? Information.getStringForViewInfinity() : Information.getStringForViewNaN();
            if (bd.getValue().compareTo(BigDecimal.ZERO) == 0)
                return BigDecimal.ZERO.toString();

            String v = bd.getStringValue();

            if (v.toUpperCase().contains("E")) {

                return v;
            }
            String minus = v.contains("-") ? "-" : "";
            v = v.replace("-", "");


            int lengthBefFloat = v.indexOf('.') != -1 ? v.substring(0, v.indexOf('.')).length() : v.length();
            int floatLeng = v.indexOf('.') == -1 ? 0 : 1;
            int lengthAfterFloat = v.indexOf('.') != -1 ? v.substring(v.indexOf('.')).length() - 1 : 0;
            if (lengthBefFloat < Information.getAccuracy()) {
                if (lengthAfterFloat > Information.getAccuracy())
                    lengthAfterFloat = Information.getAccuracy();
                return minus + v.substring(0, lengthBefFloat + floatLeng + lengthAfterFloat);

            }
            if ((lengthAfterFloat + lengthBefFloat + 1) > (Information.getAccuracy() * 2)) {
                int indexFloat = v.length() - 1;
                if (v.contains(".")) {
                    indexFloat = v.indexOf('.');
                }
                String newS = v.charAt(0) + "." + (v.substring(1).replace(".", "")).substring(0, Information.getAccuracy());
                int count = indexFloat;
                // BigDecimal bb = new BigDecimal(newS).round(new MathContext(Information.getAccuracy(), RoundingMode.HALF_UP));
                return minus + newS + "E" + count;
            }
            return bd.getStringValue();
        } catch (Throwable ex) {
            SingletonLogger.getInstance().onException(ex);
        }
        return "";
    }

    /**
     * Добавление результата в поле результата
     *
     * @param _v - результат для вывода
     */
    private void parseView_addSummaryToResult(BigDecimalBlk _v) {
        lastValue = parseView_removeInsignificantZerosFromElement(Converter.convertAdapter(_v.getStringValue(), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem()));
        String forview = parseView_removeInsignificantZerosFromElement(Converter.convertAdapter(convertBigDecimalToDoubleView(_v), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, Information.getCurrentTypeOfSystem()));
        //Animation anim = AnimationUtils.loadAnimation(activity, R.anim.textresultanimation);
        // Log.d("NEWDECIMALTODOUBLE",convertBigDecimalToDoubleView(_v));
        Log.d(TAG, "вызов addSummary(double _v) с параметром " + String.valueOf(_v.getValue()));
        {
            if (_v.isNaN()) {
                fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_RESULT, Information.getStringForViewNaN(), null), null);
                lastValue = Information.getStringForViewNaN();
            } else if (_v.isInfinity()) {
                fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_RESULT, Information.getStringForViewInfinity(), null), null);
                lastValue = Information.getStringForViewInfinity();
            } else
                fragmentTextListener.onTextViewChange(new TextFragmentStruct
                        (TextFragmentStruct.textField.TEXT_RESULT,
                                parseView_addSeparatorsToString(forview, Information.getCurrentTypeOfSystem()), null), null /*anim*/);
        }
    }


    /**
     * Добавление результата в поле выражений(при нажатии кнопки равно)
     */
    private void parseView_addSummaryToView(ArrayList<String> lst) {
        SpannableStringBuilder _v = parseView_parseArrayList(lst, Information.getCurrentTypeOfSystem());
        Log.d(TAG, "вызов addSummary(double _v) с парамертром " + String.valueOf(_v));
        typeOfAction toa = typeOfAction.ACTION_NUMERIC;


        if ((_v.toString().equals(Information.getStringForViewInfinity()) || (_v.toString().equals(Information.getStringForViewNaN()))))
            toa = typeOfAction.UNKNOWN_ACTION;
        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.textviewanimation);
        fragmentTextListener.onTextViewChange(
                new TextFragmentStruct(
                        TextFragmentStruct.textField.TEXT_VIEW, null, _v), anim
        );
    }


    /**
     * Вычисление предварительной суммы , для вывода резульата онлайн, без нажатия кнопки равно
     *
     * @return - результат
     */
    public BigDecimalBlk parse_calculatePreSum() {
        ArrayList<String> bufList = getArrayListSymbs().getConvertedToDecimalList();
        if (!bufList.isEmpty()) {
            //ArrayList<String> cloneBuf = new ArrayList<>(bufList);
            //parse_tryToMakeValidExpression(cloneBuf);
            //return new BigDecimalBlk(0);

            try {
                return Calculate.calculate(AddingElement.tryToFuckExpressiontoGood(bufList));
            } catch (Throwable throwable) {
                SingletonLogger.getInstance().onException(throwable);
            }
        }
        return new BigDecimalBlk(null, true, false);
    }

    /**
     * Метод возвращает , нужно ли показывать кнопку градусов-радиан, по сути ищет упоминание в листе синусов, косинусов, и пи
     *
     * @param list - лист
     * @return - true/false
     */
    private boolean parse_isNeedToShowRadian(ArrayList<String> list) {
        int i;
        ArrayList<String> bufList;
        if (list == null)
            bufList = getArrayListSymbs().getCurrentList();
        else
            bufList = list;
        for (i = 0; i < bufList.size(); i++) {
            typeOfAction toa = Information.getTypeOFAction(bufList.get(i));
            switch (toa) {
                case ACTION_CONST_PI:
                case ACTION_COSX:
                case ACTION_SINX: {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Выгрузка значения из памяти ( не из истории) а из блока м+ м- мс мр
     *
     * @param element - значение
     */
    public textCursorStruct memory_uploadFromMemory(String element, textCursorStruct tcs) {
        ListWithCusorPos bl = null;
        if ((!element.isEmpty()) && (element.charAt(0) == '-')) {
            bl = AddingElement.elements_addOperationToList(typeOfAction.ACTION_MINUS, new ListWithCusorPos(getArrayListSymbs().getCurrentList(), tcs));
            element = element.substring(1);
        }
        if (bl != null) {
            getArrayListSymbs().setCurrentList(bl.getList());
        }
        bl = AddingElement.addNumericExpression(getArrayListSymbs().getCurrentList(), element, tcs);

        getArrayListSymbs().setCurrentList(bl.getList());
        parseView_parseAndViewArrayList();
        return bl.getTcs();
//        if (!bufList.isEmpty())  {
//            if (availability_isAvailable(typeOfAction.ACTION_NUMERIC) && (Information.getTypeOFAction(bufList.get(bufList.size() - 1)) != typeOfAction.ACTION_NUMERIC)) {
//                bufList.add(element);
//                lastAction = typeOfAction.ACTION_NUMERIC;
//                parseView_parseAndViewArrayList();
//            }
//        } else {
//            bufList.add(element);
//            lastAction = typeOfAction.ACTION_NUMERIC;
//            parseView_parseAndViewArrayList();
//        }
    }

    //Сюда передаем только копию списка (!!!!) иначе порушится отображение и оно будет принудительно думать за юзера


    /*
     * БЛОК ФУНКЦИЙ ПАРСЕРА, ОТВЕЧАЮЩИЙ ЗА ПОИСК ВАЛИДНЫХ ВЫРАЖЕНИЙ В РАЗНЫХ СИТУАЦИЯХ И РАЗЛИЧНЫХ НЕБОЛЬШИХ МЕТОДОВ ДЛЯ ЭТОГО.
     * НУЖНО ДЛЯ ТОГО ЧТОБЫ ТОЧНО ОПРЕДЕЛЯТЬ ГРАНИЦЫ ВАЛИДНОГО ВЫРАЖЕНИЯ ПОД ТЕКУЩИМ ИНДЕКСОМ И Т Д
     * НЕВЕРОЯТНО ПОМОГАЕТ ОПРЕДЕЛЯТЬ ВАЛИДНОСТЬ КОЛИЧЕСТВА СКОБОК, НЕ ПОЗВОЛЯЕТ ПОСТАВИТЬ СКОБОК БОЛЬШЕ ЧЕМ НУЖНО
     */

    /**
     * Выявляет , находится ли выражение перед idx в листе list под степенью
     *
     * @param idx  - индекс
     * @param list - список
     * @return - true/false
     */
    public static boolean exp_isExpressionUnderDegreeBeforeIndex(int idx, ArrayList<String> list) {
        int lgt;
        int nearestDegreeIndex;
        nearestDegreeIndex = list.subList(0, idx).lastIndexOf("^");
        if (nearestDegreeIndex == -1)
            return false;
        else {
            Log.d(TAG, "calling isExpressionUnderDegreeBeforeIndex with index "
                    + String.valueOf(idx) + "lastdegr " + String.valueOf(nearestDegreeIndex));
            lgt = exp_getLengthFullExpressionAfterPLace(nearestDegreeIndex, list);
            return lgt + nearestDegreeIndex > idx - 1;
        }
    }

    /**
     * Возвращает длину полного выражения после индекса
     *
     * @param idx  - индекс
     * @param list - списко
     * @return длина
     */
    private static int exp_getLengthFullExpressionAfterPLace(int idx, ArrayList<String> list) {
        int countOpen = 0;
        int countClose = 0;
        int i;
        int mainCount;
        if ((Information.getTypeOFAction(list.get(idx + 1)) == typeOfAction.ACTION_NUMERIC)
                || ((Information.getTypeOFAction(list.get(idx + 1)) == typeOfAction.ACTION_CONST_PI))
                || ((Information.getTypeOFAction(list.get(idx + 1)) == typeOfAction.ACTION_CONST_E))) {
            Log.d(TAG, "return numeric typeofaction in index " + String.valueOf(idx));
            return 1;
        } else if (Calculate.isAloneSQRTWithNumeric(idx, list))
            return 2;
        for (i = idx + 1, mainCount = 0; i < list.size(); i++) {
            mainCount++;
            if (Calculate.isOpeningParenthesis(list.get(i)))
                countOpen++;
            if (Calculate.isClosingParenthesis(list.get(i))) {
                countClose++;
                if (countClose == countOpen)
                    return mainCount;
            }
        }
        return mainCount;
    }

    /**
     * Возвращает , можно ли вставить закрывающуюся скобку в конец листа
     *
     * @return true/false
     */
    private boolean exp_canPutParenthisInTheEnd() {
        ExpressionStatus i;
        ArrayList<String> bufList = getArrayListSymbs().getCurrentList();
        i = exp_getExpressionStatusShortLengthAroundIndex(bufList.size() - 1, bufList);

        return (i.getLength() == 1) && (bufList.size() == 1) &&
                (Information.getTypeOFAction(bufList.get(bufList.size() - 1)) == typeOfAction.ACTION_NUMERIC) ||
                i.getLength() == bufList.size() && (!(bufList.get(i.getBeginIndex()).equals("(") && bufList.get(i.getEndIndex()).equals(")")) ||
                        (exp_getExpressionStatusShortLengthAroundIndex(bufList.size() - 2, bufList).getBeginIndex() != exp_getExpressionStatusShortLengthAroundIndex(1, bufList).getBeginIndex()));
    }

    /**
     * Возвращает валидное ли выражение (количество открывающихся скобок равно количеству закрывающихся)
     *
     * @param start индекс "от"
     * @param end   индекс "до"
     * @param list  список
     * @return true/false
     */
    private static boolean exp_isValidExpression(int start, int end, ArrayList<String> list) {
        int i;
        int countOp = 0, countClo = 0;
        for (i = start; i < end; i++) {
            if (list.get(i).equals("("))
                countOp++;
            if (list.get(i).equals(")"))
                countClo++;
        }
        return countClo == countOp || (countClo + countOp == 0) && (end - start == 1);
    }

    /**
     * Возвращает валидное ли выражение (количество открывающихся скобок равно количеству закрывающихся)
     *
     * @param list список
     * @return true/false
     */
    private static boolean exp_isCountParenthisValid(ArrayList<String> list) {
        int countUp = 0;
        int countDown = 0;
        for (int i = 0; i < list.size(); i++) {
            if (Information.getTypeOFAction(list.get(i)) == typeOfAction.ACTION_OPENING_PARENTHESIS) {
                countUp++;
            }
            if (Information.getTypeOFAction(list.get(i)) == typeOfAction.ACTION_CLOSING_PARENTHESIS) {
                countDown++;
            }
        }
        return (countDown == countUp);
    }

    /**
     * Возвращает статус выражения вокруг указанного индекса
     *
     * @param idx  - индекс
     * @param list - лист
     * @return - структура со статусом выражения
     */
    public static ExpressionStatus exp_getExpressionStatusAroundIndex(int idx, ArrayList<String> list) {
        int countOpen = 0;
        int countClose = 0;
        int i;
        int leftSc = -1;
        int rightSc = -1;
        for (i = idx; i >= 0; i--) {
            if (Calculate.isClosingParenthesis(list.get(i)))
                countClose++;
            if (Calculate.isOpeningParenthesis(list.get(i)))
                countOpen++;
            if (countOpen - 1 == countClose) {
                leftSc = i;
                break;
            }
        }
        if (leftSc == -1)
            leftSc = 0;
        //TODO proverit 4to voobse  net scobok
        countClose = 0;
        countOpen = 0;
        for (i = idx + 1; i < list.size(); i++) {
            if (Calculate.isOpeningParenthesis(list.get(i)))
                countOpen++;
            if (Calculate.isClosingParenthesis(list.get(i)))
                countClose++;
            if (countOpen == countClose - 1) {
                rightSc = i;
                break;
            }
        }
        if (rightSc == -1)
            rightSc = list.size() - 1;
        ExpressionStatus status = new ExpressionStatus();
        status.setBeginIndex(leftSc);
        status.setEndIndex(rightSc);
        status.setLength(rightSc - leftSc + 1);
        status.setActive(!exp_isValidExpression(0, list.size(), list));
        return status;
    }

    /**
     * Возвращает статус выражения вокруг индекса, исключая скобки
     *
     * @param idx  - индекс
     * @param list - лист
     * @return - структура статус
     */
    public static ExpressionStatus exp_getExpressionStatusShortLengthAroundIndex(int idx, ArrayList<String> list) {
        int countOpen = 0;
        int countClose = 0;
        int i;
        int leftSc = -1;
        int rightSc = -1;
        if (idx == 0) {
            ExpressionStatus e = new ExpressionStatus();
            e.setBeginIndex(0);
            e.setEndIndex(list.size() - 1);
            e.setLength(list.size());
            return e;
        }
        if (Calculate.isClosingParenthesis(list.get(idx))) {
            countClose += 1;
        }
        if (Calculate.isOpeningParenthesis(list.get(idx - 1))) {
            leftSc = idx;
            countOpen += 2;
        } else for (i = idx - 1; i >= 0; i--) {
            if (Calculate.isClosingParenthesis(list.get(i)))
                countClose++;
            if (Calculate.isOpeningParenthesis(list.get(i)))
                countOpen++;
            if (countOpen - 1 == countClose) {
                leftSc = i + 1;
                break;
            }
        }
        if (leftSc == -1)
            leftSc = 0;
        countOpen = 0;
        countClose = 0;
        for (i = leftSc; i < list.size(); i++) {
            if (Calculate.isOpeningParenthesis(list.get(i)))
                countOpen++;
            if (Calculate.isClosingParenthesis(list.get(i)))
                countClose++;
            if (i >= idx)
                if ((countOpen == countClose - 1)) {
                    rightSc = i - 1;
                    break;
                }
        }
        if (rightSc == -1)
            rightSc = list.size() - 1;
        ExpressionStatus status = new ExpressionStatus();
        status.setBeginIndex(leftSc);
        status.setEndIndex(rightSc);
        status.setLength(rightSc - leftSc + 1);
        status.setActive(!exp_isValidExpression(0, list.size(), list));
        return status;
    }

    /**
     * Возвращает , можно ли вставить закрывающуюся скобку в конец листа ( просто подсчет количества скобок)
     *
     * @return true/false
     */
    private boolean exp_isAvailableToPutClosingParenthesis() {
        int i, countOpen = 0, countClose = 0;
        ArrayList<String> bufList = getArrayListSymbs().getCurrentList();
        for (String buf : bufList) {
            if (buf.equals("(")) countOpen++;
            if (buf.equals(")")) countClose++;
        }
        return countOpen - countClose >= 1;
    }



    /*
     * БЛОК ДОБАВЛЕНИЕ ЭЛЕМЕНТОВ В КОЛЛЕКЦИЮ,ДОБАВЛЕНИЕ ОПЕРАЦИЙ, ЧИСЕЛ СТЕПЕНЕЙ И ВСЕГО ТАКОГО
     */

    /**
     *
     */
    public static class textCursorStruct {
        private int indexInArrayList;

        public int getIndexInArrayList() {
            return indexInArrayList;
        }

        public void setIndexInArrayList(int indexInArrayList) {
            this.indexInArrayList = indexInArrayList;
        }

        public int getCursorPositionFromEnd() {
            return cursorPositionFromEnd;
        }

        public void setCursorPositionFromEnd(int cursorPositionFromEnd) {
            this.cursorPositionFromEnd = cursorPositionFromEnd;
        }

        textCursorStruct(int i, int pos) {
            this.indexInArrayList = i;
            this.cursorPositionFromEnd = pos;
        }

        private int cursorPositionFromEnd;
    }

    private int getFullLengthElement(String str) {
        int spacer = ((Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL) || (Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_OCTAL)) ? 3 : 4;
        int lengthElementWithSpacers;
        if (Information.getTypeOFAction(str) == typeOfAction.ACTION_NUMERIC) {
            if (str.length() <= spacer)
                lengthElementWithSpacers = str.length();
            else {
                String str2 = !str.contains(".") ? str : str.substring(0, str.indexOf('.'));
                lengthElementWithSpacers = str2.length() / spacer + str2.length() + (str.indexOf('.') == -1 ? 0 : str.substring(str.indexOf('.')).length());
                lengthElementWithSpacers = (str2.length() % spacer == 0) ? --lengthElementWithSpacers : lengthElementWithSpacers;
            }
        } else if (Calculate.isCommand(str)) {
            lengthElementWithSpacers = 3;
        } else if (Calculate.isDegree(str)) {
            lengthElementWithSpacers = 1;
        } else if (Calculate.isConstExp(str) || Calculate.isConstPi(str)) {
            str = "e"; // пох что на длиной в одын
            lengthElementWithSpacers = 1;
        } else {
            lengthElementWithSpacers = Information.getStringByTypeOfAction(Information.getTypeOFAction(str)).length();
        }
        return lengthElementWithSpacers;
    }

    public textCursorStruct getIndexofListByTextCursorPosition(int pos, String text1) {

        int spacer = ((Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL) || (Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_OCTAL)) ? 3 : 4;
        int sumLength = 0;
        if (pos > 0) {
            for (int i = 0; i < getArrayListSymbs().getCurrentList().size(); i++) {
                String str = getArrayListSymbs().getCurrentList().get(i);
                int lengthElementWithSpacers;
                lengthElementWithSpacers = getFullLengthElement(str);
                if (Calculate.isConstPi(str) || Calculate.isConstExp(str)) {
                    str = "d";
                }

                sumLength += lengthElementWithSpacers;
                String str2 = !str.contains(".") ? str : str.substring(0, str.indexOf('.'));
                int countSpacers = (str2.length() <= spacer) ? 0 : str2.length() / spacer;
                countSpacers = (str2.length() % spacer == 0) ? --countSpacers : countSpacers;

                int lengthFloatPart = !str.contains(".") ? 0 : (str.substring(str.indexOf('.')).length());
                if (sumLength >= pos) {
                    int countSpacersAfterPos = sumLength - pos - lengthFloatPart;
                    int b = 0;
                    int c = 0;
                    while (b <= countSpacersAfterPos) {
                        b += (spacer + 1);
                        if (b <= countSpacersAfterPos)
                            c++;
                    }
                    countSpacersAfterPos = c;

                    int bufPos = 0;
                    if (str.indexOf('.') != -1) {
                        bufPos = sumLength - pos - countSpacersAfterPos;
                    } else {
                        bufPos = sumLength - pos - countSpacersAfterPos; //count spacerc righter pos;
                    }
                    return new textCursorStruct(i, bufPos);
                }
            }
        } else if (pos == -1) {
            return new textCursorStruct(getArrayListSymbs().getCurrentList().size() - 1, 0);
        } else if (!getArrayListSymbs().getCurrentList().isEmpty()) {
            String element = getArrayListSymbs().getCurrentList().get(0);
            if (isCommand(element)) {
                return new textCursorStruct(0, getFullLengthElement(element));
            } else
                return new textCursorStruct(0, getArrayListSymbs().getCurrentList().get(0).length());
        } else return new textCursorStruct(-1, 0);
        return new textCursorStruct(-1, 0);
    }

    private ListWithCusorPos lwcp;

    /**
     * Добавление элемента. метод верхнего уровня, после которого идет распределение на добавление числа, константы, операции, отрабатывает по листенеру кнопки с клавиатур
     *
     * @param toa тип вставляемого действия
     */
    public textCursorStruct elements_addElement(typeOfAction toa, int position, String text) {
        int pos = getArrayListSymbs().getCurrentList().size(); // // TODO: 22.03.2018 Костыль позиции,потом допилить
        textCursorStruct tcs;

        if ((getArrayListSymbs().getCurrentList().indexOf(Information.getStringForViewNaN()) != -1))
            getArrayListSymbs().getCurrentList().remove(getArrayListSymbs().getCurrentList().indexOf(Information.getStringForViewNaN()));
        if ((getArrayListSymbs().getCurrentList().indexOf(Information.getStringForViewInfinity()) != -1))
            getArrayListSymbs().getCurrentList().remove(getArrayListSymbs().getCurrentList().indexOf(Information.getStringForViewInfinity()));

        tcs = getIndexofListByTextCursorPosition(position, text);
        lwcp = new ListWithCusorPos(getArrayListSymbs().getCurrentList(), tcs);
        lwcp = AddingElement.elements_addOperationToList(toa, lwcp);
        //AddingElement.tryToFuckExpressiontoGood(testList);
        getArrayListSymbs().setCurrentList(lwcp.getList());
        parseView_parseAndViewArrayList();
        getArrayListSymbs().convertToDecimalList();
        if (lastAction != typeOfAction.ACTION_ENDED_NUMERIC)
            parseView_calculateAndViewPreSummary();
        if (!AddingElement.isCountParenthesisValid(getArrayListSymbs().getCurrentList()))
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_PARENTHESIS, "( )", null), null);
        else
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_PARENTHESIS, "", null), null);
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_UPDATE, null, null), null);
        Log.d("NEWPARSER", parseView_staticParseListToSpanString(lwcp.getList(), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, null).toString());
        if (toa == typeOfAction.ACTION_SUMMARY) {
            return elements_addSummaryAction();
        }
        if (toa == typeOfAction.ACTION_CLEAR) {
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_CLEAR, "", null), null);
        }
        return lwcp.getTcs();
    }

    public int tcsToCursorPosition(textCursorStruct tcs, String text) {
        Log.d("TCSTOCURSOR", "ARRAY INDEX = " + String.valueOf(tcs.getIndexInArrayList()) + " POSITION FROM END = " + String.valueOf(tcs.getCursorPositionFromEnd()));
        textCursorStruct tcsOut = new textCursorStruct(-1, -1);

        int sumLength = 0;
        int spacer = ((Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL) || (Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_OCTAL)) ? 3 : 4;
        if (text.isEmpty())
            return 0;
        if (tcs.getIndexInArrayList() >= getArrayListSymbs().getCurrentList().size()) {
            //Странная ситуация, выход за границы.
            tcs.setIndexInArrayList(getArrayListSymbs().getCurrentList().size() - 1);
        }
        if ((tcs.getIndexInArrayList() == 0) && (getArrayListSymbs().getCurrentList().size() == 0)) {
            return text.length() - tcs.getCursorPositionFromEnd() - 1;
        }
        for (int i = 0; i <= tcs.getIndexInArrayList(); i++) {
            String str = getArrayListSymbs().getCurrentList().get(i);
            int lengthElementWithSpacers;

            if (Information.getTypeOFAction(str) == typeOfAction.ACTION_NUMERIC) {
                if (str.length() <= spacer)
                    lengthElementWithSpacers = str.length();
                else {
                    String str2 = !str.contains(".") ? str : str.substring(0, str.indexOf('.'));
                    lengthElementWithSpacers = str2.length() / spacer + str2.length() + (str.indexOf('.') == -1 ? 0 : str.substring(str.indexOf('.')).length());
                    lengthElementWithSpacers = (str2.length() % spacer == 0) ? --lengthElementWithSpacers : lengthElementWithSpacers;
                }
            } else if (Calculate.isCommand(str)) {
                lengthElementWithSpacers = 3;
            } else if (Calculate.isDegree(str)) {
                lengthElementWithSpacers = 1;
            } else if (Calculate.isConstExp(str) || Calculate.isConstPi(str)) {
                str = "P"; // пох что на длиной в одын
                lengthElementWithSpacers = 1;
            } else {
                lengthElementWithSpacers = Information.getStringByTypeOfAction(Information.getTypeOFAction(str)).length();
                lengthElementWithSpacers = lengthElementWithSpacers == 0 ? str.length() : lengthElementWithSpacers;
            }


            sumLength += lengthElementWithSpacers;


            String str2 = !str.contains(".") ? str : str.substring(0, str.indexOf('.'));
            int countSpacers = (str2.length() <= spacer) ? 0 : str2.length() / spacer;
            countSpacers = (str2.length() % spacer == 0) ? --countSpacers : countSpacers;

            int lengthFloatPart = !str.contains(".") ? 0 : (str.substring(str.indexOf('.')).length());


            if (i == tcs.getIndexInArrayList()) {
                if ((Information.getTypeOFAction(getArrayListSymbs().getCurrentList().get(tcs.getIndexInArrayList())) != typeOfAction.ACTION_NUMERIC)
                        && (Information.getTypeOFAction(getArrayListSymbs().getCurrentList().get(tcs.getIndexInArrayList())) != typeOfAction.ACTION_CONST_PI)) {
                    return (sumLength - tcs.getCursorPositionFromEnd());
                }
                if (tcs.getCursorPositionFromEnd() == 0) {
                    return (sumLength);
                }
                if (tcs.getCursorPositionFromEnd() <= lengthFloatPart) {
                    return (sumLength - tcs.getCursorPositionFromEnd());
                } else {
                    //предположим тута что  ы сюда зашли без точки уже
                    int ostatokSperedi = str2.length() % spacer;
                    int countSpacersOBSH = countSpacers == -1 ? 0 : countSpacers;

                    int b = ostatokSperedi;
                    int lengthMainPart = str.length() - lengthFloatPart;

                    int indexInMainPart = lengthMainPart - (tcs.getCursorPositionFromEnd() - lengthFloatPart) - 1;

                    int iter = indexInMainPart;
                    int countSpacersAfter = 0;
                    while (iter <= lengthMainPart) {
                        iter += (spacer + 1);
                        if (iter <= lengthMainPart)
                            countSpacersAfter++;
                    }

                    int countSpacersBefore = countSpacersOBSH - countSpacersAfter;
                    int pos = str2.substring(0, indexInMainPart + 1).length() + countSpacersBefore;
                    return (sumLength - lengthElementWithSpacers) + pos;

                }
            }
        }


        return 0;
    }


    /**
     * Добавление действия "очистка списка"
     */
    private void elements_addClearAction() {
        ArrayList<String> bufList = getArrayListSymbs().getCurrentList();
        //fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_UPDATE,null,null));

        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_VIEW, "", null), null);
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_PARENTHESIS, "", null), null);
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_RESULT, "", null), null);
        exStatus.setActive(false);

        lastAction = typeOfAction.UNKNOWN_ACTION;
        //parseView_setButtonDPressed(false);
        Information.setIsNeedButtonRadian(false);
        fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_UPDATE, null, null), null);
        if (!bufList.isEmpty())
            bufList.clear();
    }


    /**
     * Добавление действия по кнопке равенства
     */
    private textCursorStruct elements_addSummaryAction() {
        ArrayList<String> bufList = getArrayListSymbs().getCurrentList();
        String lastElement = !bufList.isEmpty() ? bufList.get(bufList.size() - 1) : "";
        if (!bufList.isEmpty()) {
            if (Calculate.isCommand(bufList.get(bufList.size() - 1)) || (Calculate.isSQRTelement(lastElement))) {
                bufList.remove(bufList.size() - 1);
                getArrayListSymbs().convertToDecimalList();
            }
            parseView_addSummaryToResult(parse_calculatePreSum());
            hist_writeToHistory();
            //parseView_setButtonDPressed(false);
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_VIEW, "", null), null);
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_RESULT, "", null), null);
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_PARENTHESIS, "", null), null);

            if (!bufList.isEmpty())
                bufList.clear();
            if ((!lastValue.isEmpty()) && (lastValue.charAt(0) == '-')) {
                bufList.add("-");
                lastValue = lastValue.substring(1);
            }
            bufList.add(lastValue);

            parseView_addSummaryToView(bufList);
            getArrayListSymbs().convertToDecimalList();
            // lastValue = "0";
            //parseView_setTextViewBigSize();
            //TODO lastAction = typeOfAction.ACTION_ENDED_NUMERIC;
            return new textCursorStruct(getArrayListSymbs().getCurrentList().size(), 0);
        }
        parseView_parseAndViewArrayList();
        return new textCursorStruct(0, 0);
    }

    public void addAllSmartParenthesis() {
        while (!AddingElement.isCountParenthesisValid(getArrayListSymbs().getCurrentList())) {
            getArrayListSymbs().setCurrentList(AddingElement.addSmartOneClosingParenthesis(getArrayListSymbs().getCurrentList()));
        }
        parseView_parseAndViewArrayList();
        parseView_calculateAndViewPreSummary();
    }

    public void addSmartParethesis() {
        getArrayListSymbs().setCurrentList(AddingElement.addSmartOneClosingParenthesis(getArrayListSymbs().getCurrentList()));
        parseView_parseAndViewArrayList();
        parseView_calculateAndViewPreSummary();
        if (!AddingElement.isCountParenthesisValid(getArrayListSymbs().getCurrentList()))
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_PARENTHESIS, "( )", null), null);
        else
            fragmentTextListener.onTextViewChange(new TextFragmentStruct(TextFragmentStruct.textField.TEXT_PARENTHESIS, "", null), null);
    }


}

