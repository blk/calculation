package com.blk_.calculate.parsing;

import java.util.ArrayList;

public class ListWithCusorPos {
    private ArrayList<String> list;
    private Parse.textCursorStruct tcs;
//        ListWithCusorPos(ArrayList<String> lst){
//            for (String el :lst) {
//                int spacer = ((Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL) || (Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_OCTAL)) ? 3 : 4;
//                if (Calculate.isCommand(el))
//                    deltaPos += 3;
//                else if (isNumeric(el))
//                    deltaPos = (el.length() % spacer == 0) ? (el.length() / spacer + el.length() - 1) : (el.length() / spacer + el.length());
//                else
//                    deltaPos += Information.getStringByTypeOfAction(Information.getTypeOFAction(el)).length();
//            }

    //                deltaPos = Calculate.isCommand(el)?
//                        deltaPos+3:
//                        isNumeric(el)?
//                                (el.length() % spacer == 0)?(deltaPos+=(el.length() / spacer + el.length()-1)):(deltaPos+=el.length() / spacer + el.length())
//                                :(deltaPos+=el.length());
    //       }
    public ListWithCusorPos(ArrayList<String> lst, Parse.textCursorStruct deltaPos) {
        this.tcs = deltaPos;
        this.list = lst;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public Parse.textCursorStruct getTcs() {
        return tcs;
    }
}