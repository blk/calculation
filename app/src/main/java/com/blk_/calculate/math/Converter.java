package com.blk_.calculate.math;

import android.util.Log;

import com.blk_.calculate.Information;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.logger.SingletonLogger;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by blk on 11.03.2018.
 * Класс - конвертер из одной системы числения в другую (2, 8, 10, 16)
 */

public class Converter {
    private static final int accuracy = 10; // точность перевода дробной части (знаков после запятой)
    private static final String MAX_BIN = "0111111111111111" +
            "1111111111111111" +
            "1111111111111111" +
            "1111111111111111";
    private static final String MAX_HEX = "7FFFFFFFFFFFFFFF";
    private static final String MAX_OCT = "777777777777777777777";
    private static final String MAX_DEC = "9223372036854775807";


    /**
     * Основная функция перевода.
     *
     * @param element Переводимое число
     * @param tosFrom Система счисления из которой осуществляется перевод
     * @param tosTo   Система счисления в которую осуществляется перевод
     * @return переведенное число в строковом виде
     */
    public static String convertAdapter(String element, typeOfNumberSystem tosFrom, typeOfNumberSystem tosTo) {
        try {
            if (element.equals(Information.getStringForViewNaN()) || element.equals(Information.getStringForViewInfinity()) || element.isEmpty())
                return element;
            if (tosFrom == tosTo)
                return element;
            // int z = countLengthZAfterZap(element);
            switch (tosFrom) {
                case NUMBER_SYSTEM_BINARY: {
                    return convertFromBin(element, tosTo);
                }
                case NUMBER_SYSTEM_OCTAL: {
                    return convertFromOct(element, tosTo);
                }
                case NUMBER_SYSTEM_DECIMAL: {
                    return convertFromDec(element, tosTo);
                }
                case NUMBER_SYSTEM_HEXADECIMAL: {
                    return convertFromHex(element, tosTo);
                }
            }
            return "";
        } catch (Throwable ex) {
            //MainActivity.onException(ex);
            SingletonLogger.getInstance().onException(ex);
        }
        return "";
    }

    /**
     * Под-адаптер для перевода. В нем уже явно известно что осуществляется перевод из двоичной системы счисления
     *
     * @param element переводимое число
     * @param tosTo   система счисления, в которую нужно перевести число
     * @return переведенное число в строковом виде
     */
    private static String convertFromBin(String element, typeOfNumberSystem tosTo) {
        switch (tosTo) {
            case NUMBER_SYSTEM_BINARY:
                if (element.contains(MAX_BIN)) return MAX_BIN;
                else return element;
            case NUMBER_SYSTEM_OCTAL:
                if (convertBinOct(element).contains(MAX_OCT)) return MAX_OCT;
                else return convertBinOct(element);
            case NUMBER_SYSTEM_DECIMAL:
                if (convertBinDec(element).contains(MAX_BIN)) return MAX_BIN;
                else return convertBinDec(element);
            case NUMBER_SYSTEM_HEXADECIMAL:
                if ((convertBinHex(element).toUpperCase()).contains(MAX_HEX)) return MAX_HEX;
                else return convertBinHex(element).toUpperCase();
        }
        return element;
    }

    /**
     * Под-адаптер для перевода. В нем уже явно известно что осуществляется перевод из восьмеричной системы счисления
     *
     * @param element переводимое число
     * @param tosTo   система счисления, в которую нужно перевести число
     * @return переведенное число в строковом виде
     */
    private static String convertFromOct(String element, typeOfNumberSystem tosTo) {
        switch (tosTo) {
            case NUMBER_SYSTEM_BINARY:
                if (convertOctBin(element).contains(MAX_BIN)) return MAX_BIN;
                else return convertOctBin(element);
            case NUMBER_SYSTEM_OCTAL:
                if (element.contains(MAX_OCT)) return MAX_OCT;
                else return element;
            case NUMBER_SYSTEM_DECIMAL:
                if (convertOctDec(element).contains(MAX_DEC)) return MAX_DEC;
                else return convertOctDec(element);
            case NUMBER_SYSTEM_HEXADECIMAL:
                if ((convertOctHex(element)).toUpperCase().contains(MAX_HEX)) return MAX_HEX;
                else return (convertOctHex(element)).toUpperCase();
        }
        return element;
    }

    /**
     * Под-адаптер для перевода. В нем уже явно известно что осуществляется перевод из десятичной системы счисления
     *
     * @param element переводимое число
     * @param tosTo   система счисления, в которую нужно перевести число
     * @return переведенное число в строковом виде
     */
    private static String convertFromDec(String element, typeOfNumberSystem tosTo) {
        switch (tosTo) {
            case NUMBER_SYSTEM_BINARY:
                if (convertDecBin(element).contains(MAX_BIN)) return MAX_BIN;
                else return convertDecBin(element);
            case NUMBER_SYSTEM_OCTAL:
                if (convertDecOct(element).contains(MAX_OCT)) return MAX_OCT;
                else return convertDecOct(element);
            case NUMBER_SYSTEM_DECIMAL:
                if (element.contains(MAX_DEC)) return MAX_DEC;
                else return element;
            case NUMBER_SYSTEM_HEXADECIMAL:
                if ((convertDecHex(element).toUpperCase()).contains(MAX_HEX)) return MAX_HEX;
                else return convertDecHex(element).toUpperCase();
        }
        return element;
    }

    /**
     * Под-адаптер для перевода. В нем уже явно известно что осуществляется перевод из шестнадццатеричной системы счисления
     *
     * @param element переводимое число
     * @param tosTo   система счисления, в которую нужно перевести число
     * @return переведенное число в строковом виде
     */
    private static String convertFromHex(String element, typeOfNumberSystem tosTo) {
        switch (tosTo) {
            case NUMBER_SYSTEM_BINARY:
                if (convertHexBin(element).contains(MAX_BIN)) return MAX_BIN;
                else return convertHexBin(element);
            case NUMBER_SYSTEM_OCTAL:
                if (convertHexOct(element).contains(MAX_OCT)) return MAX_OCT;
                else return convertHexOct(element);
            case NUMBER_SYSTEM_DECIMAL:
                if (convertHexDec(element).contains(MAX_DEC)) return MAX_DEC;
                else return convertHexDec(element);
            case NUMBER_SYSTEM_HEXADECIMAL:
                if ((convertBinHex(element).toUpperCase()).contains(MAX_HEX)) return MAX_HEX;
                else return convertBinHex(element).toUpperCase();
        }
        return element;
    }

    /**
     * Перевод из десятичной в двоичную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertDecBin(String val) {
        if (!val.isEmpty()) {
            boolean isMinus = Double.valueOf(val) < 0;
            double decimalValue = Double.valueOf(val);
            if (Math.abs(decimalValue) % 1 == 0) { // то есть число не дробное
                try {
                    return Long.toBinaryString(((long) decimalValue));
                } catch (NumberFormatException ex) {
                    return MAX_BIN;
                }
            } else {
                decimalValue = Math.abs(decimalValue);
                String leftPart;
                String rightPart;
                StringBuilder returnedString;
                try {
                    val = val.indexOf('-') == -1 ? val : val.substring(1, val.length());
                    leftPart = Long.toBinaryString(Long.parseLong(val.substring(0, val.indexOf('.'))));
                } catch (NumberFormatException ex) {
                    leftPart = MAX_BIN;
                }
                double rightDouble = decimalValue % 1;
                rightPart = "";
                for (int i = 0; i < accuracy; i++) {
                    rightDouble *= 2;
                    if (rightDouble >= 1) {
                        rightPart = rightPart.concat("1");
                        rightDouble -= 1;
                        if (rightDouble == 0)
                            break;
                    } else {
                        rightPart = rightPart.concat("0");
                    }
                }
                returnedString = new StringBuilder(leftPart + "." + rightPart);
                if (!isMinus)
                    return returnedString.toString();
                else { //если число таки отрицательное да еще и, сука, дробное
                    for (int i = 0; returnedString.toString().indexOf('.') <= 63; i++) {
                        returnedString.insert(0, "0");
                    }
                    String bufString = inverseBinaryString(returnedString.toString());

                    String left = bufString.substring(0, bufString.indexOf('.'));
                    String right = bufString.substring(bufString.indexOf('.') + 1, bufString.length());

                    long bf = Long.parseLong(convertBinDec(right)) + 1;
                    StringBuilder buferRight = new StringBuilder(convertDecBin(String.valueOf(bf)));
                    if (buferRight.length() == right.length()) {
                        returnedString = new StringBuilder(left + "." + buferRight);
                        return returnedString.toString();
                    } else if (buferRight.length() < right.length()) { //нужно добавить нулей вперед
                        while (buferRight.length() < right.length())
                            buferRight.insert(0, "0");
                        returnedString = new StringBuilder(left + "." + buferRight);
                        return returnedString.toString();
                    } else {
                        //чисто логически, сюда мы не попадем никогда...
                        double bfl = Long.parseLong(convertBinDec(left)) + 1;
                        String buferLeft = convertDecBin(String.valueOf(bfl));
                        returnedString = new StringBuilder(buferLeft + "." + buferRight.substring(1, buferRight.length()));
                        Log.d("CONVERTER", "EBABOBAAAAAAAAAAAAAAA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        return returnedString.toString();
                    }
                }
            }
        }
        return "";
    }

    /**
     * Перевод из десятичной в восьмеричную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertDecOct(String val) {
        double value;
        value = Double.valueOf(val);
        if (value % 1 == 0)
            return Long.toOctalString((long) value);
        else {
            double vb = value % 1;
            String outputString = "";
            outputString = outputString.concat(String.valueOf(Long.toOctalString((long) (value - vb))));
            //String buf = val.substring(val.indexOf('.')+1,val.length());
            String bufDr = "";
            for (int i = 0; i < accuracy; i++) {
                vb *= 8;
                if (vb >= 1) {
                    bufDr = bufDr.concat(String.valueOf((int) (vb - (vb % 1))));
                    vb = vb % 1;
                    if (vb == 0)
                        break;
                } else {
                    bufDr = bufDr.concat("0");
                }
            }
            outputString = outputString.concat("." + bufDr);
            return outputString;
        }
    }

    /**
     * Перевод из десятичной в шестнадцатеричную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertDecHex(String val) {
        double value;
        value = Double.valueOf(val);
        if (value % 1 == 0)
            return Long.toHexString((long) value);
        else {
            double vb = Double.valueOf("0" + val.substring(val.indexOf('.'), val.length()));
            String outputString = "";
            outputString = outputString.concat(String.valueOf(Long.toHexString((long) (value - vb))));
            String bufDr = "";
            for (int i = 0; i < accuracy; i++) {
                vb *= 16;
                if (vb >= 1) {
                    bufDr = bufDr.concat(Integer.toHexString((int) (vb - (vb % 1))));
                    vb = vb % 1;
                    if (vb == 0)
                        break;
                } else {
                    bufDr = bufDr.concat("0");
                }
            }
            outputString = outputString.concat("." + bufDr);
            if (Double.valueOf(convertHexDec(outputString)) % 1 == 0)
                outputString = outputString.replace(".0", "");
            return outputString;
        }
    }

    /**
     * Перевод из двоичной в восьмеричную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertBinOct(String val) {
        return (convertDecOct(convertBinDec(val)));
    }

    /**
     * Перевод из двоичной в шестнадцатеричную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertBinHex(String val) {
        return (convertDecHex(convertBinDec(val)));
    }

    /**
     * Перевод из двоичной в десятичную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertBinDec(String val) {
        if (val.contains(".")) {
            if (!val.substring(val.indexOf('.'), val.length()).contains("1")) {
                //значит не дробное точно. этим условием мы откинули возможный вариант с .0
                try {
                    return String.valueOf(Long.parseLong(val.substring(0, val.indexOf('.')), 2));
                } catch (NumberFormatException ex) {
                    return MAX_DEC;
                }
            } else // тут дробное приходит
            {
                //проверяем на отрицательность двоичного дробного числа
                if (val.length() >= 64) {
                    if (val.charAt(0) == '1') {
                        //отрицательное дробное
                        String left = val.substring(1, val.indexOf('.'));
                        String right = val.substring(val.indexOf('.') + 1, val.length());
                        //вообще по логике вещей можно не плюсовать ничего к левой части выражения а только лишь к правой ибо
                        //алгоритм таков , что в любом случае в дробной части есть хотя бы один нолик да есть
                        long bf = Long.parseLong(convertBinDec(right)) - 1;
                        StringBuilder buferRight = new StringBuilder(convertDecBin(String.valueOf(bf)));
                        if (buferRight.length() < right.length()) { //нужно добавить нулей вперед
                            while (buferRight.length() < right.length())
                                buferRight.insert(0, "0");
                        }
                        buferRight = new StringBuilder(inverseBinaryString(buferRight.toString()));
                        left = inverseBinaryString(left);
                        double rightPart = 0;
                        for (int i = 1; i < buferRight.length() + 1; i++) {
                            int v;
                            if (buferRight.charAt(i - 1) == '0')
                                v = 0;
                            else v = 1;
                            rightPart += v * Math.pow(2, -i);

                        }
                        double leftD = Long.parseLong(left, 2);
                        return String.valueOf((-1) * (leftD + rightPart));
                    } else
                        return "";
                } else {
                    // положительное дробное
                    String left = val.substring(0, val.indexOf('.'));
                    String right = val.substring(val.indexOf('.') + 1, val.length());
                    double rightD = 0;
                    for (int i = 1; i < right.length() + 1; i++) {
                        int v;
                        if (right.charAt(i - 1) == '0')
                            v = 0;
                        else v = 1;
                        rightD += v * Math.pow(2, -i);

                    }
                    return String.valueOf(Long.parseLong(left, 2)) + "." + String.valueOf(rightD).replace("0.", "");

                }
            }
        } else {
            if (val.length() >= 64) {
                if (val.charAt(0) == '1') {
                    //отрицательное целое
                    String returnedString = inverseBinaryString(val);
                    long value;
                    try {
                        value = (Long.parseLong(returnedString, 2));
                    } catch (NumberFormatException ex) {
                        return MAX_DEC;
                    }
                    value += 1;
                    return String.valueOf((-1) * value);
                } else
                    return "";
            } else {
                try {
                    return String.valueOf(Long.parseLong(val, 2));
                } catch (NumberFormatException ex) {
                    return MAX_DEC;
                }
            }
        }
    }

    /**
     * Перевод из восьмеричной в десятичую системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertOctDec(String val) {
        int lastIndex;
        String valRight, valLeft;
        if (val.indexOf('.') != -1) {
            lastIndex = val.indexOf('.');
            valRight = val.substring(lastIndex + 1, val.length());
        } else {
            valRight = "";
            lastIndex = val.length();
        }
        valLeft = val.substring(0, lastIndex);
        long valueLeft = 0;
        double valueRight = 0;
        String outputString = "";
        int baseStep = valLeft.length() - 1;

        for (int i = 1; i < valLeft.length() + 1; i++, baseStep--) {
            String buf;
            if (valLeft.charAt(i - 1) == '-') {
                buf = Character.toString(valLeft.charAt(i - 1)) + Character.toString(valLeft.charAt(i));
                valueLeft += Long.valueOf(buf) * Math.pow(8, baseStep);
                i++;
            } else {
                valueLeft += Long.valueOf(Character.toString(valLeft.charAt(i - 1))) * Math.pow(8, baseStep);
            }

        }
        if (!valRight.isEmpty()) {
            for (int i = 1; i < valRight.length() + 1; i++)
                valueRight += Integer.valueOf(Character.toString(valRight.charAt(i - 1))) * Math.pow(8, -i);
        }
        outputString = outputString.concat(String.valueOf(valueLeft) + String.valueOf(valueRight).replace("0.", "."));
        if (Double.valueOf(outputString) % 1 == 0)
            outputString = outputString.replace(".0", "");
        return outputString;
    }

    /**
     * Перевод из восьмеричной в двоичную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertOctBin(String val) {
        return (convertDecBin(convertOctDec(val)));
    }

    /**
     * Перевод из восьмеричной в шестанадцатеричную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertOctHex(String val) {
        return (convertDecHex(convertOctDec(val)));
    }

    /**
     * Перевод из шестнадцатеричной в десятичую системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertHexDec(String val) {
        int lastIndex;
        String valRight, valLeft;
        if (val.indexOf('.') != -1) {
            lastIndex = val.indexOf('.');
            valRight = val.substring(lastIndex + 1, val.length());
        } else {
            valRight = "";
            lastIndex = val.length();
        }
        valLeft = val.substring(0, lastIndex);
        long valueLeft = 0;//"";
        //long bf = 0;
        double valueRight = 0;
        int baseStep = valLeft.length() - 1;
        for (int i = 1; i < valLeft.length() + 1; i++, baseStep--) {
            String buf;
            if (valLeft.charAt(i - 1) == '-') {
                buf = Character.toString(valLeft.charAt(i - 1)) + Character.toString(valLeft.charAt(i));
                valueLeft += Long.valueOf(hexDecValue(buf)) * Math.pow(16, baseStep);
                i++;
            } else {
                valueLeft += Long.valueOf(hexDecValue(Character.toString(valLeft.charAt(i - 1)))) * Math.pow(16, baseStep);
            }
        }
        if (!valRight.isEmpty()) {
            for (int i = 1; i < valRight.length() + 1; i++)
                valueRight += Integer.valueOf(hexDecValue(Character.toString(valRight.charAt(i - 1)))) * Math.pow(16, -i);
        }
        double newVal;
        newVal = new BigDecimal(valueLeft + valueRight).setScale(accuracy, RoundingMode.UP).doubleValue();

        return String.valueOf(newVal);
    }

    /**
     * Перевод из шестнадцатеричной в восьмеричную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertHexOct(String val) {
        return (convertDecOct(convertHexDec(val)));
    }

    /**
     * Перевод из шестнадцатеричной в двоичную системы счисления
     *
     * @param val переводимое число
     * @return переведенное число
     */
    private static String convertHexBin(String val) {
        return (convertDecBin(convertHexDec(val)));
    }

    /**
     * Функция возвращает десятичное значение соответствующее шестнадцатеричную
     *
     * @param v 16-ое значение
     * @return десятичное значение
     */
    private static String hexDecValue(String v) {
        v = v.toUpperCase();
        switch (v) {
            case "A":
                return "10";
            case "B":
                return "11";
            case "C":
                return "12";
            case "D":
                return "13";
            case "E":
                return "14";
            case "F":
                return "15";
            case "-A":
                return "-10";
            case "-B":
                return "-11";
            case "-C":
                return "-12";
            case "-D":
                return "-13";
            case "-E":
                return "-14";
            case "-F":
                return "-15";
            default:
                return v;
        }
    }

    /**
     * Инвертирование двоичной строки, для перевода в дополнительный код
     *
     * @param str входная строка
     * @return измененная строка
     */
    private static String inverseBinaryString(String str) {
        String bufString = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '0')
                bufString = bufString.concat("1");
            else if (str.charAt(i) == '1')
                bufString = bufString.concat("0");
            if (str.charAt(i) == '.')
                bufString = bufString.concat(".");
        }
        return bufString;
    }

}
