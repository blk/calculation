package com.blk_.calculate.math;


import android.util.Log;

import com.blk_.calculate.Information;
import com.blk_.calculate.enums.StateRadDegr;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.logger.SingletonLogger;
import com.blk_.calculate.parsing.AddingElement;
import com.blk_.calculate.parsing.Parse;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Created by blk_ on 22.03.2018.
 * Математический класс.
 * Вся математика и все подсчеты производятся здесь
 * Также в этом классе происходит идентификация элемента.
 * Класс полностью статик
 */

public class Calculate {


    private static String stirling(BigDecimalBlk n) {
        if (n.getValue().compareTo(BigDecimal.ZERO) <= 0)
            return Information.getStringForViewNaN();
        if (n.isNaN() || n.isInfinity())
            Log.d("ops", "DDD");
        try {
            // return  Math.sqrt(Math.PI * 2 * n) *Math.pow((n / Math.E),n)* Math.pow(Math.E,(1/((12*n)+(0.7509*2/3))));// 2/3 по формуле необязательно, кэф взят из вики как максимальная верхняя граница по формуле стирлинга
            return (((new BigDecimal(Math.sqrt(n.getValue().multiply(new BigDecimal(Math.PI * 2)).doubleValue())).
                    multiply(new BigDecimal(Math.pow((n.getValue().divide(new BigDecimal(Math.E), 20, RoundingMode.HALF_UP)).doubleValue(), n.getValue().doubleValue()))))
                    .multiply(new BigDecimal(Math.pow(Math.E, (Information.getAprocsimateCoefficient() / ((n.getValue().multiply(new BigDecimal(12)).doubleValue()))))))).round(new MathContext(15, RoundingMode.HALF_UP))).toString();//0.7509*2/3
        } catch (NumberFormatException ex) {
            SingletonLogger.getInstance().onException(ex);
            return Information.getStringForViewInfinity();
        }
    }


    /**
     * Основной паблик метод подсчета выражения
     *
     * @param blist коллекция с десятичным представлением чисел и операций
     * @return значение подсчета. либо NaN в случае ошибки
     */
    public static BigDecimalBlk calculate(ArrayList<String> blist) throws Throwable {

        BigDecimalBlk val;
        if (blist.isEmpty())
            val = BigDecimalBlk.valueOf(Double.NaN);
        else
            val = getNearestNumeric(blist);
        while (blist.size() > 1) {
            int firstIndx = blist.lastIndexOf("(");
            int lastIndx = -1;
            if (firstIndx != -1) {
                int b = blist.subList(firstIndx, blist.size()).indexOf(")");
                lastIndx = b == -1 ? -1 : firstIndx + b;
            }
            if ((firstIndx != -1) && (lastIndx != -1)) {
                ArrayList<String> bufList = new ArrayList<>(blist.subList(firstIndx + 1, lastIndx));
                val = calculateMath(bufList);
                int count = lastIndx - firstIndx + 1;
                for (int j = 0; j < count; j++)
                    blist.remove(firstIndx);
                blist.add(firstIndx, Parse.parseView_removeInsignificantZerosFromElement(val.getStringValue()));
            } else {
                val = calculateMath(blist);
                blist.clear();
                blist.add(Parse.parseView_removeInsignificantZerosFromElement(val.getStringValue()));
                return val;
            }
        }
        return val;

        //return new BigDecimalBlk(null, true, false);
    }

    /**
     * Внутренний метод подсчета математики, учитывается порядок действий(это важно, порядок менять не нужно)
     *
     * @param _list Коллекция с десятичным представлением чисел и операций
     * @return числовое значение подсчета
     */
    private static BigDecimalBlk calculateMath(ArrayList<String> _list) {
//поставил степень перед син. был перед перцентом

        checkAndCalculateFactorial(_list);
        checkAndCalculateDegree(_list);
        checkAndCalculateBigCommands(_list);
//            checkAndCalculateSin(_list);
//            checkAndCalculateCos(_list);
//            checkAndCalculateLg(_list);
//            checkAndCalculateLn(_list);
//            checkAndCalculateSQRT(_list);
        checkAndCalculatePercent(_list);
        return calculateSimpleOperations(_list);


    }

    /**
     * Функция поиска ближайшего элемента коллекции, являющегося числом, работает для отбрасывания скобок, корней и операций, для того , когда у нас выражение в процессе ввода, и там только одно число ,
     * пара скобок и т д, при подсчете будет хрень, и , так как число всего одно то оно выводится в итоге на экран, в ситуациях "99+" , "((32-" и т.д.
     *
     * @param lst Коллекция с десятичным представлением чисел и операций
     * @return значение ближайшего числа
     */
    private static BigDecimalBlk getNearestNumeric(ArrayList<String> lst) {
        for (String buf : lst)
            if ((Information.getTypeOFAction(buf) == typeOfAction.ACTION_NUMERIC)
                    || ((Information.getTypeOFAction(buf) == typeOfAction.ACTION_CONST_PI))
                    || ((Information.getTypeOFAction(buf) == typeOfAction.ACTION_CONST_E)))
                return getBigDecimalValue(buf);
        if (lst.size() > 0) {
            BigDecimalBlk ggg = new BigDecimalBlk(null, Information.getStringForViewNaN().equals(lst.get(0)), Information.getStringForViewInfinity().equals(lst.get(0)));
            if (!ggg.isInfinity() && !ggg.isNaN())
                return BigDecimalBlk.valueOf(Double.NaN);
            else
                return ggg;
        }
        return BigDecimalBlk.valueOf(Double.NaN);
    }


    /**
     * Функция возврата нужного значения
     * Используется для подсчетов синуса, косинуса и констант. В зависимости от режима ввода тригонометрических значений(градусы или радианы) возвращает соотвественно 180 или ПИ, или просто значение
     * <p>
     * <p>
     * /**
     * Функция возврата нужного значения
     * Используется для подсчетов синуса, косинуса и констант. В зависимости от режима ввода тригонометрических значений(градусы или радианы) возвращает соотвественно 180 или ПИ, или просто значение
     *
     * @param element элемент выражения тригонометрическое значение которого требуется узнать
     * @return само значение которое получаем в результате работы функции
     */
    public static BigDecimalBlk getBigDecimalValue(String element) {
        BigDecimalBlk returnedDecimal = new BigDecimalBlk(0);
        if (element.contains(Information.getStringForViewInfinity()))
            return new BigDecimalBlk(null, false, true);
        if (element.contains(Information.getStringForViewNaN()))
            return new BigDecimalBlk(null, true, false);
        switch (Information.getTypeOFAction(element)) {
            case ACTION_NUMERIC:
                return new BigDecimalBlk(new BigDecimal(element));
            case ACTION_CONST_E:
                return new BigDecimalBlk(Math.E);
            case ACTION_CONST_PI:
                if (Information.getStateRadDegr() == StateRadDegr.TYPE_INPUT_RADIAN)
                    return new BigDecimalBlk(Math.PI);
                else if ((Information.getStateRadDegr() == StateRadDegr.TYPE_INPUT_DEGREE))
                    return new BigDecimalBlk(180);
                break;
        }

        return returnedDecimal;
    }


    /**
     * Функция проверки и подсчета факториала
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static void checkAndCalculateFactorial(ArrayList<String> list) {
        if (!list.isEmpty()) {
            int i = 0;
            {
                while (i < list.size()) {
                    if (isFactorial(list.get(i)) && (!list.get(i - 1).isEmpty())) {
                        String element = list.get(i - 1);
                        BigDecimalBlk value = getBigDecimalValue(element);
                        //BigInteger f = null;

                        //list.set(i-1, String.valueOf(factorial(value).getValue()));

                        if (value.getStringValue().contains("."))
                            list.set(i - 1, stirling(value));
                        else
                            list.set(i - 1, factorial(value).getStringValue());

                        //String.valueOf(fact((long) value)));
                        list.remove(i);
                    }
                    i++;
                }
            }
        }
    }

    /**
     * Пробный вариант вычисления факториала без рекурсии , через BigInteget
     */
    private static BigDecimalBlk factorial(BigDecimalBlk n) {
        if (n.getValue().compareTo(BigDecimal.ZERO) < 0)
            return new BigDecimalBlk(BigDecimal.ZERO);
        BigDecimalBlk result = new BigDecimalBlk(BigDecimal.ONE);
        while (n.getValue().compareTo(BigDecimal.ZERO) > 0) {
            result = new BigDecimalBlk(result.getValue().multiply(n.getValue()));
            n = new BigDecimalBlk(n.getValue().subtract(BigDecimal.ONE));
        }
        //result.getValue().compareTo(BigDecimal.ZERO);
        return result;
    }

    /**
     * Рекурсивная функция математического подсчета факториала
     *
     * @param num число умножаемое на значение возвращаемоей этой же функцией
     * @return значение
     */
    private static long fact(long num) {
        Log.i("FACTORIAL", "NUMBER = " + String.valueOf(num));
        return (num <= 0) ? 1 : num * fact(num - 1);
    }

    /**
     * Функция проверки и подсчета синуса
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static void checkAndCalculateSin(ArrayList<String> list) {
        if (!list.isEmpty()) {
            int i = 0;
            while (i < list.size()) {
                if (i + 1 != list.size()) {
                    if (isSin(list.get(i)) && (!list.get(i + 1).isEmpty())) {
                        String element = list.get(i + 1);
                        BigDecimalBlk value = getBigDecimalValue(element);
                        if (Information.getStateRadDegr() == StateRadDegr.TYPE_INPUT_DEGREE)
                            value = BigDecimalBlk.valueOf(Math.toRadians(value.getDoubleValue()));
                        value = BigDecimalBlk.valueOf(Math.sin(value.getDoubleValue()));
                        list.set(i, String.valueOf(value.getValue()));
                        list.remove(i + 1);
                    }
                }
                i++;
            }
        }
    }

    /**
     * Функция проверки и подсчета косинуса
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static void checkAndCalculateCos(ArrayList<String> list) {
        if (!list.isEmpty()) {
            int i = 0;
            while (i < list.size()) {
                if (i + 1 != list.size()) {
                    if (isCos(list.get(i)) && (!list.get(i + 1).isEmpty())) {
                        String element = list.get(i + 1);
                        BigDecimalBlk value = getBigDecimalValue(element);
                        if ((Information.getStateRadDegr() == StateRadDegr.TYPE_INPUT_DEGREE))
                            value = BigDecimalBlk.valueOf(Math.toRadians(value.getDoubleValue()));
                        value = BigDecimalBlk.valueOf(Math.cos(value.getDoubleValue()));
                        list.set(i, String.valueOf(value.getValue()));
                        list.remove(i + 1);
                    }
                }
                i++;
            }
        }
    }

    /**
     * Функция проверки и подсчета десятичного логарифма
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static void checkAndCalculateLg(ArrayList<String> list) {
        if (!list.isEmpty()) {
            int i = 0;
            while (i < list.size()) {
                if (i + 1 != list.size()) {
                    if (isLg(list.get(i)) && (!list.get(i + 1).isEmpty())) {
                        String element = list.get(i + 1);
                        BigDecimalBlk value = getBigDecimalValue(element);
                        value = BigDecimalBlk.valueOf(Math.log10(value.getDoubleValue()));
                        list.set(i, String.valueOf(value.getValue()));
                        list.remove(i + 1);
                    }
                }
                i++;
            }
        }
    }


    /**
     * Функция проверки и подсчета натурального логарифма
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static void checkAndCalculateLn(ArrayList<String> list) {
        if (!list.isEmpty()) {
            int i = 0;
            while (i < list.size()) {
                if (i + 1 != list.size()) {
                    if (isLn(list.get(i)) && (!list.get(i + 1).isEmpty())) {
                        String element = list.get(i + 1);
                        BigDecimalBlk value = getBigDecimalValue(element);
                        value = BigDecimalBlk.valueOf(Math.log(value.getDoubleValue()));
                        list.set(i, String.valueOf(value.getValue()));
                        list.remove(i + 1);
                    }
                }
                i++;
            }
        }
    }

    /**
     * Функция проверки и подсчета квадратного корня
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     * тут нет скобок если что , все скобки отброшены уровнем выше
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static void checkAndCalculateSQRT(ArrayList<String> list) {
        if (!list.isEmpty()) {
            while (list.lastIndexOf(Information.getStringByTypeOfAction(typeOfAction.ACTION_SQRT)) != -1) {
                int indx = list.lastIndexOf(Information.getStringByTypeOfAction(typeOfAction.ACTION_SQRT));
                if (indx != -1) {
                    if (indx < list.size() - 1) {
                        String element = list.get(indx + 1);
                        BigDecimalBlk value = getBigDecimalValue(element);
                        value = BigDecimalBlk.valueOf(Math.sqrt(value.getDoubleValue()));
                        list.set(indx, String.valueOf(value.getValue()));
                        list.remove(indx + 1);
                    }
                }
            }
        }
    }

    private static void checkAndCalculateBigCommands(ArrayList<String> list) {
        if (!list.isEmpty()) {
            int li;
            while ((li = AddingElement.getLastIndexOfBigCommand(list)) != -1) {
                typeOfAction toa = Information.getTypeOFAction(list.get(li));
                if (li < list.size() - 1) {
                    String element = list.get(li + 1);
                    BigDecimalBlk value = getBigDecimalValue(element);
                    if (!(value.isInfinity() || value.isNaN())) {
                        double buf = Double.NaN;
                        switch (toa) {
                            case ACTION_SINX: {
                                if (Information.getStateRadDegr() == StateRadDegr.TYPE_INPUT_DEGREE)
                                    buf = Math.sin(Math.toRadians(value.getDoubleValue()));
                                else
                                    buf = Math.sin(value.getDoubleValue());
                                break;
                            }
                            case ACTION_COSX: {
                                if (Information.getStateRadDegr() == StateRadDegr.TYPE_INPUT_DEGREE)
                                    buf = Math.cos(Math.toRadians(value.getDoubleValue()));
                                else
                                    buf = Math.cos(value.getDoubleValue());
                                break;
                            }
                            case ACTION_LG: {
                                buf = Math.log10(value.getDoubleValue());
                                break;

                            }
                            case ACTION_LN: {
                                buf = Math.log(value.getDoubleValue());
                                break;
                            }
                            case ACTION_SQRT: {
                                buf = Math.sqrt(value.getDoubleValue());
                                break;
                            }
                        }
                        value = new BigDecimalBlk(buf);
                        if ((!value.isInfinity() && !value.isNaN()))
                            value = new BigDecimalBlk(value.getValue().setScale(Information.getAccuracy(), RoundingMode.HALF_UP));

                    }
                    list.set(li, value.getStringValue());
                    list.remove(li + 1);
                } else {
                    list.remove(li);
                }
            }
        }
    }

    /**
     * Функция проверки и подсчета возведения в степень
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static void checkAndCalculateDegree(ArrayList<String> list) {
        if (!list.isEmpty()) {
            int i = 0;
            while (i < list.size()) {
                if (list.get(i).equals("^")) {
                    String symb = isSQRTelement(list.get(i)) ? "\u221A" : "";
                    BigDecimalBlk valNum, finalValue;
                    BigDecimalBlk valDegr;
                    if ((i + 1) < list.size()) {
                        valNum = getBigDecimalValue(list.get(i - 1));
                        valDegr = getBigDecimalValue(list.get(i + 1));
                        finalValue = BigDecimalBlk.valueOf(Math.pow(valNum.getDoubleValue(), valDegr.getDoubleValue())); // зачем то перешел на дабл пока комментирую
                        //finalValue = new BigDecimalBlk(valNum.getValue().pow(valDegr.getValue().doubleValue()));
                        //finalValue = valNum.pow(valDegr);//
                        // Math.pow(valNum.doubleValue(), valDegr.doubleValue()));
                        list.remove(i);
                        list.remove(i);
                        list.set(i - 1, symb + finalValue.getStringValue());
                        i -= 1;
                    }
                }
                i++;
            }
        }
    }

    /**
     * Функция проверки и подсчета процента
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static void checkAndCalculatePercent(ArrayList<String> list) {
        if (!list.isEmpty()) {
            int i = 0;
            while (i < list.size()) {
                if (list.get(i).equals("%")) {
                    BigDecimalBlk newElement, percVal;
                    if (i == 1) {
                        // если выражение процента - первое в списке
                        newElement = new BigDecimalBlk(BigDecimal.ZERO);
                        list.set(i, newElement.getStringValue());
                        list.remove(0);
                        continue;
                    }

                    ArrayList<String> _blist = new ArrayList<>(list);
                    for (int j = i - 2; j < list.size(); j++) {
                        _blist.remove(_blist.size() - 1);
                    }
                    percVal = getBigDecimalValue(list.get(i - 1));
                    percVal = new BigDecimalBlk(percVal.getValue().divide(new BigDecimal(100), 10, RoundingMode.HALF_UP));

                    BigDecimalBlk buf1 = calculateSimpleOperations(_blist);
                    BigDecimalBlk buf2 = percVal;
                    ArrayList<String> bufl = new ArrayList<>();
                    bufl.add(buf1.getStringValue());
                    bufl.add(Information.getStringByTypeOfAction(typeOfAction.ACTION_UMNOZ));
                    bufl.add(buf2.getStringValue());
                    newElement = calculateSimpleOperations(bufl);


                    list.set(i - 1, newElement.getStringValue());
                    list.remove(i);
                }
                i++;
            }
        }
    }

    /**
     * Функция проверки и подсчета простейших операций таких как сложение, вычитание, умножение, деление
     * По входящей коллекции гоняется цикл, происходит поиск выражения под этой операцией. Происходит подсчет операции, и замена во входяем списке этого выражения на результат.
     *
     * @param list входящая коллекция десятичного представления выражения
     */
    private static BigDecimalBlk calculateSimpleOperations(ArrayList<String> list) {
        //По моему алгоритму, сначала расставляем приоритеты операций над пришедшим выражением
        int i = 0;
        // первым циклом прогоняем приоритетные операции такие как умножение деление, позже добавим новые операции
        while (i < list.size()) {
            String currentElement = list.get(i);
            if (isCommand(currentElement)) {
                switch (currentElement) {
                    case "\u00D7": {
                        list = replaceArray(i, calculateUmnoz(list, i).getStringValue(), list);
                        break;
                    }
                    case "/": {
                        list = replaceArray(i, calculateDel(list, i).getStringValue(), list);
                        break;
                    }
                    case "+":
                    case "-":
                        i++;
                }
            } else
                i++;
        }
        i = 0;
        while (i < list.size()) {
            String currentElement = list.get(i);
            if (isCommand(currentElement)) {
                switch (currentElement) {
                    case "+": {
                        list = replaceArray(i, calculateSumm(list, i).getStringValue(), list);
                        break;
                    }
                    case "-": {
                        list = replaceArray(i, calculateMinus(list, i).getStringValue(), list);
                        break;
                    }
                }
            } else
                i++;
        }
        //if (!isOpeningParenthesis(list.get(0)))
        //     return !list.isEmpty() ? getBigDecimalValue(list.get(0)) : BigDecimalBlk.valueOf(Double.NaN);
        //return BigDecimalBlk.valueOf(Double.NaN);
        if (list.isEmpty()) {
            return BigDecimalBlk.valueOf(Double.NaN);
        } else {
            return getBigDecimalValue(list.get(0));
        }
    }

    /**
     * TODO:Переделай это говно
     * Функция подсчета уножения
     *
     * @param list  входящая коллекция десятичного представления выражения
     * @param index номер элемента коллекции по которому определяется положение операции, по которому становится понятно где находятся операнды
     */
    private static BigDecimalBlk calculateUmnoz(ArrayList<String> list, int index) {
        try {
            if (getBigDecimalValue(list.get(index - 1)).isInfinity() || getBigDecimalValue(list.get(index + 1)).isInfinity())
                return new BigDecimalBlk(null, false, true);
            if (getBigDecimalValue(list.get(index - 1)).isNaN() || getBigDecimalValue(list.get(index + 1)).isNaN())
                return new BigDecimalBlk(null, true, false);

            return new BigDecimalBlk(getBigDecimalValue(list.get(index - 1)).getValue().multiply(getBigDecimalValue(list.get(index + 1)).getValue()));
        } catch (NullPointerException ne) {
            SingletonLogger.getInstance().onException(ne);
            ne.printStackTrace();
        }
        return new BigDecimalBlk(0);

        //return (getDoubleValue(list.get(index - 1)) * (getDoubleValue(list.get(index + 1))));
    }


    /**
     * TODO:Переделай это говно
     * Функция подсчета деления
     *
     * @param list  входящая коллекция десятичного представления выражения
     * @param index номер элемента коллекции по которому определяется положение операции, по которому становится понятно где находятся операнды
     */
    private static BigDecimalBlk calculateDel(ArrayList<String> list, int index) {
        BigDecimalBlk val = getBigDecimalValue(list.get(index + 1));
        if (getBigDecimalValue(list.get(index - 1)).isInfinity() || getBigDecimalValue(list.get(index + 1)).isInfinity())
            return new BigDecimalBlk(null, false, true);
        if (getBigDecimalValue(list.get(index - 1)).isNaN() || getBigDecimalValue(list.get(index + 1)).isNaN())
            return new BigDecimalBlk(null, true, false);
        if (val.getValue().compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimalBlk.valueOf(Double.POSITIVE_INFINITY);
        }
        return new BigDecimalBlk((getBigDecimalValue(list.get(index - 1)).getValue().divide(getBigDecimalValue(list.get(index + 1)).getValue(), 10, RoundingMode.HALF_UP)));

    }

    /**
     * TODO:Переделай это говно
     * Функция подсчета сложения
     *
     * @param list  входящая коллекция десятичного представления выражения
     * @param index номер элемента коллекции по которому определяется положение операции, по которому становится понятно где находятся операнды
     */
    private static BigDecimalBlk calculateSumm(ArrayList<String> list, int index) {
        if (getBigDecimalValue(list.get(index - 1)).isInfinity() || getBigDecimalValue(list.get(index + 1)).isInfinity())
            return new BigDecimalBlk(null, false, true);
        if (getBigDecimalValue(list.get(index - 1)).isNaN() || getBigDecimalValue(list.get(index + 1)).isNaN())
            return new BigDecimalBlk(null, true, false);

        return new BigDecimalBlk(getBigDecimalValue(list.get(index - 1)).getValue().add(getBigDecimalValue(list.get(index + 1)).getValue()));
    }

    /**
     * TODO:Переделай это говно
     * Функция подсчета вычитания
     *
     * @param list  входящая коллекция десятичного представления выражения
     * @param index номер элемента коллекции по которому определяется положение операции, по которому становится понятно где находятся операнды
     */
    private static BigDecimalBlk calculateMinus(ArrayList<String> list, int index) {
        if (getBigDecimalValue(list.get(index - 1)).isInfinity() || getBigDecimalValue(list.get(index + 1)).isInfinity())
            return new BigDecimalBlk(null, false, true);
        if (getBigDecimalValue(list.get(index - 1)).isNaN() || getBigDecimalValue(list.get(index + 1)).isNaN())
            return new BigDecimalBlk(null, true, false);
        BigDecimal bd = (getBigDecimalValue(list.get(index - 1)).getValue().subtract(getBigDecimalValue(list.get(index + 1)).getValue()));
        return new BigDecimalBlk(bd);
    }

    /**
     * TODO:Желательно тоже переделай это говно
     * Функция замены элемента коллекции на новый элемент, с удалением двух других вокруг индекса
     *
     * @param index номер элемента
     * @param value новое значение
     * @param list  коллекция в которой это происходит
     * @return новая коллекция
     */
    private static ArrayList<String> replaceArray(int index, String value, ArrayList<String> list) {
        list.set(index, value);
        list.remove(index - 1);
        list.remove(index);
        return list;
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент степенью
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isDegree(String element) {
        return (element.contains("^") || element.contains("степени") || element.contains("степень"));
        //return (!element.contains("^") || (((element.indexOf("^") + 1 != element.length() - 1) || (element.charAt(element.indexOf("^") + 1) != '0')))) && (element.contains("^"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент процентом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isPercent(String element) {
        return (element.equals("%") || element.equals("процент") || (element.equals("процентов")));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент квадратным корнем
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isSQRTelement(String element) {
        return element.equals("\u221A") || element.equals("квадратный корень") || element.equals("корень");
    }

    /**
     * Проверка на одиноко стоящий знак квадратного корня
     *
     * @param idx  - индекс
     * @param list - лист
     * @return - true/false
     */
    public static boolean isAloneSQRTWithNumeric(int idx, ArrayList<String> list) {
        return list.size() > idx + 2 && (Information.getTypeOFAction(list.get(idx + 1)) == typeOfAction.ACTION_SQRT) && (Information.getTypeOFAction(list.get(idx + 2)) == typeOfAction.ACTION_NUMERIC);
    }

    /**
     * определяет является ли входящий элемент пост-командой, то есть командой которая применяется на выражение перед ней
     *
     * @param s входящий элемент
     * @return - true/false
     */
    public static boolean isPastCommand(String s) {
        return (s.contains("%") || (s.contains("!")));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент открытой скобкой
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isOpeningParenthesis(String element) {
        return element.equals("(") || (element.equals("открывающая скобка"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент закрытой скобкой
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isClosingParenthesis(String element) {
        return element.equals(")") || (element.equals("закрывающая скобка"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент экспонентной константой
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isConstExp(String element) {
        return (element.equals("exp") || element.equals("е"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент числом пи
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isConstPi(String element) {
        return (element.equals("\u03C0") || element.equals("пи"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент числом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isNumeric(String element) {
        try {
            if (Information.getCurrentTypeOfSystem() == typeOfNumberSystem.NUMBER_SYSTEM_HEXADECIMAL ||
                    (element.contains("A") || element.contains("B") || element.contains("C") || element.contains("D") || element.contains("E") || element.contains("F")))
                return isHexadecimal(element);
            double d = Double.valueOf(element);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент десятичным логарифмом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isLg(String element) {
        return element.contains("lg") || element.equals("десятичный логарифм");
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент натуральным логарифмом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isLn(String element) {
        return element.contains("ln") || element.equals("натуральный логарифм");
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент синусом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isSin(String element) {
        return element.contains("sin") || element.equals("синус");
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент косинусом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isCos(String element) {
        return element.contains("cos") || element.equals("косинус");
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент плюсом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isPlus(String element) {
        return (element.equals("+") ||
                element.equals("плюс") ||
                element.equals("plus") ||
                element.equals("сложение") ||
                element.equals("добавить") ||
                element.equals("прибавить"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент минусом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isMinus(String element) {
        return (element.equals("-") ||
                element.equals("минус") ||
                element.equals("minus") ||
                element.equals("вычесть") ||
                element.equals("убавить") ||
                element.equals("отнять") ||
                element.equals("вычитание"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент умножением
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isUmnoz(String element) {
        return (element.equals("*") ||
                element.equals("умножить") ||
                element.equals("x") ||
                element.equals("\u00D7") ||
                element.equals("помножить") ||
                element.equals("перемножить"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент делением
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isDelen(String element) {
        return (element.equals("/") ||
                element.equals("разделить") ||
                element.equals("поделить") ||
                element.equals("делить"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент факториалом
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isFactorial(String element) {
        return (element.equals("факториал") || element.equals("!") || element.equals("factorial"));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент командой из множества {+,-,*,/}
     *
     * @param element входящий элемент
     * @return true/false
     */
    public static boolean isCommand(String element) {
        return (isPlus(element) || isMinus(element) || isDelen(element) || isUmnoz(element));
    }

    /**
     * Функция возвращаюшая, является ли входящий элемент шестнадцатеричным числом
     *
     * @param element входящий элемент
     * @return true/false
     */
    private static boolean isHexadecimal(String element) {
        return !(element.equals(".") || element.equals("=") || isCommand(element) || isSQRTelement(element) || isDegree(element) || isPercent(element)
                || isDegree(element) || isOpeningParenthesis(element) || isClosingParenthesis(element) || isDegree(element) || isCos(element) || isSin(element) || (element.isEmpty()));
    }


    public static double sin(double va) {
        int accuracy = 10;
        double val = 0;
        int counter = 3;
        for (int i = 0; i < accuracy; i++) {
            val += (Math.pow(va, counter) / fact(counter));
            counter += 2;
        }
        return val;
    }
}
