package com.blk_.calculate.math;

import com.blk_.calculate.Information;

import java.math.BigDecimal;

public class BigDecimalBlk {
    public BigDecimal getValue() {
        return value;
    }

    public double getDoubleValue() {
        if (isNaN)
            return Double.NaN;
        else if (isInfinity)
            return Double.POSITIVE_INFINITY;
        else return value.doubleValue();
    }

    public String getStringValue() {
        if (isInfinity)
            return Information.getStringForViewInfinity();
        if (isNaN)
            return Information.getStringForViewNaN();
        if (this.getValue().compareTo(BigDecimal.ZERO) == 0)
            return "0";
        return (value.toString()).replace("E+", "E");
    }

    private BigDecimal value;
    private boolean isNaN;
    private boolean isInfinity;

    public BigDecimalBlk(BigDecimal bd, boolean isNaN, boolean isInfinity) {
        if (isInfinity || isNaN)
            value = null;
        else
            value = bd;
        this.isNaN = isNaN;
        this.isInfinity = isInfinity;

    }

    BigDecimalBlk(BigDecimal val) {
        value = val;
        isNaN = false;
        isInfinity = false;
    }

    public BigDecimalBlk(double val) {
        setValue(val);
    }


    public void setValue(double val) {
        isNaN = Double.isNaN(val);
        isInfinity = Double.isInfinite(val);
        if (!isInfinity && !isNaN) {
            value = new BigDecimal(val);
        } else
            value = null;
    }

    //    public void setValue(String val){
//        setValue(Double.valueOf(val));
//    }
    public static BigDecimalBlk valueOf(double val) {
        if (Double.isNaN(val) || (Double.isInfinite(val))) {
            return new BigDecimalBlk(null, Double.isNaN(val), Double.isInfinite(val));
        } else {
            return new BigDecimalBlk(BigDecimal.valueOf(val), Double.isNaN(val), Double.isInfinite(val));
        }
    }

//    public static BigDecimalBlk valueOf(String val){
//        return BigDecimalBlk.valueOf(Double.valueOf(val));
//    }


    public boolean isInfinity() {
        return isInfinity;
    }

    public boolean isNaN() {
        return isNaN;
    }
}
