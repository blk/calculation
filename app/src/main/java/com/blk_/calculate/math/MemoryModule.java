package com.blk_.calculate.math;

import com.blk_.calculate.Information;
import com.blk_.calculate.enums.typeOfAction;
import com.blk_.calculate.enums.typeOfNumberSystem;
import com.blk_.calculate.parsing.Parse;

/**
 * Модуль-класс отвечающий за клавиши памяти и работу с ними (м+ м- мс мр)
 */

public class MemoryModule {
    private boolean cleanStatus;
    private String value;
    private typeOfNumberSystem currentTNS;

    /**
     * Установить текущую систему счисления , и в соответствии с этим перевести значение в памяти в нужную сс
     *
     * @param currentTNS - система сичлсения
     */
    public void setCurrentTNS(typeOfNumberSystem currentTNS) {
        if ((this.currentTNS != currentTNS) && (this.currentTNS != null) && ((!value.equals("")))) {
            value = Converter.convertAdapter(value, this.currentTNS, currentTNS);
        }
        this.currentTNS = currentTNS;
    }

    /**
     * Конструктор класса.инициализация значения пустотой)))
     */
    public MemoryModule() {
        value = "";
        cleanStatus = true;
    }

    /**
     * Добавить в память
     *
     * @param addVal - добавляемое значение
     */
    public void plusToMemory(String addVal) {
        addToMemory(addVal, typeOfAction.ACTION_PLUS);
        cleanStatus = false;
    }

    /**
     * Отнять из памяти
     *
     * @param addVal - отнимаемое значение
     */
    public void minusFromMemory(String addVal) {
        addToMemory(addVal, typeOfAction.ACTION_MINUS);
        cleanStatus = false;
    }

    /**
     * внести в память значение уже в математическом плане
     *
     * @param addVal - значение
     * @param toa    - тип действия
     */
    private void addToMemory(String addVal, typeOfAction toa) {
        setCurrentTNS(Information.getCurrentTypeOfSystem());
        if (value.equals(""))
            value = "0";
        String bufDecVal = Converter.convertAdapter(addVal, currentTNS, typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL);
        String bufDecCurVal = Converter.convertAdapter(value, currentTNS, typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL);
        String preResult;

        if (bufDecCurVal.equals(Information.getStringForViewNaN()) || bufDecVal.equals(Information.getStringForViewNaN())) {
            value = Information.getStringForViewNaN();
            return;
        }

        if (bufDecCurVal.equals(Information.getStringForViewInfinity()) || bufDecVal.equals(Information.getStringForViewInfinity())) {
            value = Information.getStringForViewInfinity();
            return;
        }
        switch (toa) {
            case ACTION_PLUS: {
                preResult = String.valueOf(Calculate.getBigDecimalValue(bufDecCurVal).getValue().
                        add(Calculate.getBigDecimalValue(bufDecVal).getValue()));
                break;
            }
            case ACTION_MINUS: {
                preResult = String.valueOf(Calculate.getBigDecimalValue(bufDecCurVal).getValue().subtract(Calculate.getBigDecimalValue(bufDecVal).getValue()));
                break;
            }
            default:
                return;
        }
        value = Converter.convertAdapter(preResult, typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL, currentTNS);
    }

    /**
     * очистка памяти
     */
    public void clearMemory() {
        value = "";
        cleanStatus = true;

    }

    /**
     * Возврат пустая ли память
     *
     * @return - true/false
     */
    public boolean getCleanStatus() {
        return cleanStatus;
    }

    /**
     * Возврат значения лежащего в памяти с разделитялями, чисто для внешнего вывода
     *
     * @return - true/false
     */
    public String getFormatMemoryValue() {
        if (!value.isEmpty()) {
            if (Double.isNaN(Calculate.getBigDecimalValue(Converter.convertAdapter(value, Information.getCurrentTypeOfSystem(), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL))
                    .getDoubleValue()))
                return Information.getStringForViewNaN();
            else if (Double.isInfinite(Calculate.getBigDecimalValue
                    (Converter.convertAdapter(value, Information.getCurrentTypeOfSystem(), typeOfNumberSystem.NUMBER_SYSTEM_DECIMAL))
                    .getDoubleValue()))
                return Information.getStringForViewInfinity();
        }
        return Parse.parseView_addSeparatorsToString(Parse.parseView_removeInsignificantZerosFromElement(value), currentTNS);
    }

    /**
     * Возврат значения лежащего в памяти без разделителей, для работы с ним
     *
     * @return - true/false
     */
    public String getMemoryValue() {
        if (!value.isEmpty()) {
            if (Calculate.getBigDecimalValue(value).isNaN())
                return Information.getStringForViewNaN();
            if (Calculate.getBigDecimalValue(value).isInfinity())
                return Information.getStringForViewInfinity();
        }
        return Parse.parseView_removeInsignificantZerosFromElement(value);
    }
}
