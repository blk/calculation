package com.blk_.calculate.styleTheme;

import android.content.Context;

import com.blk_.calculate.R;
import com.blk_.calculate.enums.typeOfStyleTheme;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DrawableStyleManager {

    public static void setDarkTheme(Context context) {
        rcontext = context;
        params = getDarkParams();
        setCurrentStyle(typeOfStyleTheme.DARK_THEME);
        try {
            saveState(context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setLightTheme(Context context) {
        rcontext = context;
        params = getLightParams();
        setCurrentStyle(typeOfStyleTheme.LIGHT_THEME);
        try {
            saveState(context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static StyleThemeParams getParams() {
        return params;
    }

    private static StyleThemeParams params;
    private static typeOfStyleTheme currentStyle = typeOfStyleTheme.LIGHT_THEME;

    private static int styleToInt(typeOfStyleTheme tot) {
        switch (tot) {
            case DARK_THEME:
                return 1;
            case LIGHT_THEME:
                return 2;
            default:
                return -1;
        }
    }

    private static typeOfStyleTheme intToStyle(int tot) {
        switch (tot) {
            case 1:
                return typeOfStyleTheme.DARK_THEME;
            case 2:
                return typeOfStyleTheme.LIGHT_THEME;
            default:
                return typeOfStyleTheme.LIGHT_THEME;
        }
    }

    public static void saveState(Context context) throws IOException {
        FileOutputStream fos = new FileOutputStream(context.getFilesDir() + "style.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeInt(styleToInt(getCurrentStyle()));
        rcontext = context;
        oos.flush();
        oos.close();
    }

    private static Context rcontext;

    public static typeOfStyleTheme getCurrentStyle() {
        return currentStyle;
    }

    private static void setCurrentStyle(typeOfStyleTheme _currentStyle) {
        currentStyle = _currentStyle;
    }

    public static void getState(Context context) throws IOException {
        rcontext = context;
        FileInputStream fis = new FileInputStream(context.getFilesDir() + "style.out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        setCurrentStyle(intToStyle(oin.readInt()));


    }

    private static StyleThemeParams getDarkParams() {
        StyleThemeParams buf = new StyleThemeParams();
        buf.drawableNumericID = (R.drawable.styledarknumeric);
        buf.drawableOperationID = R.drawable.styledarkoperation;
        buf.textNumericColor = rcontext.getResources().getColor(R.color.numericDarkText);
        buf.textOperationColor = rcontext.getResources().getColor(R.color.operationDarkText);
        buf.drawableToggleID = R.drawable.styledarktoggle;
        buf.styleRadioID = R.drawable.styledarkradio;
        buf.drawableTextID = R.drawable.styledarktext;
        buf.parserColorExpr = R.color.darkThemeExpressionExpression;
        buf.parserColorConst = R.color.darkThemeExpressionConst;
        buf.parserColorNum = R.color.darkThemeExpressionNumeric;
        buf.parserColorOper = R.color.darkThemeExpressionOperation;
        buf.textResultColor = R.color.darkThemeTextResult;
        buf.dialogHistoryButtonColor = R.color.darkThemeDialogButton;
        buf.dialogHistoryText = R.color.darkThemeDialogText;
        //buf.dialogHistoryIconID= R.drawable.icons8deletegray;
        buf.dialogHistoryBackground = R.color.darkThemeDialogBackground;
        buf.drawableForSettings = R.drawable.styledarksettings;
        buf.drawableForSlider = R.drawable.styledarkslider;
        buf.drawableMicro = R.drawable.icon_voice_selector_dark;
        buf.drawableHistory = R.drawable.icon_history_selector_dark;
        buf.styleRadioVoiceID = R.drawable.styledarkradiovoice;
        buf.textDisabled = R.color.numericDarkTextDisabled;
        buf.drawableminislider = R.drawable.styledarkminislider;
        buf.drawableForButtonTheme = R.drawable.styledarkbuttonfortheme;
        return buf;
    }

    private static StyleThemeParams getLightParams() {
        StyleThemeParams buf = new StyleThemeParams();
        buf.drawableNumericID = (R.drawable.stylelightnumeric);
        buf.drawableOperationID = (R.drawable.stylelightoperation);
        buf.textNumericColor = rcontext.getResources().getColor(R.color.numericLightText);
        buf.textOperationColor = rcontext.getResources().getColor(R.color.operationLightText);
        buf.drawableToggleID = R.drawable.stylelighttoggle;
        buf.styleRadioID = R.drawable.stylelightradio;
        buf.drawableTextID = R.drawable.stylelighttext;
        buf.parserColorExpr = R.color.lightThemeExpressionExpression;
        buf.parserColorConst = R.color.lightThemeExpressionConst;
        buf.parserColorNum = R.color.lightThemeExpressionNumeric;
        buf.parserColorOper = R.color.lightThemeExpressionOperation;
        buf.textResultColor = R.color.lightThemeTextResult;

        buf.dialogHistoryButtonColor = R.color.lightThemeDialogButton;
        buf.dialogHistoryText = R.color.lightThemeDialogText;
        //buf.dialogHistoryIconID= R.drawable.icons8deletegray;
        buf.dialogHistoryBackground = R.color.lightThemeDialogBackground;
        buf.drawableForSettings = R.drawable.stylelightsettings;
        buf.drawableForSlider = R.drawable.stylelightslider;
        buf.drawableMicro = R.drawable.icon_voice_selector_light;
        buf.drawableHistory = R.drawable.icon_history_selector_light;
        buf.styleRadioVoiceID = R.drawable.stylelightradiovoice;
        buf.textDisabled = R.color.numericLightTextDisabled;
        buf.drawableminislider = R.drawable.stylelightminislider;
        buf.drawableForButtonTheme = R.drawable.stylelightbuttonfortheme;
        return buf;
    }

}
