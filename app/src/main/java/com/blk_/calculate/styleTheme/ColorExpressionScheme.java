package com.blk_.calculate.styleTheme;

/**
 * Класс - структуры, цветовая схема, используемая для "раскрашивания" текста выражения на экране
 */
public class ColorExpressionScheme {
    public int getColorOperation() {
        return colorOperation;
    }

    public int getColorNumeric() {
        return colorNumeric;
    }

    public int getColorExpression() {
        return colorExpression;
    }

    public int getColorConst() {
        return colorConst;
    }

    private final int colorOperation;
    private final int colorNumeric;
    private final int colorExpression;
    private final int colorConst;

    /**
     * Конструктор в котором инициализируем цвета
     *
     * @param colorNumeric    - цвет чисел
     * @param colorOperation  - цвет операций
     * @param colorExpression - цвет подсвеченного незаконченного выражения
     * @param colorConst      - цвет отображения констант
     */
    ColorExpressionScheme(int colorNumeric, int colorOperation, int colorExpression, int colorConst) {
        this.colorConst = colorConst;
        this.colorNumeric = colorNumeric;
        this.colorOperation = colorOperation;
        this.colorExpression = colorExpression;
    }
}
