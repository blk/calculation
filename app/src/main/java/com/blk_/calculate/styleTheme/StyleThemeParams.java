package com.blk_.calculate.styleTheme;

public class StyleThemeParams {
    public int drawableNumericID;
    public int drawableOperationID;
    public int textNumericColor;
    public int textOperationColor;
    public int drawableToggleID;
    public int styleRadioID;
    public int drawableTextID;
    public int parserColorExpr;
    public int parserColorConst;
    public int parserColorNum;
    public int parserColorOper;
    public int textResultColor;
    public int dialogHistoryText;
    public int dialogHistoryIconID;
    public int dialogHistoryButtonColor;
    public int dialogHistoryBackground;
    public int drawableForSettings;
    public int drawableForSlider;
    public int drawableMicro;
    public int drawableHistory;
    public int styleRadioVoiceID;
    public int textDisabled;
    public int drawableminislider;
    public int drawableForButtonTheme;

    StyleThemeParams() {
    }
}
