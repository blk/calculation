package com.blk_.calculate.enums;

/**
 * Структура для передачи того какая кнопка была нажата в диалоговом окне истории
 */
public enum typeOfDialogButton {
    DIALOG_BUTTON_OK,
    DIALOG_BUTTON_CANCEL,
    DIALOG_BUTTON_CLEAR
}
