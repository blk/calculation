package com.blk_.calculate.enums;

/**
 * Created by blk_ on 12.03.2018.
 * Структура типа системы счисления
 */
public enum typeOfNumberSystem {
    NUMBER_SYSTEM_UNKNOWN,
    NUMBER_SYSTEM_BINARY,
    NUMBER_SYSTEM_OCTAL,
    NUMBER_SYSTEM_DECIMAL,
    NUMBER_SYSTEM_HEXADECIMAL
}
