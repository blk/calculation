package com.blk_.calculate.enums;

/**
 * Created by blk_ on 19.03.2018.
 * Структура радианы - градусы . нужно для вычислений тригонометрии и слушания кнопки рад-дегр
 */

public enum StateRadDegr {
    TYPE_INPUT_RADIAN,
    TYPE_INPUT_DEGREE
}
