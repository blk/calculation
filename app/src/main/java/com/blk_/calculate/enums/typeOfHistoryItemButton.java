package com.blk_.calculate.enums;

/**
 * Структура для передачи того какая кнопка нажата на каждый элемент списка в диалоговом окне историй
 */
public enum typeOfHistoryItemButton {
    BUTTON_DIALOG_ITEM_UPLOAD,
    BUTTON_CLEAR_ALL,
    BUTTON_CLOSE,
    BUTTON_DIALOG_ITEM_DELETE
}
