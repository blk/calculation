package com.blk_.calculate.enums;

/**
 * Структура со списком типов тем
 */
public enum typeOfStyleTheme {
    DARK_THEME,
    LIGHT_THEME
}
