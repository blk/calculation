package com.blk_.calculate.enums;

/**
 * Структура текстового размера , передается от ползунка в настройках через маин активити в текстовый фрагмент
 */
public enum textSize {
    TEXT_SIZE_SMALL,
    TEXT_SIZE_MEDIUM,
    TEXT_SIZE_BIG,
    TEXT_SIZE_FUCKIN_LARGE
}
