package com.blk_.calculate.enums;

/**
 * Created by blk_ on 12.03.2018.
 * Структура типов действий. все типы действий которые  только могут быть в программе
 */
public enum typeOfAction {
    UNKNOWN_ACTION,
    ACTION_NUMERIC,
    ACTION_NUMERIC0,
    ACTION_NUMERIC1,
    ACTION_NUMERIC2,
    ACTION_NUMERIC3,
    ACTION_NUMERIC4,
    ACTION_NUMERIC5,
    ACTION_NUMERIC6,
    ACTION_NUMERIC7,
    ACTION_NUMERIC8,
    ACTION_NUMERIC9,
    ACTION_NUMERICA,
    ACTION_NUMERICB,
    ACTION_NUMERICC,
    ACTION_NUMERICD,
    ACTION_NUMERICE,
    ACTION_NUMERICF,
    ACTION_PLUS,
    ACTION_MINUS,
    ACTION_UMNOZ,
    ACTION_DELEN,
    ACTION_DEGREE,
    ACTION_UNDEGREE,
    ACTION_NUMERIC_PLUS_MINUS,
    ACTION_SQRT,
    ACTION_PERCENT,
    ACTION_FLOAT,
    ACTION_ENDED_NUMERIC,
    ACTION_OPENING_PARENTHESIS,
    ACTION_CLOSING_PARENTHESIS,
    ACTION_FACTORIAL,
    ACTION_1DX,
    ACTION_SUBOPERATION_X2,
    ACTION_SINX,
    ACTION_COSX,
    ACTION_SUMMARY,
    ACTION_CLEAR,
    ACTION_CLEAR_LAST,
    ACTION_RADIO_BIN,
    ACTION_RADIO_OCT,
    ACTION_RADIO_DEC,
    ACTION_RADIO_HEX,
    ACTION_LG,
    ACTION_LN,
    ACTION_CONST_E,
    ACTION_CONST_PI,
    BUTTON_MEMORY_PLUS,
    BUTTON_MEMORY_MINUS,
    BUTTON_MEMORY_CLEAR,
    BUTTON_MEMORY_UPLOAD,
    BUTTON_SETTINGS;

    public static String getShortValue(typeOfAction t) {
        switch (t) {
            case UNKNOWN_ACTION:
                return "UA";
            case ACTION_NUMERIC:
                return "N";
            case ACTION_NUMERIC0:
                return "N0";
            case ACTION_NUMERIC1:
                return "N1";
            case ACTION_NUMERIC2:
                return "N2";
            case ACTION_NUMERIC3:
                return "N3";
            case ACTION_NUMERIC4:
                return "N4";
            case ACTION_NUMERIC5:
                return "N5";
            case ACTION_NUMERIC6:
                return "N6";
            case ACTION_NUMERIC7:
                return "N7";
            case ACTION_NUMERIC8:
                return "N8";
            case ACTION_NUMERIC9:
                return "N9";
            case ACTION_NUMERICA:
                return "NA";
            case ACTION_NUMERICB:
                return "NB";
            case ACTION_NUMERICC:
                return "NC";
            case ACTION_NUMERICD:
                return "ND";
            case ACTION_NUMERICE:
                return "NE";
            case ACTION_NUMERICF:
                return "NF";
            case ACTION_PLUS:
                return "PL";
            case ACTION_MINUS:
                return "MI";
            case ACTION_UMNOZ:
                return "UM";
            case ACTION_DELEN:
                return "DL";
            case ACTION_DEGREE:
                return "DG";
            case ACTION_UNDEGREE:
                return "UD";
            case ACTION_NUMERIC_PLUS_MINUS:
                return "PM";
            case ACTION_SQRT:
                return "SQ";
            case ACTION_PERCENT:
                return "PR";
            case ACTION_FLOAT:
                return "FL";
            case ACTION_ENDED_NUMERIC:
                return "EN";
            case ACTION_OPENING_PARENTHESIS:
                return "OP";
            case ACTION_CLOSING_PARENTHESIS:
                return "CP";
            case ACTION_FACTORIAL:
                return "FC";
            case ACTION_1DX:
                return "1D";
            case ACTION_SUBOPERATION_X2:
                return "X2";
            case ACTION_SINX:
                return "SN";
            case ACTION_COSX:
                return "CS";
            case ACTION_SUMMARY:
                return "SU";
            case ACTION_CLEAR:
                return "C";
            case ACTION_CLEAR_LAST:
                return "CL";
            case ACTION_RADIO_BIN:
                return "RB";
            case ACTION_RADIO_OCT:
                return "RO";
            case ACTION_RADIO_DEC:
                return "RD";
            case ACTION_RADIO_HEX:
                return "RH";
            case ACTION_LG:
                return "LG";
            case ACTION_LN:
                return "LN";
            case ACTION_CONST_E:
                return "EX";
            case ACTION_CONST_PI:
                return "PI";
            case BUTTON_MEMORY_PLUS:
                return "MP";
            case BUTTON_MEMORY_MINUS:
                return "MM";
            case BUTTON_MEMORY_CLEAR:
                return "MC";
            case BUTTON_MEMORY_UPLOAD:
                return "MU";
            case BUTTON_SETTINGS:
                return "BS";
            default:
                return "";
        }
    }

    public static typeOfAction getToaByString(String t) {
        switch (t) {
            case "UA":
                return UNKNOWN_ACTION;
            case "N":
                return ACTION_NUMERIC;
            case "N0":
                return ACTION_NUMERIC0;
            case "N1":
                return ACTION_NUMERIC1;
            case "N2":
                return ACTION_NUMERIC2;
            case "N3":
                return ACTION_NUMERIC3;
            case "N4":
                return ACTION_NUMERIC4;
            case "N5":
                return ACTION_NUMERIC5;
            case "N6":
                return ACTION_NUMERIC6;
            case "N7":
                return ACTION_NUMERIC7;
            case "N8":
                return ACTION_NUMERIC8;
            case "N9":
                return ACTION_NUMERIC9;
            case "NA":
                return ACTION_NUMERICA;
            case "NB":
                return ACTION_NUMERICB;
            case "NC":
                return ACTION_NUMERICC;
            case "ND":
                return ACTION_NUMERICD;
            case "NE":
                return ACTION_NUMERICE;
            case "NF":
                return ACTION_NUMERICF;
            case "PL":
                return ACTION_PLUS;
            case "MI":
                return ACTION_MINUS;
            case "UM":
                return ACTION_UMNOZ;
            case "DL":
                return ACTION_DELEN;
            case "DG":
                return ACTION_DEGREE;
            case "UD":
                return ACTION_UNDEGREE;
            case "PM":
                return ACTION_NUMERIC_PLUS_MINUS;
            case "SQ":
                return ACTION_SQRT;
            case "PR":
                return ACTION_PERCENT;
            case "FL":
                return ACTION_FLOAT;
            case "EN":
                return ACTION_ENDED_NUMERIC;
            case "OP":
                return ACTION_OPENING_PARENTHESIS;
            case "CP":
                return ACTION_CLOSING_PARENTHESIS;
            case "FC":
                return ACTION_FACTORIAL;
            case "1D":
                return ACTION_1DX;
            case "X2":
                return ACTION_SUBOPERATION_X2;
            case "SN":
                return ACTION_SINX;
            case "CS":
                return ACTION_COSX;
            case "SU":
                return ACTION_SUMMARY;
            case "C":
                return ACTION_CLEAR;
            case "CL":
                return ACTION_CLEAR_LAST;
            case "RB":
                return ACTION_RADIO_BIN;
            case "RO":
                return ACTION_RADIO_OCT;
            case "RD":
                return ACTION_RADIO_DEC;
            case "RH":
                return ACTION_RADIO_HEX;
            case "LG":
                return ACTION_LG;
            case "LN":
                return ACTION_LN;
            case "EX":
                return ACTION_CONST_E;
            case "PI":
                return ACTION_CONST_PI;
            case "MP":
                return BUTTON_MEMORY_PLUS;
            case "MM":
                return BUTTON_MEMORY_MINUS;
            case "MC":
                return BUTTON_MEMORY_CLEAR;
            case "MU":
                return BUTTON_MEMORY_UPLOAD;
            case "BS":
                return BUTTON_SETTINGS;
            default:
                return UNKNOWN_ACTION;
        }
    }
}
