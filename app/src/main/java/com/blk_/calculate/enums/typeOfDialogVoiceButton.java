package com.blk_.calculate.enums;

public enum typeOfDialogVoiceButton {
    DIALOG_BUTTON_ADD,
    DIALOG_BUTTON_REPLACE,
    DIALOG_BUTTON_TRY_AGAIN,
    DIALOG_BUTTON_CANCEL
}
