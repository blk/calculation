package com.blk_.calculate.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import com.blk_.calculate.App;
import com.blk_.calculate.R;
import com.blk_.calculate.logger.SingletonLogger;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailSender {

    private final String username = "serviceblk88@gmail.com";
    private final String password = "service12345qwe";
    private Context act;

    public void sendMail(Context act) {
        StringBuilder str = new StringBuilder();
        PackageInfo pInfo = null;
        try {
            pInfo = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        str.append("BOARD : ").append(Build.BOARD).append(" \n");
        str.append("BOOTLOADER : ").append(Build.BOOTLOADER).append(" \n");
        str.append("BRAND : ").append(Build.BRAND).append(" \n");
        str.append("DEVICE : ").append(Build.DEVICE).append(" \n");
        str.append("DISPLAY : ").append(Build.DISPLAY).append(" \n");
        str.append("FINGERPRINT : ").append(Build.FINGERPRINT).append(" \n");
        str.append("HARDWARE : ").append(Build.HARDWARE).append(" \n");
        str.append("HOST : ").append(Build.HOST).append(" \n");
        str.append("ID : ").append(Build.ID).append(" \n");
        str.append("MANUFACTURER : ").append(Build.MANUFACTURER).append(" \n");
        str.append("MODEL : ").append(Build.MODEL).append(" \n");
        str.append("PRODUCT : ").append(Build.PRODUCT).append(" \n");
        str.append("TAGS : ").append(Build.TAGS).append(" \n");
        str.append("TIME : ").append(Build.TIME).append(" \n");
        str.append("TYPE : ").append(Build.TYPE).append(" \n");
        str.append("USER : ").append(Build.USER).append(" \n");
        str.append("RADIO VERSION : ").append(Build.getRadioVersion()).append(" \n");

        if (pInfo != null) {
            str.append("version: ").append(pInfo.versionName);
            str.append("\n");
            str.append("versionCode: ").append(pInfo.versionCode);
            str.append("\n");
        }
        this.act = act;
        Session session = createSessionObject();
        String email = "blkfeedback@yandex.ru";
        String subject = "Error Report";
        try {
            Message message = createMessage(email, subject, str.toString(), session, Build.ID);
            new SendMailTask().execute(message);
        } catch (AddressException | UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
            Toast.makeText(App.getContext(), act.getString(R.string.emtyLogs), Toast.LENGTH_LONG).show();
        }
    }

    private Message createMessage(String email, String subject, String messageBody, Session session, String from) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("serviceblk@gmail.com", from));
        //message.add
        message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        if (!SingletonLogger.isLogAvailable())
            throw new MessagingException();
        if (!SingletonLogger.getInstance().getFileName().isEmpty()) {
            FileDataSource fileDataSource = new FileDataSource(SingletonLogger.getInstance().getFileName());
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setText(messageBody);

            MimeBodyPart attachmentPart = new MimeBodyPart();
            attachmentPart.setDataHandler(new DataHandler(fileDataSource));
            attachmentPart.setFileName(fileDataSource.getName());

            // Create Multipart E-Mail.
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(attachmentPart);
            multipart.addBodyPart(messagePart);
            message.setContent(multipart);

        }

        return message;
    }

    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.connectiontimeout", "2000");
        properties.put("mail.smtp.timeout", "7000");

        return Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
    }

    private class SendMailTask extends AsyncTask<Message, Void, Boolean> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Toast.makeText(App.getContext(), "Error report sending in progress...", Toast.LENGTH_LONG).show();
            progressDialog = ProgressDialog.show(act, act.getString(R.string.sendMesReportTitle), act.getString(R.string.sendMesReport), true, false);
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);
            if (aVoid) {
                Toast.makeText(App.getContext(), act.getString(R.string.sendMesReportTitleSucces), Toast.LENGTH_LONG).show();
                SingletonLogger.clearFile();
            } else {
                Toast.makeText(App.getContext(), act.getString(R.string.sendMesReportTitleError), Toast.LENGTH_SHORT).show();
            }
            progressDialog.dismiss();
        }

        @Override
        protected Boolean doInBackground(Message... messages) {
            try {
                //if (1 == 2)
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
                SingletonLogger.getInstance().onException(e);
                return false;
            }
            return true;
        }
    }


}
